<?php

namespace App\Console;

use Illuminate\Support\Facades\DB;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // the call method
        $schedule->call(function () {

            $timeNow = date('Y');

            $Data = DB::table('surat_berkas')->get();

            foreach ($Data as $data) {

                $getKlasifikasiId = $data->klasifikasi_id;

                $getKlasifikasiActive = DB::table('m_surat_klasifikasi')->where('id', $getKlasifikasiId)->pluck('retensi_thn_active');

                if ( count($getKlasifikasiActive) != 0 ) {

                    $getCreated_at  = $data->created_at;
                    $getActiveYear  = $getKlasifikasiActive[0];

                    $activeUntil    = date('Y', strtotime($getCreated_at)) + $getActiveYear;

                    if ( $activeUntil == $timeNow ) {

                        DB::table('surat_berkas')
                            ->where("id", $data->id)
                            ->update([
                                'status' => 'inactive'
                            ]);
                        }
                    } else {
                        continue;
                    }

                }

        })->everyMinute();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}

        // the call method
        // $schedule->call(function () {
                        
        //     $timeNow = date('Y');

        //     $Data = DB::table('surat')->get();

        //     foreach ($Data as $data) {

        //         if ( $data->berkas_id != 'NULL' || $data->berkas_id != '' ) {

        //             $getKlasifikasiId = DB::table('surat_berkas')->where('id', $data->berkas_id)->pluck('klasifikasi_id');

        //             if ( count($getKlasifikasiId) != 0 ) {

        //                 $getKlasifikasiActive = DB::table('m_surat_klasifikasi')->where('id', $getKlasifikasiId[0])->pluck('retensi_thn_active');

        //                 if ( count($getKlasifikasiActive) != 0 ) {

        //                     $getCreated_at  = $data->created_at;
        //                     $getActiveYear  = $getKlasifikasiActive[0];

        //                     $activeUntil    = date('Y', strtotime($getCreated_at)) + $getActiveYear;

        //                     if ( $activeUntil == $timeNow ) {

        //                         DB::table('surat')
        //                             ->where("id", $data->id)
        //                             ->update([
        //                                 'status' => 'inactive'
        //                             ]);
        //                         }
        //                     } else {
        //                         continue;
        //                     }

        //                 } else {
        //                     continue;
        //                 }

        //             } else {
        //                 continue;
        //             }
        //         }

        // })->everyMinute();