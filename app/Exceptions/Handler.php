<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Contracts\Support\Responsable;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;
use InfyOm\Generator\Utils\ResponseUtil;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param \Exception $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Exception $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
//        $e = $exception;
//        $arr = $this->convertExceptionToArray($e);
//        $def = ["success"=>false,"message"=>$e->getMessage()];
//
//        if (method_exists($e, 'render') && $response = $e->render($request)) {
//            return Router::toResponse($request, $response);
//        } elseif ($e instanceof Responsable) {
//            return $e->toResponse($request);
//        }
//
//        $e = $this->prepareException($e);
//        if ($e instanceof HttpResponseException) {
//            return $e->getResponse();
//        }elseif ($e instanceof AuthenticationException) {
////            return $this->unauthenticated($request, $e);
//            return Response()->json($def,401);
//        } elseif ($e instanceof ValidationException) {
////            return $this->convertValidationExceptionToResponse($e, $request);
//            $def = array_merge($def,["errors"=>$e->errors()]);
//        }
//        $arr = array_merge($def,$arr);
//
//        return Response()->json(
//            $arr,
//            $this->isHttpException($e) ? $e->getStatusCode() : $e->status?:500,
//            $this->isHttpException($e) ? $e->getHeaders() : [],
//            JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES
//            );

        // return parent::render($request, $exception);
        // 
        $rendered = parent::render($request, $exception);

        if ($rendered->getStatusCode() == 404) {
            return response()->json([
                'error' => true,    
                'code' => $rendered->getStatusCode(),
                'message' => "Not Found",
            
            ], $rendered->getStatusCode());                  
        }

        return response()->json([
            'error' => true,    
            'code' => $rendered->getStatusCode(),
            'message' => $exception->getMessage(),
        
        ], $rendered->getStatusCode());        
        
    }
}
