<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
// use Maatwebsite\Excel\Concerns\WithEvents;

use Illuminate\Support\Facades\DB;

class BladeExports implements FromView
{

    private $code     = '';
    private $month    = '';	
    private $suratBerkas = [];

    public function __construct($code = '', $month = '')
    {
        $this->code   = $code;
        $this->month  = $month;

      $this->suratBerkas = DB::table('surat_berkas')
        ->join('surat', 'surat.berkas_id', '=', 'surat_berkas.id')
        ->join('inbox', 'inbox.surat_id', '=', 'surat.id')
        ->join('m_surat_sifat', 'm_surat_sifat.id', '=', 'surat.sifat_id')
        ->join('m_surat_tipe', 'm_surat_tipe.id', '=', 'surat.tipe_id')
        ->select(
            'surat_berkas.berkas_no as NO. BERKAS',
            'surat_berkas.klasifikasi_code as KODE KLASIFIKASI',
            'surat.no_surat_unit_kerja as NO. SURAT',
            'surat.perihal as PERIHAL',
            'inbox.ori_unor_surat_nama_asal as ASAL SURAT', 
            'inbox.ori_unor_surat_nama_tujuan as KEPADA',
            DB::raw('DATE_FORMAT(surat_berkas.updated_at, "%d %M %Y") as TANGGAL'),
            'surat.arsip_tipe_jumlah as JUMLAH',
            'm_surat_tipe.nama as KETERANGAN'
        )
        ->where('is_first', '1')
        ->Where(function ($query) {

          if ($this->month != '' && $this->code != '') {

                  $query->where('surat_berkas.klasifikasi_code', '=', $this->code);
                  $query->whereMonth('surat_berkas.updated_at', '=', $this->month);

          } elseif ($this->month != '' || $this->month != null) {

                  $query->whereMonth('surat_berkas.updated_at', '=', $this->month);

              } elseif ($this->code != '' || $this->code != null) {

                  $query->where('surat_berkas.klasifikasi_code', '=', $this->code);
                    
              }
      })        
        ->orderBy('surat_berkas.klasifikasi_code', 'asc')
        ->get();
    }

    public function view(): View
    {
        return view('exports.xml', [
            'data' => $this->suratBerkas
        ]);
    } 

}
