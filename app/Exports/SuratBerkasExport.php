<?php

namespace App\Exports;

use Maatwebsite\Excel\Excel;
use App\Exports\SuratBerkasPerCodeSheet;

// use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
// use Illuminate\Contracts\Support\Responsable;

// // use Maatwebsite\Excel\Concerns\FromCollection;
// use Maatwebsite\Excel\Concerns\FromArray;
// // use Maatwebsite\Excel\Concerns\FromQuery;

// use Maatwebsite\Excel\Concerns\WithHeadings;
// use Maatwebsite\Excel\Concerns\ShouldAutoSize;
// use Maatwebsite\Excel\Concerns\WithEvents;
// use Maatwebsite\Excel\Events\AfterSheet;
// use Maatwebsite\Excel\Concerns\WithMapping;
// use Maatwebsite\Excel\Concerns\WithTitle;

use Illuminate\Support\Facades\DB;

class SuratBerkasExport implements WithMultipleSheets
{

    // use Exportable;

    /**
    * It's required to define the fileName within
    * the export class when making use of Responsable.
    */
    // private $fileName = 'daftar_isi.xlsx';

    private $year     = '';
    private $month    = '';
    private $uke2     = '';
    private $uke2Nama     = '';

    public function __construct($year = '', $month = '', $uke2 = '', $uke2Nama = '')
    {
        $this->year     = $year;
        $this->month    = $month;
        $this->uke2     = $uke2;
        $this->uke2Nama     = $uke2Nama;
    }

    /**
     * @return array
     */
    public function sheets(): array
    {
        $sheets = [];

        $code = DB::table("m_surat_klasifikasi")->where("parent_id", 1)->pluck("code");

        for ($i = 0; $i < count($code); $i++) {
            $sheets[] = new SuratBerkasPerCodeSheet($code[$i], $this->year, $this->month, $this->uke2, $this->uke2Nama);
        }

        return $sheets;
    }

}
