<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;
use Illuminate\Support\Facades\DB;

class SuratBerkasPerCodeSheet implements FromArray, WithHeadings, ShouldAutoSize, WithEvents, WithMapping, WithTitle
{
    private $code     = '';
    private $year     = '';
    private $month    = '';
    private $uke2     = '';
    private $uke2Nama     = '';

    private $suratBerkas = [];

    private $noTmp = 4;

    public function __construct($code = '', $year = '', $month = '', $uke2 = '', $uke2Nama = '')
    {
        $this->code     = $code;
        $this->year 	= $year;
        $this->month    = $month;
        $this->uke2     = $uke2;
        $this->uke2Nama 	= $uke2Nama;
    }

    public function array(): array
    {
    	$this->suratBerkas = DB::table('surat_berkas')
        ->join('surat', 'surat.berkas_id', '=', 'surat_berkas.id')
        ->join('inbox', 'inbox.surat_id', '=', 'surat.id')
        ->join('m_surat_sifat', 'm_surat_sifat.id', '=', 'surat.sifat_id')
        ->join('m_surat_tipe', 'm_surat_tipe.id', '=', 'surat.tipe_id')
        ->select(
            'surat_berkas.id as NO. BERKAS',
            'surat_berkas.berkas_no as NO. ITEM ARSIP',
            'surat_berkas.klasifikasi_code as KODE KLASIFIKASI',
            'surat.no_surat_unit_kerja as NO. SURAT',
            'surat.perihal as PERIHAL',
            'inbox.ori_unor_surat_nama_asal as ASAL SURAT',
            'inbox.ori_unor_surat_nama_tujuan as KEPADA',
            DB::raw('DATE_FORMAT(surat.created_at, "%d %M %Y") as TANGGAL'),
            'surat.arsip_tipe_jumlah as JUMLAH',
            'm_surat_tipe.nama as KETERANGAN'
        )
        ->where('is_first', '1')
        ->where('eselon_ii_unor_id', $this->uke2)
	    ->Where(function ($query) {

	    		if ($this->month != '' && $this->code != '' && $this->year != '') {

                    // $query->where('surat_berkas.klasifikasi_code', '=', $this->code);
                	$query->where('surat_berkas.klasifikasi_code', 'LIKE', "%" . $this->code . "%");
                    $query->whereMonth('surat.created_at', '=', $this->month);

                	$query->whereYear('surat.created_at', '=', $this->year);

	    		} elseif ($this->month != '' || $this->month != null) {

                	$query->whereMonth('surat.created_at', '=', $this->month);

                    $query->whereYear('surat.created_at', '=', $this->year);

	            } elseif ($this->code != '' || $this->code != null) {

                	$query->where('surat_berkas.klasifikasi_code', 'LIKE', "%" . $this->code . "%");
	            }
	    })
        ->orderBy('surat_berkas.klasifikasi_code', 'asc')
        ->get()->toArray();

        $code = "NO. BERKAS";
        $klasifikasiCode = 'KODE KLASIFIKASI';

        for ($i = 0; $i < count($this->suratBerkas); $i++) {
            $this->suratBerkas[$i]->$code = $i;

            for ($x = 0; $x < count($this->suratBerkas); $x++) {

                if ( $this->suratBerkas[$x]->$klasifikasiCode == $this->suratBerkas[$i]->$klasifikasiCode ) {
                    $this->suratBerkas[$x]->$code = $i;
                } else {
                    continue;
                }
            }
        }

        $data = $this->suratBerkas;

    	return $data;
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return $this->code;
    }

    public function map($data): array
    {
        $arr = [];

        $noBerkas           = 'NO. BERKAS';
        $noItems            = 'NO. ITEM ARSIP';
        $kodeKlasifikasi    = 'KODE KLASIFIKASI';
        $noSurat            = 'NO. SURAT';
        $perihal            = 'PERIHAL';
        $asalSurat          = 'ASAL SURAT';
        $kepada             = 'KEPADA';
        $tanggal            = 'TANGGAL';
        $jumlah             = 'JUMLAH';
        $keterangan         = 'KETERANGAN';

        $arrTmp = array (
            [
                $data->$noBerkas,
                $data->$noItems,
                $data->$kodeKlasifikasi,
                'Asal Surat',
                ':',
                $data->$asalSurat,
                $data->$tanggal,
                $data->$jumlah . ' Lembar',
                $data->$keterangan
            ],
            ['','','','No. Surat', ':', $data->$noSurat], // empty MEANS new column
            ['','','','Kepada', ':', $data->$kepada],
            ['','','','Hal', ':', $data->$perihal],
            [] // add new row
        );

        $arr = $arrTmp;

        return $arr;
    }

    public function headings(): array
    {
        return [
            [
                'Daftar Isi Berkas ' . $this->uke2Nama . ' ' . $this->year,
            ],
            [
                'NO. BERKAS',
                'NO. ITEM ARSIP',
                'KODE KLASIFIKASI',
                'URAIAN INFORMASI ARSIP',
                ' ',
                ' ',
                'TANGGAL',
                'JUMLAH',
                'KETERANGAN',
            ],
            [
                ' ',
            ],
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $headPrimary = 'A1:I2';

                $numb = count($this->suratBerkas)+3;

                $allRowRecords = 'A3:I'. $numb;
                $allCell = 'A2:AC100';

                // set font size
                $secondHead = 'A2:I2';
                $event->sheet->getDelegate()->getStyle($secondHead)->getFont()->setSize(12);

                $firstHead = 'A1:I1';
                $event->sheet->getDelegate()->getStyle($firstHead)->getFont()->setSize(14);
                $event->sheet->getDelegate()->getStyle('A2:I2')->getFill()->setFillType('solid')->getStartColor()->setARGB('E59D20');

                // set font family
                $event->sheet->getDelegate()->getStyle($allCell)->getFont()->setName('Times New Roman');

                $event->sheet->getDelegate()->getStyle($headPrimary)->getFont()
                ->applyFromArray(
                    array(
                        'name' => 'Calibri',
                        'bold' =>  true,
                    )
                );

				$event->sheet->getStyle($headPrimary)->applyFromArray([
		                    'borders' => [
		                        'allBorders' => [
		                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
		                            'color' => ['argb' => '000000'],
		                        ],
		                    ],
				         ]);

                // merge cell
                $event->sheet->mergeCells('A1:I1');
                $event->sheet->mergeCells('A3:I3');
                $event->sheet->mergeCells('D2:F2');

                // merge with loop
                $noTmpTarget = 4;
                $colTarget = ['A', 'B', 'C', 'G', 'H', 'I'];
                for ($i = 0; $i < count($this->suratBerkas); $i++) {

                    $lastMergeTarget = $noTmpTarget+3;

                    foreach ($colTarget as $key) {

                        $tmpNmb = $key.$noTmpTarget.':'.$key.$lastMergeTarget;
                        $event->sheet->mergeCells($tmpNmb);

                    }

                    $noTmpTarget = $noTmpTarget + 5;

                }

                $lastForBorder = $noTmpTarget-2;
                $forBorderRec = 'A3:I'. $lastForBorder;
                $event->sheet->getStyle($forBorderRec)->applyFromArray([
                            'borders' => [
                                'allBorders' => [
                                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                                    'color' => ['argb' => '000000'],
                                ],
                            ],
                         ]);

                // set no. items
                $klasifikasiCode = 'KODE KLASIFIKASI';
                for ($i = 0; $i < count($this->suratBerkas); $i++) {
                    $tmpCode = '';
                    $getCode = $this->suratBerkas[$i]->$klasifikasiCode;

                    $this->noTmp = $this->noTmp + 4;
                    $forMergeData = 'A'.$this->noTmp.':I'.$this->noTmp;
                    $event->sheet->mergeCells($forMergeData);

                    if ($tmpCode == $getCode) {
                        continue;
                    }  else if ($tmpCode != $getCode) {

                        $tmpCode = $getCode;

                        $value = DB::table('surat_berkas')->where('klasifikasi_code', $tmpCode)->pluck('berkas_no');

                        if ( count($value) != 0 ) {
                            for ($x = 0; $x < count($value); $x++) {
                                $no = $x+4;

                                if ( $no == 5 ) {
                                    $this->noTmp = $no + 4;
                                    $event->sheet->setCellValue('B' . $this->noTmp, $value[$x]);

                                    $noMergeRow = $this->noTmp + 4;
                                    $forMergeDistinctData = 'A'.$noMergeRow.':I'.$noMergeRow;
                                    $event->sheet->mergeCells($forMergeDistinctData);
                                    $event->sheet->getDelegate()->getStyle($forMergeDistinctData)->getFill()->setFillType('solid')->getStartColor()->setARGB('BFBFBF');

                                } else {
                                    $event->sheet->setCellValue('B' . $no, $value[$x]);
                                }

                            }
                        }
                    }

                }

                $event->sheet->getDelegate()->getStyle($allCell)->getAlignment()->setWrapText(true);

                $event->sheet->getDelegate()->getStyle('A1:I1')->getAlignment()->setHorizontal('center');

                $event->sheet->getDelegate()->getStyle('A1:I1')->getAlignment()->setVertical('center');

                $event->sheet->getDelegate()->getStyle($allCell)->getAlignment()->setHorizontal('center');

                $event->sheet->getDelegate()->getStyle('D3:D500')->getAlignment()->setHorizontal('left');

                $event->sheet->getDelegate()->getStyle('F3:F500')->getAlignment()->setHorizontal('left');

                $event->sheet->getDelegate()->getStyle($allCell)->getAlignment()->setVertical('center');

				$event->sheet->getColumnDimension('A')->setAutoSize(false);
                $event->sheet->getColumnDimension('A')->setWidth(10);

				$event->sheet->getColumnDimension('B')->setAutoSize(false);
				$event->sheet->getColumnDimension('B')->setWidth(12);

				$event->sheet->getColumnDimension('C')->setAutoSize(false);
				$event->sheet->getColumnDimension('C')->setWidth(15);

				$event->sheet->getColumnDimension('D')->setAutoSize(false);
				$event->sheet->getColumnDimension('D')->setWidth(10);

				$event->sheet->getColumnDimension('E')->setAutoSize(false);
				$event->sheet->getColumnDimension('E')->setWidth(2);

				$event->sheet->getColumnDimension('F')->setAutoSize(false);
				$event->sheet->getColumnDimension('F')->setWidth(120);

				$event->sheet->getColumnDimension('G')->setAutoSize(false);
				$event->sheet->getColumnDimension('G')->setWidth(21);

				$event->sheet->getColumnDimension('H')->setAutoSize(false);
				$event->sheet->getColumnDimension('H')->setWidth(12);

				$event->sheet->getColumnDimension('I')->setAutoSize(false);
				$event->sheet->getColumnDimension('I')->setWidth(15);

                $event->sheet->getRowDimension(1)->setRowHeight(40);
				$event->sheet->getRowDimension(2)->setRowHeight(35);
            },
        ];
    }

}
