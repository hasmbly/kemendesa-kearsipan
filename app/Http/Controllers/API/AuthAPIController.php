<?php

namespace App\Http\Controllers\API;

use App\Exceptions\Handler;
use App\Http\Controllers\AppBaseController;
use GuzzleHttp\Handler\CurlFactory;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\User;
use Illuminate\Support\Facades\Log;
use InfyOm\Generator\Utils\ResponseUtil;
use Ixudra\Curl\Facades\Curl;
use Response;

class AuthAPIController extends AppBaseController
{
    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/auth/login",
     *      summary="Login.",
     *      tags={"Auth"},
     *      description="Login",
     *      produces={"application/json"},
     *     @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="login",
     *          required=true,
     *          @SWG\Schema(
     *              @SWG\Property(
     *                  property="email_or_username",
     *                  type="string",
     *                  description="email_or_username",
     *                  example="exam@exampl.ex"
     *              ),
     *              @SWG\Property(
     *                  property="password",
     *                  type="password",
     *                  description="password",
     *                  example="12345678"
     *              )
     *
     *           )
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(
     *                      @SWG\Property(
     *                          property="accessToken",
     *                          type="string"
     *                      ),
     *                      @SWG\Property(
     *                          property="tokenType",
     *                          type="string"
     *                      ),
     *                      @SWG\Property(
     *                          property="pnsId",
     *                          type="string"
     *                      ),
     *                      @SWG\Property(
     *                          property="username",
     *                          type="string"
     *                      ),
     *                      @SWG\Property(
     *                          property="email",
     *                          type="string"
     *                      ),
     *                      @SWG\Property(
     *                          property="nama",
     *                          type="string"
     *                      ),
     *                      @SWG\Property(
     *                          property="jenisJabatan",
     *                          type="string"
     *                      ),
     *                      @SWG\Property(
     *                          property="roles",
     *                          type="array",
     *                          @SWG\Items(
     *                              type="string"
     *                          )
     *                      ),
     *                      @SWG\Property(
     *                          property="unorId",
     *                          type="string"
     *                      ),
     *                      @SWG\Property(
     *                          property="unorNama",
     *                          type="string"
     *                      )
     *                  )
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function login(Request $request)
    {
        $request->validate([
            "email_or_username" => "required|string",
            "password" => "required|string"
        ]);

        $result = Curl::to(getenv("URL_SIMPEG") . "/api/auth/signin")
            ->withData(array("usernameOrEmail" => $request->email_or_username, "password" => $request->password))
            ->withHeaders(array('Accept: application/json', 'Content-Type: application/json'))
            ->withResponseHeaders()
            ->asJson(true)
            ->post();

        if(array_key_exists ('error',$result)){
            return Response()->json(ResponseUtil::makeError($result['error']),$result['status']);
        }
        $roles = ['ROLE_ADMIN', 'ROLE_TU', 'ROLE_RECORD_CENTER'];
        $true = false;
        foreach ($result['roles'] as $rol){
            if(in_array($rol,$roles)){
                $true = true;
            }
        }
        if($result['jenisJabatan'] != "STRUKTURAL" && !$true){ return $this->sendError("Forbidden",403); }

        return $this->sendResponse($result,"success");

//        $credentials = \request(['email'=>$request->email,'password'=>$request->password]);
//        $credentials = $request->only('email', 'password');
//        $credentials = request(['email', 'password']);
//
//        if (!Auth::attempt($credentials)){
//            return $this->sendError("Unauthorized",401);
//        }
//
//        $user = Auth::user();
//        $tokenResult = $user->createToken('Personal Access Token');
//        $token = $tokenResult->token;
//        if ($request->remember_me)
//            $token->expires_at = Carbon::now()->addWeeks(1);
//        $token->save();
//        $tkn = [
//            'access_token' => $tokenResult->accessToken,
//            'token_type' => 'Bearer',
////            'expires_at' => Carbon::parse(
////                $tokenResult->token->expires_at
////            )->toDateTimeString()
//        ];
//        return $this->sendResponse($tkn,"Success");
//
//        return $this->sendResponse(["user"=>$user,"token"=>$tkn],"success");
    }


    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/auth/user",
     *      summary="Display the specified User",
     *      tags={"Auth"},
     *      description="Get User",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/User")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string",
     *                  example="Successfully logged out"
     *              )
     *          )
     *      )
     * )
     */
    public function user(Request $request)
    {
//        $result = Curl::to(getenv("URL_SIMPEG") . "api/auth/check-token")
//            ->withHeaders(array('Accept: application/json', 'Content-Type: application/json','Authorization:'.$request->header('Authorization')))
//            ->withResponseHeaders()
//            ->asJson(true)
//            ->get();
//
//        if(array_key_exists ('error',$result)){
//            return Response()->json(ResponseUtil::makeError($result['error']),$result['status']);
//        }

        $resUser = (object) $request->attributes->get('resUser');
//        $result2 = Curl::to(getenv("URL_SIMPEG") . "/api/users/".$result['username'])
//        $result2 = Curl::to(getenv("URL_SIMPEG") . "/api/users/".$resUser->username)
//            ->withHeaders(array('Accept: application/json', 'Content-Type: application/json','Authorization:'.$request->header('Authorization')))
//            ->withResponseHeaders()
//            ->asJson(true)
//            ->get();

//        if(array_key_exists ('error',$result2)){
//            return Response()->json(ResponseUtil::makeError($result2['error']),$result2['status']);
//        }
//
//        return $this->sendResponse($result2,"success");

        return $this->sendResponse($resUser,"success");

//        return $this->sendResponse(Auth::user(),"Success");
    }
}
