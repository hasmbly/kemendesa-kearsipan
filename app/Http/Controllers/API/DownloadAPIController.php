<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;

class DownloadAPIController extends AppBaseController
{
    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/download",
     *      summary="Get a file.",
     *      tags={"Download"},
     *      description="download file",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *     @SWG\Parameter(
     *          name="path",
     *          in="query",
     *          description="path file",
     *          required=true,
     *          type="string"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *      )
     * )
     */
    public function download(Request $request)
    {
//        return Storage::download('storage/ZgbX9pOCVYUSn160KMWtPfs5Z1dZkvgsI4Q4lN1V.png');
        if($request->has('path')) {
            return Storage::download($request->input('path'));
        }else{
            return $this->sendError('path is required');
        }

    }
}
