<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateMasterActivitySifatAPIRequest;
use App\Http\Requests\API\UpdateMasterActivitySifatAPIRequest;
use App\Models\MasterActivitySifat;
use App\Repositories\MasterActivitySifatRepository;
use App\Repositories\MasterActivitySifatRepositoryEloquent;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Response;

/**
 * Class MasterActivitySifatController
 * @package App\Http\Controllers\API
 */

class MasterActivitySifatAPIController extends AppBaseController
{
    /** @var  MasterActivitySifatRepository */
    private $masterActivitySifatRepository;

    public function __construct(MasterActivitySifatRepositoryEloquent $masterActivitySifatRepo)
    {
        $this->masterActivitySifatRepository = $masterActivitySifatRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/masterActivitySifats",
     *      summary="Get a listing of the MasterActivitySifats.",
     *      tags={"MasterActivitySifat"},
     *      description="Get all MasterActivitySifats",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(ref="#/parameters/offset"),
     *      @SWG\Parameter(ref="#/parameters/limit"),
     *      @SWG\Parameter(ref="#/parameters/search"),
     *      @SWG\Parameter(ref="#/parameters/filters"),
     *      @SWG\Parameter(ref="#/parameters/sortedBy"),
     *      @SWG\Parameter(ref="#/parameters/orderBy"),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/MasterActivitySifat")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $masterActivitySifats = $this->masterActivitySifatRepository->pushCriteria(new LimitOffsetCriteria($request));
        if($request->has('filters')){
            $filters = $request->get('filters');
            $filters = explode(";",$filters);
            $masterActivitySifats->scopeQuery(function($query) use ($filters){
                foreach ($filters as $fil){
                    $fil = explode(",",$fil);
                    $whr = collect([trim($fil[0]),trim($fil[1]),trim($fil[2])])->all();
                    $query->where([$whr]);
                }
                return $query;
            });
        }

        return $this->sendResponse($masterActivitySifats->paginate(), 'Master Activity Sifats retrieved successfully');
    }

    /**
     * @param CreateMasterActivitySifatAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/masterActivitySifats",
     *      summary="Store a newly created MasterActivitySifat in storage",
     *      tags={"MasterActivitySifat"},
     *      description="Store MasterActivitySifat",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="MasterActivitySifat that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/MasterActivitySifat")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/MasterActivitySifat"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateMasterActivitySifatAPIRequest $request)
    {
        $input = $request->all();

        $masterActivitySifat = $this->masterActivitySifatRepository->create($input);

        return $this->sendResponse($masterActivitySifat->toArray(), 'Master Activity Sifat saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/masterActivitySifats/{id}",
     *      summary="Display the specified MasterActivitySifat",
     *      tags={"MasterActivitySifat"},
     *      description="Get MasterActivitySifat",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of MasterActivitySifat",
     *          type="string",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/MasterActivitySifat"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var MasterActivitySifat $masterActivitySifat */
        $masterActivitySifat = $this->masterActivitySifatRepository->find($id);

        if (empty($masterActivitySifat)) {
            return $this->sendError('Master Activity Sifat not found');
        }

        return $this->sendResponse($masterActivitySifat->toArray(), 'Master Activity Sifat retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateMasterActivitySifatAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/masterActivitySifats/{id}",
     *      summary="Update the specified MasterActivitySifat in storage",
     *      tags={"MasterActivitySifat"},
     *      description="Update MasterActivitySifat",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of MasterActivitySifat",
     *          type="string",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="MasterActivitySifat that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/MasterActivitySifat")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/MasterActivitySifat"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateMasterActivitySifatAPIRequest $request)
    {
        $input = $request->all();

        /** @var MasterActivitySifat $masterActivitySifat */
        $masterActivitySifat = $this->masterActivitySifatRepository->find($id);

        if (empty($masterActivitySifat)) {
            return $this->sendError('Master Activity Sifat not found');
        }

        $masterActivitySifat = $this->masterActivitySifatRepository->update($input, $id);

        return $this->sendResponse($masterActivitySifat->toArray(), 'MasterActivitySifat updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/masterActivitySifats/{id}",
     *      summary="Remove the specified MasterActivitySifat from storage",
     *      tags={"MasterActivitySifat"},
     *      description="Delete MasterActivitySifat",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of MasterActivitySifat",
     *          type="string",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var MasterActivitySifat $masterActivitySifat */
        $masterActivitySifat = $this->masterActivitySifatRepository->find($id);

        if (empty($masterActivitySifat)) {
            return $this->sendError('Master Activity Sifat not found');
        }

        $masterActivitySifat->delete();

        return $this->sendResponse($id, 'Master Activity Sifat deleted successfully');
    }
}
