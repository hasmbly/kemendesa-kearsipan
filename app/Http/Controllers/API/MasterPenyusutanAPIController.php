<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateMasterPenyusutanAPIRequest;
use App\Http\Requests\API\UpdateMasterPenyusutanAPIRequest;
use App\Models\MasterPenyusutan;
use App\Repositories\MasterPenyusutanRepository;
use App\Repositories\MasterPenyusutanRepositoryEloquent;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Response;

/**
 * Class MasterPenyusutanController
 * @package App\Http\Controllers\API
 */

class MasterPenyusutanAPIController extends AppBaseController
{
    /** @var  MasterPenyusutanRepository */
    private $masterPenyusutanRepository;

    public function __construct(MasterPenyusutanRepositoryEloquent $masterPenyusutanRepo)
    {
        $this->masterPenyusutanRepository = $masterPenyusutanRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/masterPenyusutans",
     *      summary="Get a listing of the MasterPenyusutans.",
     *      tags={"MasterPenyusutan"},
     *      description="Get all MasterPenyusutans",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(ref="#/parameters/offset"),
     *      @SWG\Parameter(ref="#/parameters/limit"),
     *      @SWG\Parameter(ref="#/parameters/search"),
     *      @SWG\Parameter(ref="#/parameters/filters"),
     *      @SWG\Parameter(ref="#/parameters/sortedBy"),
     *      @SWG\Parameter(ref="#/parameters/orderBy"),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/MasterPenyusutan")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $masterPenyusutans = $this->masterPenyusutanRepository->pushCriteria(new LimitOffsetCriteria($request));
        if($request->has('filters')){
            $filters = $request->get('filters');
            $filters = explode(";",$filters);
            $masterPenyusutans->scopeQuery(function($query) use ($filters){
                foreach ($filters as $fil){
                    $fil = explode(",",$fil);
                    $whr = collect([trim($fil[0]),trim($fil[1]),trim($fil[2])])->all();
                    $query->where([$whr]);
                }
                return $query;
            });
        }

        return $this->sendResponse($masterPenyusutans->paginate(), 'Master Penyusutans retrieved successfully');
    }

    /**
     * @param CreateMasterPenyusutanAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/masterPenyusutans",
     *      summary="Store a newly created MasterPenyusutan in storage",
     *      tags={"MasterPenyusutan"},
     *      description="Store MasterPenyusutan",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="MasterPenyusutan that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/MasterPenyusutan")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/MasterPenyusutan"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateMasterPenyusutanAPIRequest $request)
    {
        $input = $request->all();

        $masterPenyusutan = $this->masterPenyusutanRepository->create($input);

        return $this->sendResponse($masterPenyusutan->toArray(), 'Master Penyusutan saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/masterPenyusutans/{id}",
     *      summary="Display the specified MasterPenyusutan",
     *      tags={"MasterPenyusutan"},
     *      description="Get MasterPenyusutan",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of MasterPenyusutan",
     *          type="string",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/MasterPenyusutan"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var MasterPenyusutan $masterPenyusutan */
        $masterPenyusutan = $this->masterPenyusutanRepository->find($id);

        if (empty($masterPenyusutan)) {
            return $this->sendError('Master Penyusutan not found');
        }

        return $this->sendResponse($masterPenyusutan->toArray(), 'Master Penyusutan retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateMasterPenyusutanAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/masterPenyusutans/{id}",
     *      summary="Update the specified MasterPenyusutan in storage",
     *      tags={"MasterPenyusutan"},
     *      description="Update MasterPenyusutan",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of MasterPenyusutan",
     *          type="string",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="MasterPenyusutan that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/MasterPenyusutan")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/MasterPenyusutan"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateMasterPenyusutanAPIRequest $request)
    {
        $input = $request->all();

        /** @var MasterPenyusutan $masterPenyusutan */
        $masterPenyusutan = $this->masterPenyusutanRepository->find($id);

        if (empty($masterPenyusutan)) {
            return $this->sendError('Master Penyusutan not found');
        }

        $masterPenyusutan = $this->masterPenyusutanRepository->update($input, $id);

        return $this->sendResponse($masterPenyusutan->toArray(), 'MasterPenyusutan updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/masterPenyusutans/{id}",
     *      summary="Remove the specified MasterPenyusutan from storage",
     *      tags={"MasterPenyusutan"},
     *      description="Delete MasterPenyusutan",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of MasterPenyusutan",
     *          type="string",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var MasterPenyusutan $masterPenyusutan */
        $masterPenyusutan = $this->masterPenyusutanRepository->find($id);

        if (empty($masterPenyusutan)) {
            return $this->sendError('Master Penyusutan not found');
        }

        $masterPenyusutan->delete();

        return $this->sendResponse($id, 'Master Penyusutan deleted successfully');
    }
}
