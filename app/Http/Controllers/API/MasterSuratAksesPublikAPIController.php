<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateMasterSuratAksesPublikAPIRequest;
use App\Http\Requests\API\UpdateMasterSuratAksesPublikAPIRequest;
use App\Models\MasterSuratAksesPublik;
use App\Repositories\MasterSuratAksesPublikRepository;
use App\Repositories\MasterSuratAksesPublikRepositoryEloquent;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Response;

/**
 * Class MasterSuratAksesPublikController
 * @package App\Http\Controllers\API
 */

class MasterSuratAksesPublikAPIController extends AppBaseController
{
    /** @var  MasterSuratAksesPublikRepository */
    private $masterSuratAksesPublikRepository;

    public function __construct(MasterSuratAksesPublikRepositoryEloquent $masterSuratAksesPublikRepo)
    {
        $this->masterSuratAksesPublikRepository = $masterSuratAksesPublikRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/masterSuratAksesPubliks",
     *      summary="Get a listing of the MasterSuratAksesPubliks.",
     *      tags={"MasterSuratAksesPublik"},
     *      description="Get all MasterSuratAksesPubliks",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(ref="#/parameters/offset"),
     *      @SWG\Parameter(ref="#/parameters/limit"),
     *      @SWG\Parameter(ref="#/parameters/search"),
     *      @SWG\Parameter(ref="#/parameters/filters"),
     *      @SWG\Parameter(ref="#/parameters/sortedBy"),
     *      @SWG\Parameter(ref="#/parameters/orderBy"),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/MasterSuratAksesPublik")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->masterSuratAksesPublikRepository->pushCriteria(new LimitOffsetCriteria($request));
        $masterSuratAksesPubliks = $this->masterSuratAksesPublikRepository->scopeQuery(function ($query){
            return $query;
        });
        if($request->has('filters')){
            $filters = $request->get('filters');
            $filters = explode(";",$filters);
            $masterSuratAksesPubliks->scopeQuery(function($query) use ($filters){
                foreach ($filters as $fil){
                    $fil = explode(",",$fil);
                    $whr = collect([trim($fil[0]),trim($fil[1]),trim($fil[2])])->all();
                    $query->where([$whr]);
                }
                return $query;
            });
        }

        return $this->sendResponse($masterSuratAksesPubliks->paginate(), 'Master Surat Akses Publiks retrieved successfully');
    }

    /**
     * @param CreateMasterSuratAksesPublikAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/masterSuratAksesPubliks",
     *      summary="Store a newly created MasterSuratAksesPublik in storage",
     *      tags={"MasterSuratAksesPublik"},
     *      description="Store MasterSuratAksesPublik",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="MasterSuratAksesPublik that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/MasterSuratAksesPublik")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/MasterSuratAksesPublik"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateMasterSuratAksesPublikAPIRequest $request)
    {
        $input = $request->all();

        $masterSuratAksesPublik = $this->masterSuratAksesPublikRepository->create($input);

        return $this->sendResponse($masterSuratAksesPublik->toArray(), 'Master Surat Akses Publik saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/masterSuratAksesPubliks/{id}",
     *      summary="Display the specified MasterSuratAksesPublik",
     *      tags={"MasterSuratAksesPublik"},
     *      description="Get MasterSuratAksesPublik",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of MasterSuratAksesPublik",
     *          type="string",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/MasterSuratAksesPublik"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var MasterSuratAksesPublik $masterSuratAksesPublik */
        $masterSuratAksesPublik = $this->masterSuratAksesPublikRepository->find($id);

        if (empty($masterSuratAksesPublik)) {
            return $this->sendError('Master Surat Akses Publik not found');
        }

        return $this->sendResponse($masterSuratAksesPublik->toArray(), 'Master Surat Akses Publik retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateMasterSuratAksesPublikAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/masterSuratAksesPubliks/{id}",
     *      summary="Update the specified MasterSuratAksesPublik in storage",
     *      tags={"MasterSuratAksesPublik"},
     *      description="Update MasterSuratAksesPublik",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of MasterSuratAksesPublik",
     *          type="string",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="MasterSuratAksesPublik that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/MasterSuratAksesPublik")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/MasterSuratAksesPublik"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateMasterSuratAksesPublikAPIRequest $request)
    {
        $input = $request->all();

        /** @var MasterSuratAksesPublik $masterSuratAksesPublik */
        $masterSuratAksesPublik = $this->masterSuratAksesPublikRepository->find($id);

        if (empty($masterSuratAksesPublik)) {
            return $this->sendError('Master Surat Akses Publik not found');
        }

        $masterSuratAksesPublik = $this->masterSuratAksesPublikRepository->update($input, $id);

        return $this->sendResponse($masterSuratAksesPublik->toArray(), 'MasterSuratAksesPublik updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/masterSuratAksesPubliks/{id}",
     *      summary="Remove the specified MasterSuratAksesPublik from storage",
     *      tags={"MasterSuratAksesPublik"},
     *      description="Delete MasterSuratAksesPublik",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of MasterSuratAksesPublik",
     *          type="string",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var MasterSuratAksesPublik $masterSuratAksesPublik */
        $masterSuratAksesPublik = $this->masterSuratAksesPublikRepository->find($id);

        if (empty($masterSuratAksesPublik)) {
            return $this->sendError('Master Surat Akses Publik not found');
        }

        $masterSuratAksesPublik->delete();

        return $this->sendResponse($id, 'Master Surat Akses Publik deleted successfully');
    }
}
