<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateMasterSuratArsipKategoriAPIRequest;
use App\Http\Requests\API\UpdateMasterSuratArsipKategoriAPIRequest;
use App\Models\MasterSuratArsipKategori;
use App\Repositories\MasterSuratArsipKategoriRepository;
use App\Repositories\MasterSuratArsipKategoriRepositoryEloquent;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Response;

/**
 * Class MasterSuratArsipKategoriController
 * @package App\Http\Controllers\API
 */

class MasterSuratArsipKategoriAPIController extends AppBaseController
{
    /** @var  MasterSuratArsipKategoriRepository */
    private $masterSuratArsipKategoriRepository;

    public function __construct(MasterSuratArsipKategoriRepositoryEloquent $masterSuratArsipKategoriRepo)
    {
        $this->masterSuratArsipKategoriRepository = $masterSuratArsipKategoriRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/masterSuratArsipKategoris",
     *      summary="Get a listing of the MasterSuratArsipKategoris.",
     *      tags={"MasterSuratArsipKategori"},
     *      description="Get all MasterSuratArsipKategoris",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(ref="#/parameters/offset"),
     *      @SWG\Parameter(ref="#/parameters/limit"),
     *      @SWG\Parameter(ref="#/parameters/search"),
     *      @SWG\Parameter(ref="#/parameters/filters"),
     *      @SWG\Parameter(ref="#/parameters/sortedBy"),
     *      @SWG\Parameter(ref="#/parameters/orderBy"),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/MasterSuratArsipKategori")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->masterSuratArsipKategoriRepository->pushCriteria(new LimitOffsetCriteria($request));
        $masterSuratArsipKategoris = $this->masterSuratArsipKategoriRepository->scopeQuery(function ($query){
            return $query;
        });
        if($request->has('filters')){
            $filters = $request->get('filters');
            $filters = explode(";",$filters);
            $masterSuratArsipKategoris->scopeQuery(function($query) use ($filters){
                foreach ($filters as $fil){
                    $fil = explode(",",$fil);
                    $whr = collect([trim($fil[0]),trim($fil[1]),trim($fil[2])])->all();
                    $query->where([$whr]);
                }
                return $query;
            });
        }

        return $this->sendResponse($masterSuratArsipKategoris->paginate(), 'Master Surat Arsip Kategoris retrieved successfully');
    }

    /**
     * @param CreateMasterSuratArsipKategoriAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/masterSuratArsipKategoris",
     *      summary="Store a newly created MasterSuratArsipKategori in storage",
     *      tags={"MasterSuratArsipKategori"},
     *      description="Store MasterSuratArsipKategori",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="MasterSuratArsipKategori that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/MasterSuratArsipKategori")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/MasterSuratArsipKategori"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateMasterSuratArsipKategoriAPIRequest $request)
    {
        $input = $request->all();

        $masterSuratArsipKategori = $this->masterSuratArsipKategoriRepository->create($input);

        return $this->sendResponse($masterSuratArsipKategori->toArray(), 'Master Surat Arsip Kategori saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/masterSuratArsipKategoris/{id}",
     *      summary="Display the specified MasterSuratArsipKategori",
     *      tags={"MasterSuratArsipKategori"},
     *      description="Get MasterSuratArsipKategori",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of MasterSuratArsipKategori",
     *          type="string",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/MasterSuratArsipKategori"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var MasterSuratArsipKategori $masterSuratArsipKategori */
        $masterSuratArsipKategori = $this->masterSuratArsipKategoriRepository->find($id);

        if (empty($masterSuratArsipKategori)) {
            return $this->sendError('Master Surat Arsip Kategori not found');
        }

        return $this->sendResponse($masterSuratArsipKategori->toArray(), 'Master Surat Arsip Kategori retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateMasterSuratArsipKategoriAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/masterSuratArsipKategoris/{id}",
     *      summary="Update the specified MasterSuratArsipKategori in storage",
     *      tags={"MasterSuratArsipKategori"},
     *      description="Update MasterSuratArsipKategori",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of MasterSuratArsipKategori",
     *          type="string",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="MasterSuratArsipKategori that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/MasterSuratArsipKategori")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/MasterSuratArsipKategori"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateMasterSuratArsipKategoriAPIRequest $request)
    {
        $input = $request->all();

        /** @var MasterSuratArsipKategori $masterSuratArsipKategori */
        $masterSuratArsipKategori = $this->masterSuratArsipKategoriRepository->find($id);

        if (empty($masterSuratArsipKategori)) {
            return $this->sendError('Master Surat Arsip Kategori not found');
        }

        $masterSuratArsipKategori = $this->masterSuratArsipKategoriRepository->update($input, $id);

        return $this->sendResponse($masterSuratArsipKategori->toArray(), 'MasterSuratArsipKategori updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/masterSuratArsipKategoris/{id}",
     *      summary="Remove the specified MasterSuratArsipKategori from storage",
     *      tags={"MasterSuratArsipKategori"},
     *      description="Delete MasterSuratArsipKategori",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of MasterSuratArsipKategori",
     *          type="string",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var MasterSuratArsipKategori $masterSuratArsipKategori */
        $masterSuratArsipKategori = $this->masterSuratArsipKategoriRepository->find($id);

        if (empty($masterSuratArsipKategori)) {
            return $this->sendError('Master Surat Arsip Kategori not found');
        }

        $masterSuratArsipKategori->delete();

        return $this->sendResponse($id, 'Master Surat Arsip Kategori deleted successfully');
    }
}
