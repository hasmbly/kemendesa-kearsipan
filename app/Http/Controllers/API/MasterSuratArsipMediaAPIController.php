<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateMasterSuratArsipMediaAPIRequest;
use App\Http\Requests\API\UpdateMasterSuratArsipMediaAPIRequest;
use App\Models\MasterSuratArsipMedia;
use App\Repositories\MasterSuratArsipMediaRepository;
use App\Repositories\MasterSuratArsipMediaRepositoryEloquent;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Response;

/**
 * Class MasterSuratArsipMediaController
 * @package App\Http\Controllers\API
 */

class MasterSuratArsipMediaAPIController extends AppBaseController
{
    /** @var  MasterSuratArsipMediaRepository */
    private $masterSuratArsipMediaRepository;

    public function __construct(MasterSuratArsipMediaRepositoryEloquent $masterSuratArsipMediaRepo)
    {
        $this->masterSuratArsipMediaRepository = $masterSuratArsipMediaRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/masterSuratArsipMedia",
     *      summary="Get a listing of the MasterSuratArsipMedia.",
     *      tags={"MasterSuratArsipMedia"},
     *      description="Get all MasterSuratArsipMedia",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(ref="#/parameters/offset"),
     *      @SWG\Parameter(ref="#/parameters/limit"),
     *      @SWG\Parameter(ref="#/parameters/search"),
     *      @SWG\Parameter(ref="#/parameters/filters"),
     *      @SWG\Parameter(ref="#/parameters/sortedBy"),
     *      @SWG\Parameter(ref="#/parameters/orderBy"),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/MasterSuratArsipMedia")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->masterSuratArsipMediaRepository->pushCriteria(new LimitOffsetCriteria($request));
        $masterSuratArsipMedia = $this->masterSuratArsipMediaRepository->scopeQuery(function($query){
            return $query;
        });
        if($request->has('filters')){
            $filters = $request->get('filters');
            $filters = explode(";",$filters);
            $masterSuratArsipMedia->scopeQuery(function($query) use ($filters){
                foreach ($filters as $fil){
                    $fil = explode(",",$fil);
                    $whr = collect([trim($fil[0]),trim($fil[1]),trim($fil[2])])->all();
                    $query->where([$whr]);
                }
                return $query;
            });
        }

        return $this->sendResponse($masterSuratArsipMedia->paginate(), 'Master Surat Arsip Media retrieved successfully');
    }

    /**
     * @param CreateMasterSuratArsipMediaAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/masterSuratArsipMedia",
     *      summary="Store a newly created MasterSuratArsipMedia in storage",
     *      tags={"MasterSuratArsipMedia"},
     *      description="Store MasterSuratArsipMedia",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="MasterSuratArsipMedia that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/MasterSuratArsipMedia")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/MasterSuratArsipMedia"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateMasterSuratArsipMediaAPIRequest $request)
    {
        $input = $request->all();

        $masterSuratArsipMedia = $this->masterSuratArsipMediaRepository->create($input);

        return $this->sendResponse($masterSuratArsipMedia->toArray(), 'Master Surat Arsip Media saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/masterSuratArsipMedia/{id}",
     *      summary="Display the specified MasterSuratArsipMedia",
     *      tags={"MasterSuratArsipMedia"},
     *      description="Get MasterSuratArsipMedia",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of MasterSuratArsipMedia",
     *          type="string",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/MasterSuratArsipMedia"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var MasterSuratArsipMedia $masterSuratArsipMedia */
        $masterSuratArsipMedia = $this->masterSuratArsipMediaRepository->find($id);

        if (empty($masterSuratArsipMedia)) {
            return $this->sendError('Master Surat Arsip Media not found');
        }

        return $this->sendResponse($masterSuratArsipMedia->toArray(), 'Master Surat Arsip Media retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateMasterSuratArsipMediaAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/masterSuratArsipMedia/{id}",
     *      summary="Update the specified MasterSuratArsipMedia in storage",
     *      tags={"MasterSuratArsipMedia"},
     *      description="Update MasterSuratArsipMedia",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of MasterSuratArsipMedia",
     *          type="string",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="MasterSuratArsipMedia that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/MasterSuratArsipMedia")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/MasterSuratArsipMedia"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateMasterSuratArsipMediaAPIRequest $request)
    {
        $input = $request->all();

        /** @var MasterSuratArsipMedia $masterSuratArsipMedia */
        $masterSuratArsipMedia = $this->masterSuratArsipMediaRepository->find($id);

        if (empty($masterSuratArsipMedia)) {
            return $this->sendError('Master Surat Arsip Media not found');
        }

        $masterSuratArsipMedia = $this->masterSuratArsipMediaRepository->update($input, $id);

        return $this->sendResponse($masterSuratArsipMedia->toArray(), 'MasterSuratArsipMedia updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/masterSuratArsipMedia/{id}",
     *      summary="Remove the specified MasterSuratArsipMedia from storage",
     *      tags={"MasterSuratArsipMedia"},
     *      description="Delete MasterSuratArsipMedia",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of MasterSuratArsipMedia",
     *          type="string",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var MasterSuratArsipMedia $masterSuratArsipMedia */
        $masterSuratArsipMedia = $this->masterSuratArsipMediaRepository->find($id);

        if (empty($masterSuratArsipMedia)) {
            return $this->sendError('Master Surat Arsip Media not found');
        }

        $masterSuratArsipMedia->delete();

        return $this->sendResponse($id, 'Master Surat Arsip Media deleted successfully');
    }
}
