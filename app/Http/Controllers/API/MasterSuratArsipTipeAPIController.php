<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateMasterSuratArsipTipeAPIRequest;
use App\Http\Requests\API\UpdateMasterSuratArsipTipeAPIRequest;
use App\Models\MasterSuratArsipTipe;
use App\Repositories\MasterSuratArsipTipeRepository;
use App\Repositories\MasterSuratArsipTipeRepositoryEloquent;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Response;

/**
 * Class MasterSuratArsipTipeController
 * @package App\Http\Controllers\API
 */

class MasterSuratArsipTipeAPIController extends AppBaseController
{
    /** @var  MasterSuratArsipTipeRepository */
    private $masterSuratArsipTipeRepository;

    public function __construct(MasterSuratArsipTipeRepositoryEloquent $masterSuratArsipTipeRepo)
    {
        $this->masterSuratArsipTipeRepository = $masterSuratArsipTipeRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/masterSuratArsipTipes",
     *      summary="Get a listing of the MasterSuratArsipTipes.",
     *      tags={"MasterSuratArsipTipe"},
     *      description="Get all MasterSuratArsipTipes",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(ref="#/parameters/offset"),
     *      @SWG\Parameter(ref="#/parameters/limit"),
     *      @SWG\Parameter(ref="#/parameters/search"),
     *      @SWG\Parameter(ref="#/parameters/filters"),
     *      @SWG\Parameter(ref="#/parameters/sortedBy"),
     *      @SWG\Parameter(ref="#/parameters/orderBy"),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/MasterSuratArsipTipe")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $masterSuratArsipTipes = $this->masterSuratArsipTipeRepository->pushCriteria(new LimitOffsetCriteria($request));
        if($request->has('filters')){
            $filters = $request->get('filters');
            $filters = explode(";",$filters);
            $masterSuratArsipTipes->scopeQuery(function($query) use ($filters){
                foreach ($filters as $fil){
                    $fil = explode(",",$fil);
                    $whr = collect([trim($fil[0]),trim($fil[1]),trim($fil[2])])->all();
                    $query->where([$whr]);
                }
                return $query;
            });
        }

        return $this->sendResponse($masterSuratArsipTipes->paginate(), 'Master Surat Arsip Tipes retrieved successfully');
    }

    /**
     * @param CreateMasterSuratArsipTipeAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/masterSuratArsipTipes",
     *      summary="Store a newly created MasterSuratArsipTipe in storage",
     *      tags={"MasterSuratArsipTipe"},
     *      description="Store MasterSuratArsipTipe",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="MasterSuratArsipTipe that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/MasterSuratArsipTipe")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/MasterSuratArsipTipe"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateMasterSuratArsipTipeAPIRequest $request)
    {
        $input = $request->all();

        $masterSuratArsipTipe = $this->masterSuratArsipTipeRepository->create($input);

        return $this->sendResponse($masterSuratArsipTipe->toArray(), 'Master Surat Arsip Tipe saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/masterSuratArsipTipes/{id}",
     *      summary="Display the specified MasterSuratArsipTipe",
     *      tags={"MasterSuratArsipTipe"},
     *      description="Get MasterSuratArsipTipe",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of MasterSuratArsipTipe",
     *          type="string",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/MasterSuratArsipTipe"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var MasterSuratArsipTipe $masterSuratArsipTipe */
        $masterSuratArsipTipe = $this->masterSuratArsipTipeRepository->find($id);

        if (empty($masterSuratArsipTipe)) {
            return $this->sendError('Master Surat Arsip Tipe not found');
        }

        return $this->sendResponse($masterSuratArsipTipe->toArray(), 'Master Surat Arsip Tipe retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateMasterSuratArsipTipeAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/masterSuratArsipTipes/{id}",
     *      summary="Update the specified MasterSuratArsipTipe in storage",
     *      tags={"MasterSuratArsipTipe"},
     *      description="Update MasterSuratArsipTipe",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of MasterSuratArsipTipe",
     *          type="string",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="MasterSuratArsipTipe that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/MasterSuratArsipTipe")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/MasterSuratArsipTipe"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateMasterSuratArsipTipeAPIRequest $request)
    {
        $input = $request->all();

        /** @var MasterSuratArsipTipe $masterSuratArsipTipe */
        $masterSuratArsipTipe = $this->masterSuratArsipTipeRepository->find($id);

        if (empty($masterSuratArsipTipe)) {
            return $this->sendError('Master Surat Arsip Tipe not found');
        }

        $masterSuratArsipTipe = $this->masterSuratArsipTipeRepository->update($input, $id);

        return $this->sendResponse($masterSuratArsipTipe->toArray(), 'MasterSuratArsipTipe updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/masterSuratArsipTipes/{id}",
     *      summary="Remove the specified MasterSuratArsipTipe from storage",
     *      tags={"MasterSuratArsipTipe"},
     *      description="Delete MasterSuratArsipTipe",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of MasterSuratArsipTipe",
     *          type="string",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var MasterSuratArsipTipe $masterSuratArsipTipe */
        $masterSuratArsipTipe = $this->masterSuratArsipTipeRepository->find($id);

        if (empty($masterSuratArsipTipe)) {
            return $this->sendError('Master Surat Arsip Tipe not found');
        }

        $masterSuratArsipTipe->delete();

        return $this->sendResponse($id, 'Master Surat Arsip Tipe deleted successfully');
    }
}
