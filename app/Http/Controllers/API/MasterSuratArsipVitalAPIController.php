<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateMasterSuratArsipVitalAPIRequest;
use App\Http\Requests\API\UpdateMasterSuratArsipVitalAPIRequest;
use App\Models\MasterSuratArsipVital;
use App\Repositories\MasterSuratArsipVitalRepository;
use App\Repositories\MasterSuratArsipVitalRepositoryEloquent;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Response;

/**
 * Class MasterSuratArsipVitalController
 * @package App\Http\Controllers\API
 */

class MasterSuratArsipVitalAPIController extends AppBaseController
{
    /** @var  MasterSuratArsipVitalRepository */
    private $masterSuratArsipVitalRepository;

    public function __construct(MasterSuratArsipVitalRepositoryEloquent $masterSuratArsipVitalRepo)
    {
        $this->masterSuratArsipVitalRepository = $masterSuratArsipVitalRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/masterSuratArsipVitals",
     *      summary="Get a listing of the MasterSuratArsipVitals.",
     *      tags={"MasterSuratArsipVital"},
     *      description="Get all MasterSuratArsipVitals",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(ref="#/parameters/offset"),
     *      @SWG\Parameter(ref="#/parameters/limit"),
     *      @SWG\Parameter(ref="#/parameters/search"),
     *      @SWG\Parameter(ref="#/parameters/filters"),
     *      @SWG\Parameter(ref="#/parameters/sortedBy"),
     *      @SWG\Parameter(ref="#/parameters/orderBy"),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/MasterSuratArsipVital")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->masterSuratArsipVitalRepository->pushCriteria(new LimitOffsetCriteria($request));
        $masterSuratArsipVitals = $this->masterSuratArsipVitalRepository->scopeQuery(function ($query){
            return $query;
        });
        if($request->has('filters')){
            $filters = $request->get('filters');
            $filters = explode(";",$filters);
            $masterSuratArsipVitals->scopeQuery(function($query) use ($filters){
                foreach ($filters as $fil){
                    $fil = explode(",",$fil);
                    $whr = collect([trim($fil[0]),trim($fil[1]),trim($fil[2])])->all();
                    $query->where([$whr]);
                }
                return $query;
            });
        }

        return $this->sendResponse($masterSuratArsipVitals->paginate(), 'Master Surat Arsip Vitals retrieved successfully');
    }

    /**
     * @param CreateMasterSuratArsipVitalAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/masterSuratArsipVitals",
     *      summary="Store a newly created MasterSuratArsipVital in storage",
     *      tags={"MasterSuratArsipVital"},
     *      description="Store MasterSuratArsipVital",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="MasterSuratArsipVital that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/MasterSuratArsipVital")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/MasterSuratArsipVital"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateMasterSuratArsipVitalAPIRequest $request)
    {
        $input = $request->all();

        $masterSuratArsipVital = $this->masterSuratArsipVitalRepository->create($input);

        return $this->sendResponse($masterSuratArsipVital->toArray(), 'Master Surat Arsip Vital saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/masterSuratArsipVitals/{id}",
     *      summary="Display the specified MasterSuratArsipVital",
     *      tags={"MasterSuratArsipVital"},
     *      description="Get MasterSuratArsipVital",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of MasterSuratArsipVital",
     *          type="string",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/MasterSuratArsipVital"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var MasterSuratArsipVital $masterSuratArsipVital */
        $masterSuratArsipVital = $this->masterSuratArsipVitalRepository->find($id);

        if (empty($masterSuratArsipVital)) {
            return $this->sendError('Master Surat Arsip Vital not found');
        }

        return $this->sendResponse($masterSuratArsipVital->toArray(), 'Master Surat Arsip Vital retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateMasterSuratArsipVitalAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/masterSuratArsipVitals/{id}",
     *      summary="Update the specified MasterSuratArsipVital in storage",
     *      tags={"MasterSuratArsipVital"},
     *      description="Update MasterSuratArsipVital",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of MasterSuratArsipVital",
     *          type="string",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="MasterSuratArsipVital that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/MasterSuratArsipVital")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/MasterSuratArsipVital"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateMasterSuratArsipVitalAPIRequest $request)
    {
        $input = $request->all();

        /** @var MasterSuratArsipVital $masterSuratArsipVital */
        $masterSuratArsipVital = $this->masterSuratArsipVitalRepository->find($id);

        if (empty($masterSuratArsipVital)) {
            return $this->sendError('Master Surat Arsip Vital not found');
        }

        $masterSuratArsipVital = $this->masterSuratArsipVitalRepository->update($input, $id);

        return $this->sendResponse($masterSuratArsipVital->toArray(), 'MasterSuratArsipVital updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/masterSuratArsipVitals/{id}",
     *      summary="Remove the specified MasterSuratArsipVital from storage",
     *      tags={"MasterSuratArsipVital"},
     *      description="Delete MasterSuratArsipVital",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of MasterSuratArsipVital",
     *          type="string",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var MasterSuratArsipVital $masterSuratArsipVital */
        $masterSuratArsipVital = $this->masterSuratArsipVitalRepository->find($id);

        if (empty($masterSuratArsipVital)) {
            return $this->sendError('Master Surat Arsip Vital not found');
        }

        $masterSuratArsipVital->delete();

        return $this->sendResponse($id, 'Master Surat Arsip Vital deleted successfully');
    }
}
