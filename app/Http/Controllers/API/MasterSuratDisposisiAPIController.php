<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateMasterSuratDisposisiAPIRequest;
use App\Http\Requests\API\UpdateMasterSuratDisposisiAPIRequest;
use App\Models\MasterSuratDisposisi;
use App\Repositories\MasterSuratDisposisiRepository;
use App\Repositories\MasterSuratDisposisiRepositoryEloquent;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Log;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class MasterSuratDisposisiController
 * @package App\Http\Controllers\API
 */

class MasterSuratDisposisiAPIController extends AppBaseController
{
    /** @var  MasterSuratDisposisiRepository */
    private $masterSuratDisposisiRepository;

    public function __construct(MasterSuratDisposisiRepositoryEloquent $masterSuratDisposisiRepo)
    {
        $this->masterSuratDisposisiRepository = $masterSuratDisposisiRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/masterSuratDisposisis",
     *      summary="Get a listing of the MasterSuratDisposisis.",
     *      tags={"MasterSuratDisposisi"},
     *      description="Get all MasterSuratDisposisis",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(ref="#/parameters/offset"),
     *      @SWG\Parameter(ref="#/parameters/limit"),
     *      @SWG\Parameter(ref="#/parameters/search"),
     *      @SWG\Parameter(ref="#/parameters/filters"),
     *      @SWG\Parameter(ref="#/parameters/sortedBy"),
     *      @SWG\Parameter(ref="#/parameters/orderBy"),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/MasterSuratDisposisi")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->masterSuratDisposisiRepository->pushCriteria(new RequestCriteria($request));
        $this->masterSuratDisposisiRepository->pushCriteria(new LimitOffsetCriteria($request));

        $masterSuratDisposisis = $this->masterSuratDisposisiRepository->scopeQuery(function($query){
            return $query;
        });
        if($request->has('filters')){
            $filters = $request->get('filters');
            $filters = explode(";",$filters);
            $masterSuratDisposisis->scopeQuery(function($query) use ($filters){
                foreach ($filters as $fil){
                    $fil = explode(",",$fil);
                    $whr = collect([trim($fil[0]),trim($fil[1]),trim($fil[2])])->all();
                    $query->where([$whr]);
                }
                return $query;
            });
        }

        return $this->sendResponse($masterSuratDisposisis->paginate(), 'Master Surat Disposisis retrieved successfully');
    }

    /**
     * @param CreateMasterSuratDisposisiAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/masterSuratDisposisis",
     *      summary="Store a newly created MasterSuratDisposisi in storage",
     *      tags={"MasterSuratDisposisi"},
     *      description="Store MasterSuratDisposisi",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="MasterSuratDisposisi that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/MasterSuratDisposisi")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/MasterSuratDisposisi"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateMasterSuratDisposisiAPIRequest $request)
    {
        $input = $request->all();

        $masterSuratDisposisi = $this->masterSuratDisposisiRepository->create($input);

        return $this->sendResponse($masterSuratDisposisi->toArray(), 'Master Surat Disposisi saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/masterSuratDisposisis/{id}",
     *      summary="Display the specified MasterSuratDisposisi",
     *      tags={"MasterSuratDisposisi"},
     *      description="Get MasterSuratDisposisi",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of MasterSuratDisposisi",
     *          type="string",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/MasterSuratDisposisi"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var MasterSuratDisposisi $masterSuratDisposisi */
        $masterSuratDisposisi = $this->masterSuratDisposisiRepository->find($id);

        if (empty($masterSuratDisposisi)) {
            return $this->sendError('Master Surat Disposisi not found');
        }

        return $this->sendResponse($masterSuratDisposisi->toArray(), 'Master Surat Disposisi retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateMasterSuratDisposisiAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/masterSuratDisposisis/{id}",
     *      summary="Update the specified MasterSuratDisposisi in storage",
     *      tags={"MasterSuratDisposisi"},
     *      description="Update MasterSuratDisposisi",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of MasterSuratDisposisi",
     *          type="string",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="MasterSuratDisposisi that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/MasterSuratDisposisi")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/MasterSuratDisposisi"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateMasterSuratDisposisiAPIRequest $request)
    {
        $input = $request->all();

        /** @var MasterSuratDisposisi $masterSuratDisposisi */
        $masterSuratDisposisi = $this->masterSuratDisposisiRepository->find($id);

        if (empty($masterSuratDisposisi)) {
            return $this->sendError('Master Surat Disposisi not found');
        }

        $masterSuratDisposisi = $this->masterSuratDisposisiRepository->update($input, $id);

        return $this->sendResponse($masterSuratDisposisi->toArray(), 'MasterSuratDisposisi updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/masterSuratDisposisis/{id}",
     *      summary="Remove the specified MasterSuratDisposisi from storage",
     *      tags={"MasterSuratDisposisi"},
     *      description="Delete MasterSuratDisposisi",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of MasterSuratDisposisi",
     *          type="string",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var MasterSuratDisposisi $masterSuratDisposisi */
        $masterSuratDisposisi = $this->masterSuratDisposisiRepository->find($id);

        if (empty($masterSuratDisposisi)) {
            return $this->sendError('Master Surat Disposisi not found');
        }

        $masterSuratDisposisi->delete();

        return $this->sendResponse($id, 'Master Surat Disposisi deleted successfully');
    }
}
