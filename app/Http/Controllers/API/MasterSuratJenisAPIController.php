<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateMasterSuratJenisAPIRequest;
use App\Http\Requests\API\UpdateMasterSuratJenisAPIRequest;
use App\Models\MasterSuratJenis;
use App\Repositories\MasterSuratJenisRepository;
use App\Repositories\SuratJenisRepositoryEloquent;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Yajra\DataTables\DataTables;

/**
 * Class MasterSuratJenisController
 * @package App\Http\Controllers\API
 */

class MasterSuratJenisAPIController extends AppBaseController
{
    /** @var  MasterSuratJenisRepository */
    private $masterSuratJenisRepository;

//    public function __construct(MasterSuratJenisRepository $masterSuratJenisRepo)
    public function __construct(SuratJenisRepositoryEloquent $masterSuratJenisRepo)
    {
        $this->masterSuratJenisRepository = $masterSuratJenisRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/masterSuratJenis",
     *      summary="Get a listing of the MasterSuratJenis.",
     *      tags={"MasterSuratJenis"},
     *      description="Get all MasterSuratJenis",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(ref="#/parameters/offset"),
     *      @SWG\Parameter(ref="#/parameters/limit"),
     *      @SWG\Parameter(ref="#/parameters/search"),
     *      @SWG\Parameter(ref="#/parameters/filters"),
     *      @SWG\Parameter(ref="#/parameters/sortedBy"),
     *      @SWG\Parameter(ref="#/parameters/orderBy"),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/MasterSuratJenis")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->masterSuratJenisRepository->pushCriteria(new RequestCriteria($request));
        $this->masterSuratJenisRepository->pushCriteria(new LimitOffsetCriteria($request));
        $masterSuratJenis = $this->masterSuratJenisRepository->scopeQuery(function($query){
            return $query;
        });
        if($request->has('filters')){
            $filters = $request->get('filters');
            Log::debug($filters);
            $filters = explode(";",$filters);
            $masterSuratJenis->scopeQuery(function($query) use ($filters){
                foreach ($filters as $fil){
                    $fil = explode(",",$fil);
                    $whr = collect([trim($fil[0]),trim($fil[1]),trim($fil[2])])->all();
                    $query->where([$whr]);
                }
                return $query;
            });
        }

//        return $this->sendResponse($masterSuratJenis->toArray(), 'Master Surat Jenis retrieved successfully');
        return $this->sendResponse($masterSuratJenis->paginate(), 'Master Surat Jenis retrieved successfully');
    }

    /**
     * @param CreateMasterSuratJenisAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/masterSuratJenis",
     *      summary="Store a newly created MasterSuratJenis in storage",
     *      tags={"MasterSuratJenis"},
     *      description="Store MasterSuratJenis",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="MasterSuratJenis that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/MasterSuratJenis")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/MasterSuratJenis"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateMasterSuratJenisAPIRequest $request)
    {
        $input = $request->all();

        $masterSuratJenis = $this->masterSuratJenisRepository->create($input);

        return $this->sendResponse($masterSuratJenis->toArray(), 'Master Surat Jenis saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/masterSuratJenis/{id}",
     *      summary="Display the specified MasterSuratJenis",
     *      tags={"MasterSuratJenis"},
     *      description="Get MasterSuratJenis",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of MasterSuratJenis",
     *          type="string",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/MasterSuratJenis"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var MasterSuratJenis $masterSuratJenis */
        $masterSuratJenis = $this->masterSuratJenisRepository->find($id);

//        if (empty($masterSuratJenis)) {
        if (!$masterSuratJenis) {
            return $this->sendError('Master Surat Jenis not found');
        }

        return $this->sendResponse($masterSuratJenis->toArray(), 'Master Surat Jenis retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateMasterSuratJenisAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/masterSuratJenis/{id}",
     *      summary="Update the specified MasterSuratJenis in storage",
     *      tags={"MasterSuratJenis"},
     *      description="Update MasterSuratJenis",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of MasterSuratJenis",
     *          type="string",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="MasterSuratJenis that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/MasterSuratJenis")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/MasterSuratJenis"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateMasterSuratJenisAPIRequest $request)
    {
        $input = $request->all();

        /** @var MasterSuratJenis $masterSuratJenis */
        $masterSuratJenis = $this->masterSuratJenisRepository->find($id);

        if (empty($masterSuratJenis)) {
            return $this->sendError('Master Surat Jenis not found');
        }

        $masterSuratJenis = $this->masterSuratJenisRepository->update($input, $id);

        return $this->sendResponse($masterSuratJenis->toArray(), 'MasterSuratJenis updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/masterSuratJenis/{id}",
     *      summary="Remove the specified MasterSuratJenis from storage",
     *      tags={"MasterSuratJenis"},
     *      description="Delete MasterSuratJenis",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of MasterSuratJenis",
     *          type="string",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var MasterSuratJenis $masterSuratJenis */
        $masterSuratJenis = $this->masterSuratJenisRepository->find($id);

        if (empty($masterSuratJenis)) {
            return $this->sendError('Master Surat Jenis not found');
        }

        $masterSuratJenis->delete();

        return $this->sendResponse($id, 'Master Surat Jenis deleted successfully');
    }
}
