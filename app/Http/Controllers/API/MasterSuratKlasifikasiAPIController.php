<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Models\MasterSuratKlasifikasi;
use App\Http\Controllers\AppBaseController;

use App\Http\Requests\API\CreateMasterSuratKlasifikasiAPIRequest;
use App\Http\Requests\API\UpdateMasterSuratKlasifikasiAPIRequest;

use App\Repositories\MasterSuratKlasifikasiRepository;
use App\Repositories\MasterSuratKlasifikasiRepositoryEloquent;

use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Illuminate\Support\Facades\DB;
use Response;

/**
 * Class MasterSuratKlasifikasiController
 * @package App\Http\Controllers\API
 */

class MasterSuratKlasifikasiAPIController extends AppBaseController
{
    /** @var  MasterSuratKlasifikasiRepository */
    private $masterSuratKlasifikasiRepository;

    public function __construct(MasterSuratKlasifikasiRepositoryEloquent $masterSuratKlasifikasiRepo)
    {
        $this->masterSuratKlasifikasiRepository = $masterSuratKlasifikasiRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/masterSuratKlasifikasis",
     *      summary="Get a listing of the MasterSuratKlasifikasis.",
     *      tags={"MasterSuratKlasifikasi"},
     *      description="Get all MasterSuratKlasifikasis",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(ref="#/parameters/offset"),
     *      @SWG\Parameter(ref="#/parameters/limit"),
     *      @SWG\Parameter(ref="#/parameters/search"),
     *      @SWG\Parameter(ref="#/parameters/filters"),
     *      @SWG\Parameter(ref="#/parameters/sortedBy"),
     *      @SWG\Parameter(ref="#/parameters/orderBy"),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/MasterSuratKlasifikasi")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $masterSuratKlasifikasis = $this->masterSuratKlasifikasiRepository
        ->with('penyusutan')
        ->with('klasifikasiChild.penyusutan');
        // ->withCount('klasifikasiChild.isExpandable');

        $masterSuratKlasifikasis->pushCriteria(new LimitOffsetCriteria($request));
        if($request->has('filters')){
            $filters = $request->get('filters');
            $filters = explode(";",$filters);
            $masterSuratKlasifikasis->scopeQuery(function($query) use ($filters){
                foreach ($filters as $fil){
                    $fil = explode(",",$fil);
                    $whr = collect([trim($fil[0]),trim($fil[1]),trim($fil[2])])->all();
                    $query->where([$whr]);
                }
                return $query;
            });
        }

        return $this->sendResponse($masterSuratKlasifikasis->paginate(), 'Master Surat Klasifikasis retrieved successfully');
    }

    /**
     * @param CreateMasterSuratKlasifikasiAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/masterSuratKlasifikasis",
     *      summary="Store a newly created MasterSuratKlasifikasi in storage",
     *      tags={"MasterSuratKlasifikasi"},
     *      description="Store MasterSuratKlasifikasi",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="MasterSuratKlasifikasi that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/MasterSuratKlasifikasi")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/MasterSuratKlasifikasi"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateMasterSuratKlasifikasiAPIRequest $request)
    {
        $input = $request->all();

        $masterSuratKlasifikasi = $this->masterSuratKlasifikasiRepository->create($input);

        return $this->sendResponse($masterSuratKlasifikasi->toArray(), 'Master Surat Klasifikasi saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/masterSuratKlasifikasis/{id}",
     *      summary="Display the specified MasterSuratKlasifikasi",
     *      tags={"MasterSuratKlasifikasi"},
     *      description="Get MasterSuratKlasifikasi",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of MasterSuratKlasifikasi",
     *          type="string",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/MasterSuratKlasifikasi"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var MasterSuratKlasifikasi $masterSuratKlasifikasi */
        $masterSuratKlasifikasi = $this->masterSuratKlasifikasiRepository
            ->with('penyusutan')
            ->with('klasifikasiChild.penyusutan')
            ->find($id);

        if (empty($masterSuratKlasifikasi)) {
            return $this->sendError('Master Surat Klasifikasi not found');
        }

        return $this->sendResponse($masterSuratKlasifikasi->toArray(), 'Master Surat Klasifikasi retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateMasterSuratKlasifikasiAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/masterSuratKlasifikasis/{id}",
     *      summary="Update the specified MasterSuratKlasifikasi in storage",
     *      tags={"MasterSuratKlasifikasi"},
     *      description="Update MasterSuratKlasifikasi",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of MasterSuratKlasifikasi",
     *          type="string",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="MasterSuratKlasifikasi that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/MasterSuratKlasifikasi")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/MasterSuratKlasifikasi"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateMasterSuratKlasifikasiAPIRequest $request)
    {
        $input = $request->all();

        /** @var MasterSuratKlasifikasi $masterSuratKlasifikasi */
        $masterSuratKlasifikasi = $this->masterSuratKlasifikasiRepository->find($id);

        if (empty($masterSuratKlasifikasi)) {
            return $this->sendError('Master Surat Klasifikasi not found');
        }

        $masterSuratKlasifikasi = $this->masterSuratKlasifikasiRepository->update($input, $id);

        return $this->sendResponse($masterSuratKlasifikasi->toArray(), 'MasterSuratKlasifikasi updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/masterSuratKlasifikasis/{id}",
     *      summary="Remove the specified MasterSuratKlasifikasi from storage",
     *      tags={"MasterSuratKlasifikasi"},
     *      description="Delete MasterSuratKlasifikasi",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of MasterSuratKlasifikasi",
     *          type="string",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {

        // check before delete, if found child with parent_id is exist -> cannot deleted.
        $checkMasterSuratKlasifikasi = DB::table("m_surat_klasifikasi")->where("parent_id", $id)->pluck("parent_id");

        if ( count($checkMasterSuratKlasifikasi) > 0 ) {
            return $this->sendError('Maaf masih terdapat child klasifikasi');
        }
     
        /** @var MasterSuratKlasifikasi $masterSuratKlasifikasi */
        $masterSuratKlasifikasi = $this->masterSuratKlasifikasiRepository->find($id);

        if ( empty($masterSuratKlasifikasi) ) {
            return $this->sendError('Master Surat Klasifikasi not found');
        }

        // else go deleted()
        $masterSuratKlasifikasi->delete();

        return $this->sendResponse($id, 'Master Surat Klasifikasi deleted successfully');
    }
}
