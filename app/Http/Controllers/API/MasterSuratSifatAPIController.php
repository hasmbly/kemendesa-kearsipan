<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateMasterSuratSifatAPIRequest;
use App\Http\Requests\API\UpdateMasterSuratSifatAPIRequest;
use App\Models\MasterSuratSifat;
use App\Repositories\MasterSuratSifatRepository;
use App\Repositories\MasterSuratSifatRepositoryEloquent;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Log;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class MasterSuratSifatController
 * @package App\Http\Controllers\API
 */

class MasterSuratSifatAPIController extends AppBaseController
{
    /** @var  MasterSuratSifatRepository */
    private $masterSuratSifatRepository;

    public function __construct(MasterSuratSifatRepositoryEloquent $masterSuratSifatRepo)
    {
        $this->masterSuratSifatRepository = $masterSuratSifatRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/masterSuratSifats",
     *      summary="Get a listing of the MasterSuratSifats.",
     *      tags={"MasterSuratSifat"},
     *      description="Get all MasterSuratSifats",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(ref="#/parameters/offset"),
     *      @SWG\Parameter(ref="#/parameters/limit"),
     *      @SWG\Parameter(ref="#/parameters/search"),
     *      @SWG\Parameter(ref="#/parameters/filters"),
     *      @SWG\Parameter(ref="#/parameters/sortedBy"),
     *      @SWG\Parameter(ref="#/parameters/orderBy"),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/MasterSuratSifat")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->masterSuratSifatRepository->pushCriteria(new RequestCriteria($request));
        $this->masterSuratSifatRepository->pushCriteria(new LimitOffsetCriteria($request));
        $masterSuratSifats = $this->masterSuratSifatRepository->scopeQuery(function($query){
            return $query;
        });
        if($request->has('filters')){
            $filters = $request->get('filters');
            $filters = explode(";",$filters);
            $masterSuratSifats->scopeQuery(function($query) use ($filters){
                foreach ($filters as $fil){
                    $fil = explode(",",$fil);
                    $whr = collect([trim($fil[0]),trim($fil[1]),trim($fil[2])])->all();
                    $query->where([$whr]);
                }
                return $query;
            });
        }

        return $this->sendResponse($masterSuratSifats->paginate(), 'Master Surat Sifats retrieved successfully');
    }

    /**
     * @param CreateMasterSuratSifatAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/masterSuratSifats",
     *      summary="Store a newly created MasterSuratSifat in storage",
     *      tags={"MasterSuratSifat"},
     *      description="Store MasterSuratSifat",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="MasterSuratSifat that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/MasterSuratSifat")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/MasterSuratSifat"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateMasterSuratSifatAPIRequest $request)
    {
        $input = $request->all();

        $masterSuratSifat = $this->masterSuratSifatRepository->create($input);

        return $this->sendResponse($masterSuratSifat->toArray(), 'Master Surat Sifat saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/masterSuratSifats/{id}",
     *      summary="Display the specified MasterSuratSifat",
     *      tags={"MasterSuratSifat"},
     *      description="Get MasterSuratSifat",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of MasterSuratSifat",
     *          type="string",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/MasterSuratSifat"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var MasterSuratSifat $masterSuratSifat */
        $masterSuratSifat = $this->masterSuratSifatRepository->find($id);

        if (empty($masterSuratSifat)) {
            return $this->sendError('Master Surat Sifat not found');
        }

        return $this->sendResponse($masterSuratSifat->toArray(), 'Master Surat Sifat retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateMasterSuratSifatAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/masterSuratSifats/{id}",
     *      summary="Update the specified MasterSuratSifat in storage",
     *      tags={"MasterSuratSifat"},
     *      description="Update MasterSuratSifat",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of MasterSuratSifat",
     *          type="string",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="MasterSuratSifat that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/MasterSuratSifat")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/MasterSuratSifat"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateMasterSuratSifatAPIRequest $request)
    {
        $input = $request->all();

        /** @var MasterSuratSifat $masterSuratSifat */
        $masterSuratSifat = $this->masterSuratSifatRepository->find($id);

        if (empty($masterSuratSifat)) {
            return $this->sendError('Master Surat Sifat not found');
        }

        $masterSuratSifat = $this->masterSuratSifatRepository->update($input, $id);

        return $this->sendResponse($masterSuratSifat->toArray(), 'MasterSuratSifat updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/masterSuratSifats/{id}",
     *      summary="Remove the specified MasterSuratSifat from storage",
     *      tags={"MasterSuratSifat"},
     *      description="Delete MasterSuratSifat",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of MasterSuratSifat",
     *          type="string",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var MasterSuratSifat $masterSuratSifat */
        $masterSuratSifat = $this->masterSuratSifatRepository->find($id);

        if (empty($masterSuratSifat)) {
            return $this->sendError('Master Surat Sifat not found');
        }

        $masterSuratSifat->delete();

        return $this->sendResponse($id, 'Master Surat Sifat deleted successfully');
    }
}
