<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateMasterSuratTipeAPIRequest;
use App\Http\Requests\API\UpdateMasterSuratTipeAPIRequest;
use App\Models\MasterSuratTipe;
use App\Repositories\MasterSuratTipeRepository;
use App\Repositories\MasterSuratTipeRepositoryEloquent;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Response;

/**
 * Class MasterSuratTipeController
 * @package App\Http\Controllers\API
 */

class MasterSuratTipeAPIController extends AppBaseController
{
    /** @var  MasterSuratTipeRepository */
    private $masterSuratTipeRepository;

    public function __construct(MasterSuratTipeRepositoryEloquent $masterSuratTipeRepo)
    {
        $this->masterSuratTipeRepository = $masterSuratTipeRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/masterSuratTipes",
     *      summary="Get a listing of the MasterSuratTipes.",
     *      tags={"MasterSuratTipe"},
     *      description="Get all MasterSuratTipes",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(ref="#/parameters/offset"),
     *      @SWG\Parameter(ref="#/parameters/limit"),
     *      @SWG\Parameter(ref="#/parameters/search"),
     *      @SWG\Parameter(ref="#/parameters/filters"),
     *      @SWG\Parameter(ref="#/parameters/sortedBy"),
     *      @SWG\Parameter(ref="#/parameters/orderBy"),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/MasterSuratTipe")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->masterSuratTipeRepository->pushCriteria(new LimitOffsetCriteria($request));
        $masterSuratTipes = $this->masterSuratTipeRepository->scopeQuery(function ($query){
            return $query;
        });
        if($request->has('filters')){
            $filters = $request->get('filters');
            $filters = explode(";",$filters);
            $masterSuratTipes->scopeQuery(function($query) use ($filters){
                foreach ($filters as $fil){
                    $fil = explode(",",$fil);
                    $whr = collect([trim($fil[0]),trim($fil[1]),trim($fil[2])])->all();
                    $query->where([$whr]);
                }
                return $query;
            });
        }

        return $this->sendResponse($masterSuratTipes->paginate(), 'Master Surat Tipes retrieved successfully');
    }

    /**
     * @param CreateMasterSuratTipeAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/masterSuratTipes",
     *      summary="Store a newly created MasterSuratTipe in storage",
     *      tags={"MasterSuratTipe"},
     *      description="Store MasterSuratTipe",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="MasterSuratTipe that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/MasterSuratTipe")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/MasterSuratTipe"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateMasterSuratTipeAPIRequest $request)
    {
        $input = $request->all();

        $masterSuratTipe = $this->masterSuratTipeRepository->create($input);

        return $this->sendResponse($masterSuratTipe->toArray(), 'Master Surat Tipe saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/masterSuratTipes/{id}",
     *      summary="Display the specified MasterSuratTipe",
     *      tags={"MasterSuratTipe"},
     *      description="Get MasterSuratTipe",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of MasterSuratTipe",
     *          type="string",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/MasterSuratTipe"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var MasterSuratTipe $masterSuratTipe */
        $masterSuratTipe = $this->masterSuratTipeRepository->find($id);

        if (empty($masterSuratTipe)) {
            return $this->sendError('Master Surat Tipe not found');
        }

        return $this->sendResponse($masterSuratTipe->toArray(), 'Master Surat Tipe retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateMasterSuratTipeAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/masterSuratTipes/{id}",
     *      summary="Update the specified MasterSuratTipe in storage",
     *      tags={"MasterSuratTipe"},
     *      description="Update MasterSuratTipe",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of MasterSuratTipe",
     *          type="string",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="MasterSuratTipe that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/MasterSuratTipe")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/MasterSuratTipe"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateMasterSuratTipeAPIRequest $request)
    {
        $input = $request->all();

        /** @var MasterSuratTipe $masterSuratTipe */
        $masterSuratTipe = $this->masterSuratTipeRepository->find($id);

        if (empty($masterSuratTipe)) {
            return $this->sendError('Master Surat Tipe not found');
        }

        $masterSuratTipe = $this->masterSuratTipeRepository->update($input, $id);

        return $this->sendResponse($masterSuratTipe->toArray(), 'MasterSuratTipe updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/masterSuratTipes/{id}",
     *      summary="Remove the specified MasterSuratTipe from storage",
     *      tags={"MasterSuratTipe"},
     *      description="Delete MasterSuratTipe",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of MasterSuratTipe",
     *          type="string",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var MasterSuratTipe $masterSuratTipe */
        $masterSuratTipe = $this->masterSuratTipeRepository->find($id);

        if (empty($masterSuratTipe)) {
            return $this->sendError('Master Surat Tipe not found');
        }

        $masterSuratTipe->delete();

        return $this->sendResponse($id, 'Master Surat Tipe deleted successfully');
    }
}
