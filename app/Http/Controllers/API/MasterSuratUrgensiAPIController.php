<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateMasterSuratUrgensiAPIRequest;
use App\Http\Requests\API\UpdateMasterSuratUrgensiAPIRequest;
use App\Models\MasterSuratUrgensi;
use App\Repositories\MasterSuratUrgensiRepository;
use App\Repositories\MasterSuratUrgensiRepositoryEloquent;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Response;

/**
 * Class MasterSuratUrgensiController
 * @package App\Http\Controllers\API
 */

class MasterSuratUrgensiAPIController extends AppBaseController
{
    /** @var  MasterSuratUrgensiRepository */
    private $masterSuratUrgensiRepository;

    public function __construct(MasterSuratUrgensiRepositoryEloquent $masterSuratUrgensiRepo)
    {
        $this->masterSuratUrgensiRepository = $masterSuratUrgensiRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/masterSuratUrgensis",
     *      summary="Get a listing of the MasterSuratUrgensis.",
     *      tags={"MasterSuratUrgensi"},
     *      description="Get all MasterSuratUrgensis",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(ref="#/parameters/offset"),
     *      @SWG\Parameter(ref="#/parameters/limit"),
     *      @SWG\Parameter(ref="#/parameters/search"),
     *      @SWG\Parameter(ref="#/parameters/filters"),
     *      @SWG\Parameter(ref="#/parameters/sortedBy"),
     *      @SWG\Parameter(ref="#/parameters/orderBy"),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/MasterSuratUrgensi")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $masterSuratUrgensis = $this->masterSuratUrgensiRepository->pushCriteria(new LimitOffsetCriteria($request));
        if($request->has('filters')){
            $filters = $request->get('filters');
            $filters = explode(";",$filters);
            $masterSuratUrgensis->scopeQuery(function($query) use ($filters){
                foreach ($filters as $fil){
                    $fil = explode(",",$fil);
                    $whr = collect([trim($fil[0]),trim($fil[1]),trim($fil[2])])->all();
                    $query->where([$whr]);
                }
                return $query;
            });
        }


        return $this->sendResponse($masterSuratUrgensis->paginate(), 'Master Surat Urgensis retrieved successfully');
    }

    /**
     * @param CreateMasterSuratUrgensiAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/masterSuratUrgensis",
     *      summary="Store a newly created MasterSuratUrgensi in storage",
     *      tags={"MasterSuratUrgensi"},
     *      description="Store MasterSuratUrgensi",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="MasterSuratUrgensi that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/MasterSuratUrgensi")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/MasterSuratUrgensi"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateMasterSuratUrgensiAPIRequest $request)
    {
        $input = $request->all();

        $masterSuratUrgensi = $this->masterSuratUrgensiRepository->create($input);

        return $this->sendResponse($masterSuratUrgensi->toArray(), 'Master Surat Urgensi saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/masterSuratUrgensis/{id}",
     *      summary="Display the specified MasterSuratUrgensi",
     *      tags={"MasterSuratUrgensi"},
     *      description="Get MasterSuratUrgensi",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of MasterSuratUrgensi",
     *          type="string",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/MasterSuratUrgensi"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var MasterSuratUrgensi $masterSuratUrgensi */
        $masterSuratUrgensi = $this->masterSuratUrgensiRepository->find($id);

        if (empty($masterSuratUrgensi)) {
            return $this->sendError('Master Surat Urgensi not found');
        }

        return $this->sendResponse($masterSuratUrgensi->toArray(), 'Master Surat Urgensi retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateMasterSuratUrgensiAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/masterSuratUrgensis/{id}",
     *      summary="Update the specified MasterSuratUrgensi in storage",
     *      tags={"MasterSuratUrgensi"},
     *      description="Update MasterSuratUrgensi",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of MasterSuratUrgensi",
     *          type="string",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="MasterSuratUrgensi that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/MasterSuratUrgensi")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/MasterSuratUrgensi"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateMasterSuratUrgensiAPIRequest $request)
    {
        $input = $request->all();

        /** @var MasterSuratUrgensi $masterSuratUrgensi */
        $masterSuratUrgensi = $this->masterSuratUrgensiRepository->find($id);

        if (empty($masterSuratUrgensi)) {
            return $this->sendError('Master Surat Urgensi not found');
        }

        $masterSuratUrgensi = $this->masterSuratUrgensiRepository->update($input, $id);

        return $this->sendResponse($masterSuratUrgensi->toArray(), 'MasterSuratUrgensi updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/masterSuratUrgensis/{id}",
     *      summary="Remove the specified MasterSuratUrgensi from storage",
     *      tags={"MasterSuratUrgensi"},
     *      description="Delete MasterSuratUrgensi",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of MasterSuratUrgensi",
     *          type="string",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var MasterSuratUrgensi $masterSuratUrgensi */
        $masterSuratUrgensi = $this->masterSuratUrgensiRepository->find($id);

        if (empty($masterSuratUrgensi)) {
            return $this->sendError('Master Surat Urgensi not found');
        }

        $masterSuratUrgensi->delete();

        return $this->sendResponse($id, 'Master Surat Urgensi deleted successfully');
    }
}
