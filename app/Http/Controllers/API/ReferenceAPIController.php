<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\AppBaseController;
use App\Models\MasterActivitySifat;
use App\Models\MasterPenyusutan;
use App\Models\MasterSuratAksesPublik;
use App\Models\MasterSuratArsipKategori;
use App\Models\MasterSuratArsipMedia;
use App\Models\MasterSuratArsipTipe;
use App\Models\MasterSuratArsipVital;
use App\Models\MasterSuratDisposisi;
use App\Models\MasterSuratJenis;
use App\Models\MasterSuratKlasifikasi;
use App\Models\MasterSuratSifat;
use App\Models\MasterSuratTipe;
use App\Models\MasterSuratUrgensi;
use App\Models\SuratBerkas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\DB;
use Ixudra\Curl\Facades\Curl;


class ReferenceAPIController extends AppBaseController
{
    private function reference($model, $request)
    {
        $limit = $request->has('limit') ? $request->input('limit') : 20;
        $offset = $request->has('offset') ? $request->input('offset') : 0;

        $data = $model::select("id", "nama as text")->orderBy("nama", "asc");
        $q = $request->has('search') ? trim($request->input('search')) : "";
        if ($q) {
            $data->where("nama", "like", '%' . $q . '%');
        }
        $total = $data->count();
        $data = $data->skip($offset)->limit($limit)->get();

        return ['items' => $data, 'total' => $total, 'offset' => $offset];
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/reference/uke2BerkasCount",
     *      summary="Get all suratBerkasCount under unor eseleon II",
     *      tags={"Reference"},
     *      description="Get all suratBerkasCount under unor eseleon II",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},         
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              ref="#/definitions/Reference"
     *          )
     *      )
     * )
     */
    public function refGetUke2BerkasCount(Request $request) 
    {
        
        $result = Curl::to(getenv("URL_SIMPEG") . "/api/unor/uk/list")
            ->withHeaders(array('Accept: application/json', 'Content-Type: application/json'))
            ->withResponseHeaders()
            ->asJson(true)
            ->get();

        foreach ($result as $key => $value) {

            $countBerkas = DB::table('inbox as t1')
            ->join('sent as t2', 't2.surat_id', '=', 't1.surat_id')
            ->join('surat as t3', 't3.id', '=', 't2.surat_id')
            ->where('t3.berkas_id', '<>', 'null')
            ->where('t2.eselon_ii_unor_id', '=', $result[$key]['id'])
            ->distinct('t3.id')
            ->count('t3.id');

            $result[$key]["totalBerkas"] = $countBerkas;
        }

        // sorting algorithm
        $tmp;
        foreach ($result as $key1 => $value) {
            foreach ($result as $key2 => $value) {
                if ($result[$key1]['totalBerkas'] > $result[$key2]['totalBerkas']) {
                    $tmp = $result[$key1];
                    $result[$key1] = $result[$key2];
                    $result[$key2] = $tmp;
                }
            }
        }

        return Response::json($result);
    }    
    
    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/reference/chart",
     *      summary="Get a Chart.",
     *      tags={"Reference"},
     *      description="Get Chart",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="year",
     *          description="year chart",
     *          type="string",
     *          required=true,
     *          in="query"
     *      ),
     *      @SWG\Parameter(
     *          name="unorId",
     *          description="unorId",
     *          type="string",
     *          required=false,
     *          in="query"
     *      ),      
     *      @SWG\Parameter(
     *          name="is_uke2",
     *          description="0 or 1",
     *          type="integer",
     *          required=false,
     *          in="query"
     *      ),           
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              ref="#/definitions/Reference"
     *          )
     *      )
     * )
     */
    public function refChart(Request $request)
    {

        $userDetail         = $request->get('resUser');

        // $uke2Id             = $userDetail['uke2Id'];
        $jwtUnorId             = $userDetail['unorId'];
        // $jenisJabatan       = $userDetail['jenisJabatan'];

        $year                = $request->input('year');        
        $unorId              = $request->input('unorId');        
        $is_uke2             = $request->input('is_uke2');

        $unorField = "";     

        $resData    = [];
        $labels     = [
                        "Jan",
                        "Feb",
                        "Mar",
                        "Apr",
                        "Mei",
                        "Jun",
                        "Jul",
                        "Agust",
                        "Sept",
                        "Okt",
                        "Nov",
                        "Des"
                     ];

        $dataLabel  = [
                        "inbox",
                        "sent",
                        "berkaskan" // surat yg sudah di berkaskan
                    ];

        $summarize = [];

        $tmpData = [];
        $tmpSum  = [];
        foreach ($dataLabel as $key) {
            
            $tmpDataRes = [];
            $tblName = '';
            $tmpSumCount = 0;
            $bcColor = [];
            $bdColor = [];

            if ($key == 'inbox') {
                array_push($bcColor, "rgba(255, 99, 132, 0.2)");
                array_push($bdColor, "rgba(255, 99, 132, 1)");
            } else if ($key == 'sent') {
                array_push($bcColor, "rgba(54, 162, 235, 0.2)");
                array_push($bdColor, "rgba(54, 162, 235, 1)");                
            } else if ($key == 'berkaskan') {
                array_push($bcColor, "rgba(255, 206, 86, 0.2)");
                array_push($bdColor, "rgba(255, 206, 86, 1)");                                
            }

            $tmpData["label"] = $key;
            $tmpData["backgroundColor"] = $bcColor;
            $tmpData["borderColor"] = $bdColor;
            $tmpData["borderWidth"] = 1;

            if ($key == 'berkaskan') { 
                $tblName = 'surat_berkas'; 
            } else {
                $tblName = $key; 
            }
            for ($i = 1; $i <= count($labels); $i++) {

                // for surat_berkas
                if ($tblName == 'surat_berkas') {

                    // if empty
                    if ($is_uke2 == 0 && $unorId == "") {
                        // for inbox
                        $count1 = DB::table("inbox")
                        ->join('surat', 'surat.id', '=', 'inbox.surat_id')
                        ->where('surat.berkas_id', '<>', 'null')
                        ->whereMonth('surat.created_at', '=', $i)
                        ->whereYear('surat.created_at', '=', $year)

                        ->distinct('surat_id')
                        ->count('surat_id');

                        // for sent
                        $count2 = DB::table("sent")
                        ->join('surat', 'surat.id', '=', 'sent.surat_id')
                        ->where('surat.berkas_id', '<>', 'null')
                        ->whereMonth('surat.created_at', '=', $i)
                        ->whereYear('surat.created_at', '=', $year)

                        ->distinct('surat_id')
                        ->count('surat_id');

                        $count = $count1 + $count2;                    

                        array_push($tmpDataRes, $count);    

                        $tmpSumCount = $tmpSumCount + $count;
                    } else {
                        // for inbox
                        $count1 = DB::table("inbox")
                        ->join('surat', 'surat.id', '=', 'inbox.surat_id')
                        ->where('surat.berkas_id', '<>', 'null')
                        ->whereMonth('surat.created_at', '=', $i)
                        ->whereYear('surat.created_at', '=', $year)

                        // ->where('inbox.ori_unor_surat_id_tujuan', '=', $unorId)
                        ->where(function ($query) use( $jwtUnorId, $is_uke2, $unorField ) {

                            $unorField = '.ori_unor_surat_id_tujuan';
                            
                            $query->where('inbox' . $unorField, '=', $jwtUnorId);
                        
                        })
                        ->distinct('surat_id')
                        ->count('surat_id');

                        // for sent
                        $count2 = DB::table("sent")
                        ->join('surat', 'surat.id', '=', 'sent.surat_id')
                        ->where('surat.berkas_id', '<>', 'null')
                        ->whereMonth('surat.created_at', '=', $i)
                        ->whereYear('surat.created_at', '=', $year)

                        // ->where('sent.ori_unor_surat_id_asal', '=', $unorId) 
                        ->where(function ($query) use( $unorId, $is_uke2, $unorField ) {

                            if ($is_uke2 == 1) {
                                $unorField = '.eselon_ii_unor_id';
                            } else {
                                $unorField = '.ori_unor_surat_id_asal';
                            }
                            
                            $query->where('sent' . $unorField, '=', $unorId);
                        
                        })
                        ->distinct('surat_id')
                        ->count('surat_id');

                        $count = $count1 + $count2;                    

                        array_push($tmpDataRes, $count);    

                        $tmpSumCount = $tmpSumCount + $count;
                    }

                } else {

                    if ($is_uke2 == 0 && $unorId == "") {
                        $count = DB::table($tblName)
                        ->join('surat', 'surat.id', '=', $tblName.'.surat_id')
                        ->whereMonth('surat.created_at', '=', $i)
                        ->whereYear('surat.created_at', '=', $year)
                        ->distinct('surat_id')
                        ->count('surat_id');

                        array_push($tmpDataRes, $count);

                        $tmpSumCount = $tmpSumCount + $count;                        
                    } else {
                        $count = DB::table($tblName)
                        ->join('surat', 'surat.id', '=', $tblName.'.surat_id')
                        ->whereMonth('surat.created_at', '=', $i)
                        ->whereYear('surat.created_at', '=', $year)

                        // ->where($tblName.'.ori_unor_surat_id_asal', '=', $unorId)
                        ->where(function ($query) use( $tblName, $unorId, $jwtUnorId, $is_uke2, $unorField ) {

                            if ($is_uke2 == 1) {
                                if ($tblName == "sent") {
                                    $query->where($tblName.'.eselon_ii_unor_id', '=', $unorId);
                                } else if ($tblName == "inbox") {
                                    $query->where($tblName.'.ori_unor_surat_id_tujuan', '=', $jwtUnorId);
                                }
                            } else if ($is_uke2 == 0) {
                                if ($tblName == "sent") {
                                    $query->where($tblName.'.ori_unor_surat_id_asal', '=', $unorId);
                                } else if ($tblName == "inbox") {
                                    $query->where($tblName.'.ori_unor_surat_id_tujuan', '=', $jwtUnorId);
                                }                                  
                            }
                        })
                        ->distinct('surat_id')
                        ->count('surat_id');

                        array_push($tmpDataRes, $count);

                        $tmpSumCount = $tmpSumCount + $count;
                    }
                }

            }

            $tmpData["data"] = $tmpDataRes;
            array_push($resData, $tmpData);

            $summarize[$tblName] = $tmpSumCount;
            // array_push($summarize, $tmpSum);
        }

        $data = [
            "data"  => $resData,
            "labels" => $labels,
            "current_year" => $year,
            "summarize" => $summarize
        ];

        return Response::json($data);
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/reference/suratJenis",
     *      summary="Get a reference of the MasterSuratJenis.",
     *      tags={"Reference"},
     *      description="Get reference MasterSuratJenis",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(ref="#/parameters/offset"),
     *      @SWG\Parameter(ref="#/parameters/limit"),
     *      @SWG\Parameter(ref="#/parameters/search"),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              ref="#/definitions/Reference"
     *          )
     *      )
     * )
     */
    public function refSuratJenis(Request $request)
    {
        $data = Self::reference(MasterSuratJenis::class,$request);
        return Response::json($data);
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/reference/suratSifat",
     *      summary="Get a reference of the MasterSuratSifat.",
     *      tags={"Reference"},
     *      description="Get reference MasterSuratSifat",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(ref="#/parameters/offset"),
     *      @SWG\Parameter(ref="#/parameters/limit"),
     *      @SWG\Parameter(ref="#/parameters/search"),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              ref="#/definitions/Reference"
     *          )
     *      )
     * )
     */
    public function refSuratSifat(Request $request)
    {
        $data = Self::reference(MasterSuratSifat::class,$request);
        return Response::json($data);
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/reference/suratDisposisi",
     *      summary="Get a reference of the MasterSuratDisposisi.",
     *      tags={"Reference"},
     *      description="Get reference MasterSuratDisposisi",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(ref="#/parameters/offset"),
     *      @SWG\Parameter(ref="#/parameters/limit"),
     *      @SWG\Parameter(ref="#/parameters/search"),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              ref="#/definitions/Reference"
     *          )
     *      )
     * )
     */
    public function refSuratDisposisi(Request $request)
    {
        $data = Self::reference(MasterSuratDisposisi::class,$request);
        return Response::json($data);
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/reference/suratTipe",
     *      summary="Get a reference of the MasterSuratTipe.",
     *      tags={"Reference"},
     *      description="Get reference MasterSuratTipe",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(ref="#/parameters/offset"),
     *      @SWG\Parameter(ref="#/parameters/limit"),
     *      @SWG\Parameter(ref="#/parameters/search"),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              ref="#/definitions/Reference"
     *          )
     *      )
     * )
     */
    public function refSuratTipe(Request $request)
    {
        $data = Self::reference(MasterSuratTipe::class,$request);
        return Response::json($data);
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/reference/suratUrgensi",
     *      summary="Get a reference of the MasterSuratUrgensi.",
     *      tags={"Reference"},
     *      description="Get reference MasterSuratUrgensi",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(ref="#/parameters/offset"),
     *      @SWG\Parameter(ref="#/parameters/limit"),
     *      @SWG\Parameter(ref="#/parameters/search"),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              ref="#/definitions/Reference"
     *          )
     *      )
     * )
     */
    public function refSuratUrgensi(Request $request)
    {
        $data = Self::reference(MasterSuratUrgensi::class,$request);
        return Response::json($data);
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/reference/suratAksesPublik",
     *      summary="Get a reference of the MasterSuratAksesPublik.",
     *      tags={"Reference"},
     *      description="Get reference MasterSuratAksesPublik",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(ref="#/parameters/offset"),
     *      @SWG\Parameter(ref="#/parameters/limit"),
     *      @SWG\Parameter(ref="#/parameters/search"),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              ref="#/definitions/Reference"
     *          )
     *      )
     * )
     */
    public function refSuratAksesPublik(Request $request)
    {
        $data = Self::reference(MasterSuratAksesPublik::class,$request);
        return Response::json($data);
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/reference/suratArsipKategori",
     *      summary="Get a reference of the MasterSuratArsipKategori.",
     *      tags={"Reference"},
     *      description="Get reference MasterSuratArsipKategori",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(ref="#/parameters/offset"),
     *      @SWG\Parameter(ref="#/parameters/limit"),
     *      @SWG\Parameter(ref="#/parameters/search"),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              ref="#/definitions/Reference"
     *          )
     *      )
     * )
     */
    public function refSuratArsipKategori(Request $request)
    {
        $data = Self::reference(MasterSuratArsipKategori::class,$request);
        return Response::json($data);
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/reference/suratArsipMedia",
     *      summary="Get a reference of the MasterSuratArsipMedia.",
     *      tags={"Reference"},
     *      description="Get reference MasterSuratArsipMedia",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(ref="#/parameters/offset"),
     *      @SWG\Parameter(ref="#/parameters/limit"),
     *      @SWG\Parameter(ref="#/parameters/search"),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              ref="#/definitions/Reference"
     *          )
     *      )
     * )
     */
    public function refSuratArsipMedia(Request $request)
    {
        $data = Self::reference(MasterSuratArsipMedia::class,$request);
        return Response::json($data);
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/reference/suratArsipVital",
     *      summary="Get a reference of the MasterSuratArsipVital.",
     *      tags={"Reference"},
     *      description="Get reference MasterSuratArsipVital",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(ref="#/parameters/offset"),
     *      @SWG\Parameter(ref="#/parameters/limit"),
     *      @SWG\Parameter(ref="#/parameters/search"),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              ref="#/definitions/Reference"
     *          )
     *      )
     * )
     */
    public function refSuratArsipVital(Request $request)
    {
        $data = Self::reference(MasterSuratArsipVital::class,$request);
        return Response::json($data);
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/reference/suratArsipTipe",
     *      summary="Get a reference of the MasterSuratArsipTipe.",
     *      tags={"Reference"},
     *      description="Get reference MasterSuratArsipTipe",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(ref="#/parameters/offset"),
     *      @SWG\Parameter(ref="#/parameters/limit"),
     *      @SWG\Parameter(ref="#/parameters/search"),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              ref="#/definitions/Reference"
     *          )
     *      )
     * )
     */
    public function refSuratArsipTipe(Request $request)
    {
        $data = Self::reference(MasterSuratArsipTipe::class,$request);
        return Response::json($data);
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/reference/penyusutan",
     *      summary="Get a reference of the MasterPenyusutan.",
     *      tags={"Reference"},
     *      description="Get reference MasterPenyusutan",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(ref="#/parameters/offset"),
     *      @SWG\Parameter(ref="#/parameters/limit"),
     *      @SWG\Parameter(ref="#/parameters/search"),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              ref="#/definitions/Reference"
     *          )
     *      )
     * )
     */
    public function refPenyusutan(Request $request)
    {
        $data = Self::reference(MasterPenyusutan::class,$request);
        return Response::json($data);
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/reference/suratKlasifikasi",
     *      summary="Get a reference of the MasterSuratKlasifikasi.",
     *      tags={"Reference"},
     *      description="Get reference MasterSuratKlasifikasi",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(ref="#/parameters/offset"),
     *      @SWG\Parameter(ref="#/parameters/limit"),
     *      @SWG\Parameter(ref="#/parameters/search"),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              ref="#/definitions/Reference"
     *          )
     *      )
     * )
     */
    public function refSuratKlasifikasi(Request $request)
    {
//        $data = Self::reference(MasterPenyusutan::class,$request);
//        return Response::json($data);

        $limit = $request->has('limit') ? $request->input('limit') : 20;
        $offset = $request->has('offset') ? $request->input('offset') : 0;

        $data = MasterSuratKlasifikasi::select("id", "code as text","nama")->orderBy("code", "asc");
        $q = $request->has('search') ? trim($request->input('search')) : "";
        if ($q) {
            $data->where("code", "like", '%' . $q . '%')->orWhere("nama", "like", '%' . $q . '%');
        }

        $total = $data->count();
        $data = $data->skip($offset)->limit($limit)->get();

        return Response::json(['items' => $data, 'total' => $total, 'offset' => $offset]);
    }


     
//     public function refSuratKlasifikasiTree(Request $request)
//     {
// //        $data = Self::reference(MasterPenyusutan::class,$request);
// //        return Response::json($data);

//         $limit = $request->has('limit') ? $request->input('limit') : 20;
//         $offset = $request->has('offset') ? $request->input('offset') : 0;

//         $data = MasterSuratKlasifikasi::select("id", "code as text","nama")->orderBy("code", "desc");
//         $q = $request->has('search') ? trim($request->input('search')) : "";
//         if ($q) {
//             $data->where("code", "like", '%' . $q . '%')->orWhere("nama", "like", '%' . $q . '%');
//         }

//         $total = $data->count();
//         $data = $data->skip($offset)->limit($limit)->get();

//         return Response::json(['items' => $data, 'total' => $total, 'offset' => $offset]);
//     }


    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/reference/activitySifat",
     *      summary="Get a reference of the MasterActivitySifat.",
     *      tags={"Reference"},
     *      description="Get reference MasterActivitySifat",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(ref="#/parameters/offset"),
     *      @SWG\Parameter(ref="#/parameters/limit"),
     *      @SWG\Parameter(ref="#/parameters/search"),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              ref="#/definitions/Reference"
     *          )
     *      )
     * )
     */
    public function refActivitySifat(Request $request)
    {
        $data = Self::reference(MasterActivitySifat::class,$request);
        return Response::json($data);
    }



    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/reference/suratBerkas",
     *      summary="Get a reference of the SuratBerkas No.",
     *      tags={"Reference"},
     *      description="Get reference SuratBerkas No",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *     @SWG\Parameter(ref="#/parameters/offset"),
     *      @SWG\Parameter(ref="#/parameters/limit"),
     *      @SWG\Parameter(ref="#/parameters/search"),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              ref="#/definitions/Reference"
     *          )
     *      )
     * )
     */
    public function refSuratBerkas(Request $request)
    {
//        $data = Self::reference(MasterPenyusutan::class,$request);
//        return Response::json($data);


        $limit = $request->has('limit') ? $request->input('limit') : "";
        // $offset = $request->has('offset') ? $request->input('offset') : 0;

        // $data = SuratBerkas::select("id", "berkas_nama as text","berkas_no", "deskripsi")->orderBy("berkas_no", "asc");
        // $q = $request->has('search') ? trim($request->input('search')) : "";
        // if ($q != "" || $q != null) {
        //     $data->where("berkas_nama", "like", '%' . $q . '%')->orWhere("berkas_no", "like", '%' . $q . '%');
        // }
        // $total = $data->count();
        // $data = $data->get();
        // $data = $data->skip($offset)->limit($limit)->get();

        $data = DB::table("surat_berkas")->select("id", "berkas_nama as text", "berkas_no", "deskripsi")
                ->orderBy("berkas_no", "asc");
        if ($limit != "" || $limit != null) {
            $data = $data->Limit($limit);
        }                
        $data = $data->get();                

        return Response::json([
            'items' => $data, 
            // 'total' => $total,
            // 'offset' => $offset
            ]);
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/reference/suratBerkasIncrementNo/{id}",
     *      summary="Get a reference of the SuratBerkas.",
     *      tags={"Reference"},
     *      description="Get reference SuratBerkas",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *     @SWG\Parameter(
     *          name="id",
     *          description="id of klasifikasi",
     *          type="string",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="no",
     *                  type="integer"
     *              ),
     *          )
     *      )
     * )
     */
    public function refSuratBerkasIncrementNo($id)
    {
//        $count = SuratBerkas::where('klasifikasi_id',$id)->count();
        $res = SuratBerkas::where('klasifikasi_id',$id)->max('berkas_no');
//        $no = 0;
//        if($count == $res){
//            $no = $res;
//        }else{
//            $no = $res;
//        }
        Log::debug($res);
        return Response::json(['no' => $res+1]);
    }
}
