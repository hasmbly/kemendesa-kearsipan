<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateSuratAPIRequest;
use App\Http\Requests\API\UpdateSuratAPIRequest;
use App\Models\Surat;
use App\Models\SuratActivityFile;
use App\Models\SuratActivityTujuan;
use App\Repositories\SuratRepository;
use App\Repositories\SuratRepositoryEloquent;
use App\Services\SimpegRefService;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Log\Logger;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use phpseclib\Crypt\TripleDES;
use Prettus\Repository\Criteria\RequestCriteria;
use Ramsey\Uuid\Uuid;
use Response;
use Illuminate\Database\QueryException;
use App\Mail\MailtrapExample;
use Illuminate\Support\Facades\Mail;


/**
 * Class SuratController
 * @package App\Http\Controllers\API
 */

class SuratAPIController extends AppBaseController
{
    /** @var  SuratRepository */
    private $suratRepository;

    public function __construct(SuratRepositoryEloquent $suratRepo)
    {
        $this->suratRepository = $suratRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/surats",
     *      summary="Get a listing of the Surats.",
     *      tags={"Surat"},
     *      description="Get all Surats",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *     @SWG\Parameter(
     *          name="peruntukan",
     *          in="query",
     *          description="peruntukan of Surat",
     *          required=true,
     *          type="array",
     *          @SWG\Items(
     *              type="integer",
     *              enum={"0 masuk","1 keluar"}
     *          )
     *      ),
     *      @SWG\Parameter(ref="#/parameters/offset"),
     *      @SWG\Parameter(ref="#/parameters/limit"),
     *      @SWG\Parameter(ref="#/parameters/search"),
     *      @SWG\Parameter(ref="#/parameters/filters"),
     *      @SWG\Parameter(ref="#/parameters/sortedBy"),
     *      @SWG\Parameter(ref="#/parameters/orderBy"),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Surat")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $surats = $this->suratRepository
        ->with('jenis')
        ->with('berkas')
        ->withCount('sumActivity');

        $surats->pushCriteria(new RequestCriteria($request));
        $surats->pushCriteria(new LimitOffsetCriteria($request));

        $surats->scopeQuery(function ($query) use ($request){

    	// inbox
    	if ($request->get('peruntukan') == 0) {

        // get tokenInfo
    	$userDetail = $request->get('resUser');
        $oriUnor = $userDetail['unorId'];

        $query->with(["from" => function($q) use ($oriUnor) {
                $q->where('inbox.ori_unor_surat_id_tujuan', '=', $oriUnor);
                $q->where('inbox.is_read', '=', 0);
        }]);

        // $query->with('is_read');
        $surat_inbox = DB::table('inbox')->where('ori_unor_surat_id_tujuan', $userDetail['unorId'])->pluck('surat_id');
        // }
    	if (count($surat_inbox) != 0 ) {
      	return $query->orWhere(function($query) use ($surat_inbox){
    	    for ($i=0;$i<count($surat_inbox);$i++) {
            $query->orWhere('id', $surat_inbox[$i]);
    			}
    	  	});
    	} else { return $query->Where('peruntukan', 3); }

    	}

    	// sent
    	if ($request->get('peruntukan') == 1) {

        $query->with('to');

        // get tokenInfo
    	$userDetail = $request->get('resUser');
        if ($userDetail['roles'][0] == 'ROLE_TU') {
            $surat_sent = DB::table('sent')->where('eselon_ii_unor_id', $userDetail['uke2Id'])->pluck('surat_id');
        } else {
            $surat_sent = DB::table('sent')->where('ori_unor_surat_id_asal', $userDetail['unorId'])->pluck('surat_id');
        }
    	if (count($surat_sent) != 0) {
            return $query->orWhere(function($query) use ($surat_sent){
                for ($i=0;$i<count($surat_sent);$i++) {
                    $query->orWhere('id', $surat_sent[$i]);
                    }
            	});
            }
        }

        if($request->has('filters')){
            $filters = $request->get('filters');
            $filters = explode(";",$filters);
            $surats->scopeQuery(function ($query) use ($filters){
                foreach ($filters as $filter){
                    $filter = explode(",",$filter);

                    $key = trim($filter[0]);
                    $operator = trim($filter[1]);
                    $value = trim($filter[2]);

                    if ($key == "is_read")
                    {
                     $query->with(["from" => function($q) use ($operator, $value) {
                         $q->where('inbox.is_read', $operator, $value);
                         }]);
                     }

                    // $where = $operator ." ". $value;
                    $query->where($key, $operator, $value);
                }
                return $query;
            });
        }

	});


        $results = $surats->paginate();

        $userInfo   = $request->get('resUser');

        foreach ($results as $key => $value) {

            $activity_type  = 0;
            $idUnor         = '';
            $suratActId     = '';

            if ($userInfo['roles'][0] == 'ROLE_TU') {
                $idUnor = $userInfo['uke2Id'];
            } else {
                $idUnor = $userInfo['unorId'];
            }

            // get surat_activity_id
            $getSurActId = DB::table('surat_activity_tujuan')
            ->where('surat_id', $results[$key]['id'])
            ->where('tujuan_tembusan_id', $idUnor)
            ->pluck('surat_activity_id');

            if ( count($getSurActId) != 0 ) {
                $suratActId = $getSurActId[0];
            }

            // get activity_type
            $getActType = DB::table('surat_activity')
            ->where('id', $suratActId)
            ->pluck('activity_type');

            if ( count($getActType) != 0 ) {
                $activity_type = $getActType[0];
            }

            $results[$key]['activity_type'] = $activity_type;

        }

        return $this->sendResponse($results, 'Surats retrieved successfully');
    }

    /**
     * @param CreateSuratAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/surats",
     *      summary="Store a newly created Surat in storage",
     *      tags={"Surat"},
     *      description="Store Surat",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Surat that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/SuratSave")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Surat"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateSuratAPIRequest $request)
    {

    // $emailName = [];

	$getUnor = $request->get('resUser');

        DB::beginTransaction();
        try {
            $input = $request->all();

            $surat = $this->suratRepository->create($input);

            $suratActivity = $surat->suratActivity()->create($request->input('surat_activity'));

            if($request->has('surat_activity.surat_activity_tujuan')) {
                $tujuans = json_decode($request->input('surat_activity.surat_activity_tujuan'));
                $arrTujuan = [];
                foreach ($tujuans as $tujuanObj) {
                    $tujuan = (array) $tujuanObj;
                    $tujuan = Arr::add($tujuan, 'surat_id', $surat->id);
                    $tujuan = Arr::add($tujuan, 'asal_unit_kerja_id', $getUnor['unorId']);
                    $tujuan = Arr::add($tujuan, 'asal_unit_kerja_nama', $getUnor['unorNama']);
                    $tujuan = Arr::add($tujuan, 'tujuan_tembusan_status', 0);
                    $tujuan = Arr::add($tujuan, 'tujuan_tembusan_id', $tujuanObj->value);
                    $tujuan = Arr::add($tujuan, 'tujuan_tembusan_nama', $tujuanObj->text);

                    $tujuan = Arr::add($tujuan, 'pejabat', $tujuanObj->pejabat);
                    $tujuan = Arr::add($tujuan, 'email', $tujuanObj->email);

                    $arrTujuan[] = $tujuan;

                    $emailName[] = $tujuanObj->email;

                    // $getSifat = DB::table()->pluck

                try {

                    Mail::to($tujuanObj->email)->send(new MailtrapExample(
                        $tujuanObj->pejabat,
                        $surat->no_surat_unit_kerja,
                        $surat->perihal,
                        $getUnor['unorNama'],
                        $getUnor['nama']
                    ));

                } catch (\Exception $e) {
                    // continue;
                }

                    $InsertInbox = DB::table('inbox')->insert(array(
                       'surat_id' => $surat->id,

                       'ori_unor_surat_id_asal' => $getUnor['unorId'],
                       'ori_unor_surat_nama_asal' => $getUnor['unorNama'],

                       'ori_unor_surat_id_tujuan' => $tujuanObj->value,
                       'ori_unor_surat_nama_tujuan' => $tujuanObj->text,

                       'eselon_ii_unor_id' => $getUnor['uke2Id'],
                       'is_first' => 1
                     ));
                }
                $suratActivity->suratActivityTujuan()->createMany($arrTujuan);
            }
            if($request->has('surat_activity.surat_activity_tembusan')) {
                $tembusans = json_decode($request->input('surat_activity.surat_activity_tembusan'));
                $arrTembusan = [];
                foreach ($tembusans as $tembusanObj) {
                    $tembusan = (array)$tembusanObj;
                    $tembusan = Arr::add($tembusan, 'surat_id', $surat->id);
                    $tujuan = Arr::add($tujuan, 'asal_unit_kerja_id', $getUnor['unorId']);
                    $tujuan = Arr::add($tujuan, 'asal_unit_kerja_nama', $getUnor['unorNama']);
                    $tembusan = Arr::add($tembusan, 'tujuan_tembusan_status', 1);
                    $tembusan = Arr::add($tembusan, 'tujuan_tembusan_id', $tembusanObj->value);
                    $tembusan = Arr::add($tembusan, 'tujuan_tembusan_nama', $tembusanObj->text);

                    $tujuan = Arr::add($tujuan, 'pejabat', $tujuanObj->pejabat);
                    $tujuan = Arr::add($tujuan, 'email', $tujuanObj->email);

                    $arrTembusan[] = $tembusan;

                try {

                    Mail::to($tujuanObj->email)->send(new MailtrapExample(
                        $tujuanObj->pejabat,
                        $surat->no_surat_unit_kerja,
                        $surat->perihal,
                        $getUnor['unorNama']
                    ));

                } catch (\Exception $e) {
                    // continue;
                }
                    $InsertInbox = DB::table('inbox')->insert(array(
                       'surat_id' => $surat->id,

                       'ori_unor_surat_id_asal' => $getUnor['unorId'],
                       'ori_unor_surat_nama_asal' => $getUnor['unorNama'],

                       'ori_unor_surat_id_tujuan' => $tujuanObj->value,
                       'ori_unor_surat_nama_tujuan' => $tujuanObj->text,

                       'eselon_ii_unor_id' => $getUnor['uke2Id'],
                       'is_first' => 0
                     ));
                }
                $suratActivity->suratActivityTujuan()->createMany($arrTembusan);
            }

            if($request->has('surat_activity.surat_activity_file')) {
                $arrFiles = [];
                $fileTrue = false;
                foreach ($request->file('surat_activity.surat_activity_file') as $file) {
                    if ($file->isValid()) {
                        $fileTrue = true;
                        $path = Storage::disk('local')->put($file->getClientOriginalExtension(), $file);
                        $arrFiles[] = [
                            'surat_id' => $surat->id,
                            'path' => $path,//$file->store(),
                            'nama' => $file->getClientOriginalName()
                        ];
                    }
                }
                if ($fileTrue) $suratActivity->suratActivityFile()->createMany($arrFiles);
            }

            $surat = Arr::add($surat->find($surat->id), 'surat_activity', $suratActivity->with('suratActivityTujuan')->with('suratActivityFile')->where('id', $suratActivity->id)->get());

            DB::commit();

            $InsertInbox = DB::table('sent')->insert(array(
                'surat_id' => $surat->id,
                'ori_unor_surat_id_asal' => $getUnor['unorId'],
                'ori_unor_surat_nama_asal' => $getUnor['unorNama'],
                'eselon_ii_unor_id' => $getUnor['uke2Id']
              ));

            return $this->sendResponse($surat, 'Surat saved successfully');
        }catch (\Exception $e){
            DB::rollBack();
            return $this->sendError($e->getMessage());
        }catch (\Throwable $e){
            DB::rollBack();
            throw $e;
        }
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/surats/{id}",
     *      summary="Display the specified Surat",
     *      tags={"Surat"},
     *      description="Get Surat",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Surat",
     *          type="string",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Surat"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        try {
            /** @var Surat $surat */
            $surat = $this->suratRepository
                ->with('jenis')
                ->with('tipe')
                ->with('sifat')
                ->with('urgensi')
                ->with('aksesPublik')
                ->with('arsipKategori')
                ->with('arsipMedia')
                ->with('arsipVital')
                ->with('arsipTipe')
                ->with('suratActivity.suratActivityTujuan')
                ->with('suratActivity.suratActivityFile')
                ->with('suratActivity.activitySifat')
                ->with('berkas')
                ->find($id);
            if ($surat === false) {
                throw new \Exception('Surat not found');
            }

        }catch (\Throwable $e){
            throw $e;
        }
        return $this->sendResponse($surat, 'Surat retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateSuratAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/surats/{id}",
     *      summary="Update the specified Surat in storage",
     *      tags={"Surat"},
     *      description="Update Surat",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Surat",
     *          type="string",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Surat that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/SuratSave")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Surat"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateSuratAPIRequest $request)
    {
        DB::beginTransaction();
        try {
            $input = $request->all();

            /** @var Surat $surat */
            $surat = $this->suratRepository->find($id);
            if ($surat===false) {
//                return $this->sendError('Surat not found');
                throw new \Exception('Surat not found');
            }

            $surat = $this->suratRepository->update($input, $id);

            $getSuratActivity = $surat->suratActivity(function ($query) use ($surat){
                $query->where('surat_id', $surat->id);
                $query->where('activity_type', $surat->peruntukan);
                return $query;
            })->get();
            $suratActivity = $surat->suratActivity()->updateExistingPivot($getSuratActivity[0]->id,$request->input('surat_activity'));

            if($request->has('surat_activity.surat_activity_tujuan')) {
                $tujuans = json_decode($request->input('surat_activity.surat_activity_tujuan'));
                $arrTujuan = [];
                foreach ($tujuans as $tujuanObj) {
                    $tujuan = (array) $tujuanObj;
                    $tujuan = Arr::add($tujuan, 'surat_id', $surat->id);
                    $tujuan = Arr::add($tujuan, 'tujuan_tembusan_status', 0);
                    $tujuan = Arr::add($tujuan, 'tujuan_tembusan_id', $tujuanObj->value);
                    $tujuan = Arr::add($tujuan, 'tujuan_tembusan_nama', $tujuanObj->text);
                    $arrTujuan[] = $tujuan;
                }
                $suratActivity->suratActivityTujuan()->delete();
                $suratActivity->suratActivityTujuan()->createMany($arrTujuan);
            }
            if($request->has('surat_activity.surat_activity_tembusan')) {
                $tembusans = json_decode($request->input('surat_activity.surat_activity_tembusan'));
                $arrTembusan = [];
                foreach ($tembusans as $tembusanObj) {
                    $tembusan = (array)$tembusanObj;
                    $tembusan = Arr::add($tembusan, 'surat_id', $surat->id);
                    $tembusan = Arr::add($tembusan, 'tujuan_tembusan_status', 1);
                    $tembusan = Arr::add($tembusan, 'tujuan_tembusan_id', $tembusanObj->value);
                    $tembusan = Arr::add($tembusan, 'tujuan_tembusan_nama', $tembusanObj->text);
                    $arrTembusan[] = $tembusan;
                }
                $suratActivity->suratActivityTujuan()->createMany($arrTembusan);
            }

            if($request->has('surat_activity.surat_activity_file')) {
                $arrFiles = [];
                $fileTrue = false;
                foreach ($request->file('surat_activity.surat_activity_file') as $file) {
                    if ($file->isValid()) {
                        $fileTrue = true;
                        $path = Storage::disk('local')->put($file->getClientOriginalExtension(), $file);
                        $arrFiles[] = [
                            'surat_id' => $id,
                            'path' => $path,//$file->store(),
                            'nama' => $file->getClientOriginalName()
                        ];
                    }
                }
                if ($fileTrue) {
                    $suratActivity->suratActivityFile()->delele();
                    $suratActivity->suratActivityFile()->createMany($arrFiles);
                }
            }

            DB::commit();
        }catch (\Exception $e){
            DB::rollBack();
            return $this->sendError($e->getMessage());
        }catch (\Throwable $e){
            DB::rollBack();
            throw $e;
        }
        return $this->sendResponse($surat->toArray(), 'Surat updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/surats/{id}",
     *      summary="Remove the specified Surat from storage",
     *      tags={"Surat"},
     *      description="Delete Surat",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Surat",
     *          type="string",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        // DB::beginTransaction();
        try {

            $Del                = DB::table("surat")->where("id", $id)->delete();
            $DelSuratActAsal    = DB::table("surat_activity")->where("surat_id", $id)->delete();
            $DelSuratFile       = DB::table("surat_activity_file")->where("surat_id", $id)->delete();
            $DelSuratActTujuan  = DB::table("surat_activity_tujuan")->where("surat_id", $id)->delete();

            $DelSuratInbox  = DB::table("inbox")->where("surat_id", $id)->delete();
            $DelSuratSent  = DB::table("sent")->where("surat_id", $id)->delete();

        }catch (\Exception $e){
            return $this->sendError($e->getMessage());
        }
        return $this->sendResponse($id, 'Surat deleted success');
    }

    public function updateBerkas(Request $request)
    {
        $id         = $request->get('id');
        $berkas_id  = $request->get('berkas_id');

        try {

            $updateBerkas   = DB::table("surat")->where("id", $id)
            ->update(array('berkas_id' => $berkas_id));

        }catch (\Exception $e){
            return $this->sendError($e->getMessage());
        }
        return $this->sendResponse($id, 'Surat di Berkaskan success');
    }

    public function is_read(Request $request)
    {
        $surat_id                   = $request->get('surat_id');
        $ori_unor_surat_id_tujuan   = $request->get('ori_unor_surat_id_tujuan');
        $is_read                    = $request->get('is_read');

        if ($surat_id == "" || $surat_id == null) {
            return $this->sendError("Please fill surat_id");
        }
        if ($ori_unor_surat_id_tujuan == "" || $ori_unor_surat_id_tujuan == null) {
            return $this->sendError("Please fill ori_unor_surat_id_tujuan");
        }
        if ($is_read == "" || $is_read == null) {
            return $this->sendError("Please fill is_read");
        }

        try {

            $updateBerkas   = DB::table("inbox")
            ->where("surat_id", $surat_id)
            ->where("ori_unor_surat_id_tujuan", $ori_unor_surat_id_tujuan)
            ->update(array('is_read' => $is_read));

        }catch (\Exception $e){
            return $this->sendError($e->getMessage());
        }
        return $this->sendResponse($is_read, 'Surat Already Read success');
    }

}
