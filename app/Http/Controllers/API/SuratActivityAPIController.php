<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateSuratActivityAPIRequest;
use App\Http\Requests\API\UpdateSuratActivityAPIRequest;
use App\Models\SuratActivity;
use App\Repositories\SuratActivityRepository;
use App\Repositories\SuratActivityRepositoryEloquent;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Response;

/**
 * Class SuratActivityController
 * @package App\Http\Controllers\API
 */

class SuratActivityAPIController extends AppBaseController
{
    /** @var  SuratActivityRepository */
    private $suratActivityRepository;

    public function __construct(SuratActivityRepositoryEloquent $suratActivityRepo)
    {
        $this->suratActivityRepository = $suratActivityRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/suratActivities/{surat_id}",
     *      summary="Get a listing of the SuratActivities.",
     *      tags={"SuratActivity"},
     *      description="Get all SuratActivities",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="surat_id",
     *          description="id of Surat",
     *          type="string",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(ref="#/parameters/offset"),
     *      @SWG\Parameter(ref="#/parameters/limit"),
     *      @SWG\Parameter(ref="#/parameters/search"),
     *      @SWG\Parameter(ref="#/parameters/filters"),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/SuratActivity")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index($surat_id, Request $request)
    {
        $suratActivities =$this->suratActivityRepository->pushCriteria(new LimitOffsetCriteria($request));
        $suratActivities->with('suratActivityTujuan')->with('suratActivityFile');
        if($request->has('filters')){
            $filters = $request->get('filters');
            $filters = explode(";",$filters);
            $suratActivities->scopeQuery(function ($query) use ($filters){
                foreach ($filters as $filter){
                    $filter = explode(",",$filter);
                    $where = collect(trim($filter[0]), trim($filter[1]), trim($filter[2]));
                    $query->where([$where]);
                }
                return $query;
            });
        }
        $suratActivities->scopeQuery(function ($query) use ($surat_id){
            return $query->where('surat_id',$surat_id)->orderBy('created_at', 'asc');
        });

        return $this->sendResponse($suratActivities->paginate(), 'Surat Activities retrieved successfully');
    }

    /**
     * @param CreateSuratActivityAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/suratActivities/{surat_id}",
     *      summary="Store a newly created SuratActivity in storage",
     *      tags={"SuratActivity"},
     *      description="Store SuratActivity",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="surat_id",
     *          description="id of Surat",
     *          type="string",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="SuratActivity that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/SuratActivitySave")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/SuratActivity"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store($surat_id,CreateSuratActivityAPIRequest $request)
    {

        $getUnor = $request->get('resUser');

        DB::beginTransaction();
        try{

            $input = Arr::add($request->all(), 'surat_id', $surat_id);

            $suratActivity = $this->suratActivityRepository->create($input);

            if($request->has('surat_activity_tujuan')) {
                $tujuans = json_decode($request->input('surat_activity_tujuan'));
                $arrTujuan = [];
                foreach ($tujuans as $tujuanObj) {
                    $tujuan = (array) $tujuanObj;
                    $tujuan = Arr::add($tujuan, 'surat_id', $surat_id);
                    $tujuan = Arr::add($tujuan, 'tujuan_tembusan_status', 0);
                    $tujuan = Arr::add($tujuan, 'tujuan_tembusan_id', $tujuanObj->value);
                    $tujuan = Arr::add($tujuan, 'tujuan_tembusan_nama', $tujuanObj->text);

                    $tujuan = Arr::add($tujuan, 'asal_unit_kerja_id', $getUnor['unorId']);
                    $tujuan = Arr::add($tujuan, 'asal_unit_kerja_nama', $getUnor['unorNama']);

                    $tujuan = Arr::add($tujuan, 'pejabat', $tujuanObj->pejabat);
                    $tujuan = Arr::add($tujuan, 'email', $tujuanObj->email); 

                    $arrTujuan[] = $tujuan;

                    $InsertInbox = DB::table('inbox')->insert(array(
                       'surat_id' => $surat_id,

                       'ori_unor_surat_id_asal' => $getUnor['unorId'],
                       'ori_unor_surat_nama_asal' => $getUnor['unorNama'],

                       'ori_unor_surat_id_tujuan' => $tujuanObj->value,
                       'ori_unor_surat_nama_tujuan' => $tujuanObj->text,

                       'eselon_ii_unor_id' => $getUnor['uke2Id'],
                       'is_first' => 0
                     ));                      
                }
                $suratActivity->suratActivityTujuan()->createMany($arrTujuan);
            }
            // if($request->has('surat_activity_tembusan')) {
            //     $tembusans = json_decode($request->input('surat_activity_tembusan'));
            //     $arrTembusan = [];
            //     foreach ($tembusans as $tembusanObj) {
            //         $tembusan = (array)$tembusanObj;
            //         $tembusan = Arr::add($tembusan, 'surat_id', $surat_id);
            //         $tembusan = Arr::add($tembusan, 'tujuan_tembusan_status', 1);
            //         $tembusan = Arr::add($tembusan, 'tujuan_tembusan_id', $tembusanObj->value);
            //         $tembusan = Arr::add($tembusan, 'tujuan_tembusan_nama', $tembusanObj->text);

            //         $tujuan = Arr::add($tujuan, 'asal_unit_kerja_id', $tujuanObj->asal_unit_kerja_id);
            //         $tujuan = Arr::add($tujuan, 'asal_unit_kerja_nama', $tujuanObj->asal_unit_kerja_nama);

            //         $tembusan = Arr::add($tembusan, 'pejabat', $tembusanObj->pejabat);
            //         $tembusan = Arr::add($tembusan, 'email', $tembusanObj->email);

            //         $arrTembusan[] = $tembusan;
            //     }
            //     $suratActivity->suratActivityTujuan()->createMany($arrTembusan);
            // }

            if($request->has('surat_activity_file')) {
                $arrFiles = [];
                $fileTrue = false;
                foreach ($request->file('surat_activity_file') as $file) {
                    if ($file->isValid()) {
                        $fileTrue = true;
                        $path = Storage::disk('local')->put($file->getClientOriginalExtension(), $file);
                        $arrFiles[] = [
                            'surat_id' => $surat_id,
                            'path' => $path,//$file->store(),
                            'nama' => $file->getClientOriginalName()
                        ];
                    }
                }
                if ($fileTrue) $suratActivity->suratActivityFile()->createMany($arrFiles);
            }

            $suratActivity = $suratActivity->with('suratActivityTujuan')->with('suratActivityFile')->find($suratActivity->id);

            DB::commit();
         
            $InsertInbox = DB::table('sent')->insert(array(
                'surat_id' => $surat_id,
                'ori_unor_surat_id_asal' => $getUnor['unorId'],
                'ori_unor_surat_nama_asal' => $getUnor['unorNama'],
                'eselon_ii_unor_id' => $getUnor['uke2Id']
              ));             

        }catch (\Exception $e){
            DB::rollBack();
//            throw new \Exception($e->getMessage());
            return $this->sendError($e->getMessage());
        }catch (\Throwable $e){
            DB::rollBack();
            throw $e;
        }

        return $this->sendResponse($suratActivity, 'Surat Activity saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/suratActivities/{surat_id}/{id}",
     *      summary="Display the specified SuratActivity",
     *      tags={"SuratActivity"},
     *      description="Get SuratActivity",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="surat_id",
     *          description="id of Surat",
     *          type="string",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of SuratActivity",
     *          type="string",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/SuratActivity"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($surat_id,$id)
    {
//        DB::beginTransaction();
        try {
            /** @var SuratActivity $suratActivity */
            $this->suratActivityRepository->with('suratActivityTujuan')->with('suratActivityFile');
            $suratActivity = $this->suratActivityRepository->findWhere(['id' => $id, 'surat_id' => $surat_id]);
            if ($suratActivity===false) {
//                return $this->sendError('Surat Activity not found');
                throw new \Exception('Surat Activity not found');
            }

        }catch (\Exception $e){
            return $this->sendError($e->getMessage());
        }catch (\Throwable $e){
            throw $e;
        }
        return $this->sendResponse($suratActivity, 'Surat Activity retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateSuratActivityAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/suratActivities/{surat_id}/{id}",
     *      summary="Update the specified SuratActivity in storage",
     *      tags={"SuratActivity"},
     *      description="Update SuratActivity",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="surat_id",
     *          description="id of Surat",
     *          type="string",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of SuratActivity",
     *          type="string",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="SuratActivity that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/SuratActivitySave")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/SuratActivity"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($surat_id,$id, UpdateSuratActivityAPIRequest $request)
    {
        DB::beginTransaction();
        try {
            $input = $request->all();

            /** @var SuratActivity $suratActivity */
            $suratActivity = $this->suratActivityRepository->findWhere([['id' => $id], ['surat_id' => $surat_id]]);

            if ($suratActivity===false) {
//                return $this->sendError('Surat Activity not found');
                throw new \Exception('Surat Activity not found');
            }

            $suratActivity = $this->suratActivityRepository->update($input, $id);

            if($request->has('surat_activity_tujuan')) {
                $arrTujuan = [];
                foreach ($request->input('surat_activity_tujuan') as $tujuan) {
                    $tujuan = Arr::add($tujuan, 'surat_id', $surat_id);
                    $tujuan = Arr::add($tujuan, 'tujuan_tembusan_status', 0);
                    $tujuan = Arr::add($tujuan, 'tujuan_tembusan_id', $tujuan['value']);
                    $tujuan = Arr::add($tujuan, 'tujuan_tembusan_nama', $tujuan['text']);
                    $arrTujuan[] = $tujuan;
                }
                $suratActivity->suratActivityTujuan()->delele();
                $suratActivity->suratActivityTujuan()->createMany($arrTujuan);
            }

            if($request->has('surat_activity_tembusan')) {
                $arrTujuan = [];
                foreach ($request->input('surat_activity_tembusan') as $tujuan) {
                    $tujuan = Arr::add($tujuan, 'surat_id', $surat_id);
                    $tujuan = Arr::add($tujuan, 'tujuan_tembusan_status', 1);
                    $tujuan = Arr::add($tujuan, 'tujuan_tembusan_id', $tujuan['value']);
                    $tujuan = Arr::add($tujuan, 'tujuan_tembusan_nama', $tujuan['text']);
                    $arrTujuan[] = $tujuan;
                }
                $suratActivity->suratActivityTujuan()->createMany($arrTujuan);
            }

            if($request->has('surat_activity_file')) {
                $arrFiles = [];
                $fileTrue = false;
                foreach ($request->file('surat_activity_file') as $file) {
                    if ($file->isValid()) {
                        $fileTrue = true;
                        $path = Storage::disk('local')->put($file->getClientOriginalExtension(), $file);
                        $arrFiles[] = [
                            'surat_id' => $surat_id,
                            'path' => $path,//$file->store(),
                            'nama' => $file->getClientOriginalName()
                        ];
                    }
                }
                if ($fileTrue) {
                    $suratActivity->suratActivityFile()->delele();
                    $suratActivity->suratActivityFile()->createMany($arrFiles);
                }
            }

            DB::commit();
        }catch (\Exception $e){
            DB::rollBack();
            $this->sendError($e->getMessage());
        }catch (\Throwable $e){
            DB::rollBack();
            throw $e;
        }
        return $this->sendResponse($suratActivity->with('suratActivityTujuan')->with('suratActivityFile')->find($id), 'SuratActivity updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/suratActivities/{id}",
     *      summary="Remove the specified SuratActivity from storage",
     *      tags={"SuratActivity"},
     *      description="Delete SuratActivity",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of SuratActivity",
     *          type="string",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            /** @var SuratActivity $suratActivity */
            $suratActivity = $this->suratActivityRepository->find($id);

            if (empty($suratActivity)) {
//                return $this->sendError('Surat Activity not found');
                throw new \Exception('Surat Activity not found');
            }

            $suratActivity->suratActivityTujuan()->delete();
            $suratActivity->suratActivityFile()->delete();
            $suratActivity->delete();

            DB::commit();
        }catch (\Exception $e){
            DB::rollBack();
            $this->sendError($e->getMessage());
        }catch (\Throwable $e){
            DB::rollBack();
            throw $e;
        }
        return $this->sendResponse($id, 'Surat Activity deleted successfully');
    }

    public function coba($surat,$activity,Request $request)
    {
        DB::beginTransaction();
        try {
            $suratActivity = $this->suratActivityRepository->find($activity);
            $arrFiles = [];
            $fileTrue = false;
            foreach ($request->file('file') as $file) {
                if ($file->isValid()) {
                    $fileTrue = true;
                    $path = Storage::disk('local')->put($file->getClientOriginalExtension(), $file);
//                $path = Storage::disk('local')->put('',$file);
                    $arrFiles[] = [
//                    'surat_id' => $surat,
                        'path' => $path,
                        'nama' => $file->getClientOriginalName()
                    ];
                }
            }
//        $suratActivity = $this->suratActivityRepository->find($activity);
            $suratActivity->suratActivityFile()->createMany($arrFiles);
            DB::commit();
        }catch (\Exception $e){
            DB::rollBack();
        }
        return $this->sendResponse($suratActivity->with('suratActivityFile')->find($activity), 'Surat Activity deleted successfully');
    }

    public function download(Request $request)
    {
        return Storage::download($request->input('path'));
    }
}
