<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateSuratBerkasAPIRequest;
use App\Http\Requests\API\UpdateSuratBerkasAPIRequest;
use App\Models\SuratBerkas;
use App\Repositories\SuratBerkasRepository;
use App\Repositories\SuratBerkasRepositoryEloquent;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Log;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Response;
use App\Exports\SuratBerkasExport;
use App\Exports\BladeExports;
use Maatwebsite\Excel\Facades\Excel;
use Ixudra\Curl\Facades\Curl;

/**
 * Class SuratBerkasController
 * @package App\Http\Controllers\API
 */

class SuratBerkasAPIController extends AppBaseController
{
    /** @var  SuratBerkasRepository */
    private $suratBerkasRepository;

    public function __construct(SuratBerkasRepositoryEloquent $suratBerkasRepo)
    {
        $this->suratBerkasRepository = $suratBerkasRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/suratBerkas",
     *      summary="Get a listing of the SuratBerkas.",
     *      tags={"SuratBerkas"},
     *      description="Get all SuratBerkas",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(ref="#/parameters/offset"),
     *      @SWG\Parameter(ref="#/parameters/limit"),
     *      @SWG\Parameter(ref="#/parameters/search"),
     *      @SWG\Parameter(ref="#/parameters/filters"),
     *      @SWG\Parameter(ref="#/parameters/sortedBy"),
     *      @SWG\Parameter(ref="#/parameters/orderBy"),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/SuratBerkas")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $userDetail = $request->get('resUser');
        $token      = $userDetail['accessToken'];

        $suratBerkas = $this->suratBerkasRepository->pushCriteria(new LimitOffsetCriteria($request));
        $suratBerkas->withCount('surat');
        $suratBerkas->with('retensi_active_until');

        if($request->has('filters')){
            $filters = $request->get('filters');
            $filters = explode(";",$filters);
            $suratBerkas->scopeQuery(function($query) use ($filters){
                foreach ($filters as $fil){
                    $fil = explode(",",$fil);
                    $whr = collect([trim($fil[0]),trim($fil[1]),trim($fil[2])])->all();
                    $query->where([$whr]);
                }
                return $query;
            });
        }

        $data = $suratBerkas->paginate();

        foreach ($data as $key => $value) {

            $uke2Nama = '';

            // get n_pejabat
            $result = Curl::to(getenv("URL_SIMPEG") . "/api/unor/id/" . $data[$key]['uk_id'])
                ->withData(array('page:' => 0,
                    'size' => 10,
                    'sortField' => 'id',
                    'sortOrder' => 'ASC'
                ))
                ->withHeaders(array(
                    'Accept: application/json', 
                    'Content-Type: application/json',
                    'Authorization: Bearer ' . $token
                ))
                ->withResponseHeaders()
                ->asJson(true)
                ->post();
            
            // check if eselon II or not
            if ($result['eselonNama'] == 'II.a') {

                $uke2Nama = $result['namaJabatan'];

            } else {

                // if uk_id != eselon ii then get uke2_nama from atasan
                $idPejabat = $result["pnsIdPejabat"];
                $getAtasanInfo = Curl::to(getenv("URL_SIMPEG") . "/api/pns/atasan/" . $idPejabat)
                    ->withHeaders(array(
                        'Accept: application/json', 
                        'Content-Type: application/json',
                        'Authorization: Bearer ' . $token
                    ))
                    ->withResponseHeaders()
                    ->asJson(true)
                    ->get();
                
                $uke2Nama = $getAtasanInfo['unorNamaUnor'];                

            }

            // set uke2_nama
            $data[$key]['uke2_nama'] = $uke2Nama;
        }

        return $this->sendResponse($data, 'Surat Berkas retrieved successfully');
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/suratBerkas/daftarIsi",
     *      summary="Get a listing of the SuratBerkas DaftarIsi",
     *      tags={"SuratBerkas"},
     *      description="Get all SuratBerkas",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(ref="#/parameters/offset"),
     *      @SWG\Parameter(ref="#/parameters/limit"),
     *      @SWG\Parameter(ref="#/parameters/search"),
     *      @SWG\Parameter(ref="#/parameters/filters"),
     *      @SWG\Parameter(ref="#/parameters/sortedBy"),
     *      @SWG\Parameter(ref="#/parameters/orderBy"),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/SuratBerkas")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function daftarIsi(Request $request)
    {
        $suratBerkas = $this->suratBerkasRepository->pushCriteria(new LimitOffsetCriteria($request));
        $suratBerkas->withCount('surat');

        // $suratBerkas->with(["surat_terkait.asal_tujuan" => function($q) {
        //         $q->select('inbox.surat_id','inbox.ori_unor_surat_nama_asal as asal_surat', 'inbox.ori_unor_surat_nama_tujuan as tujuan_surat');
        // }]);

        $suratBerkas->with('surat_terkait');
        
        if($request->has('filters')){
            $filters = $request->get('filters');
            $filters = explode(";",$filters);
            $suratBerkas->scopeQuery(function($query) use ($filters){
                foreach ($filters as $fil){
                    $fil = explode(",",$fil);
                    $whr = collect([trim($fil[0]),trim($fil[1]),trim($fil[2])])->all();
                    $query->where([$whr]);
                }
                return $query;
            });
        }
        return $this->sendResponse($suratBerkas->paginate(), 'Surat Berkas daftar isi retrieved successfully');
    }    

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/suratBerkas/exportExcel",
     *      summary="Get a listing of the SuratBerkas exportExcel",
     *      tags={"SuratBerkas"},
     *      description="Get all SuratBerkas",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="year",
     *          description="year klasifikasi",
     *          type="string",
     *          required=true,
     *          in="query"
     *      ),
     *      @SWG\Parameter(
     *          name="month",
     *          description="month klasifikasi",
     *          type="string",
     *          required=true,
     *          in="query"
     *      ),       
     *      @SWG\Parameter(
     *          name="uke2id",
     *          description="uke2id",
     *          type="string",
     *          required=true,
     *          in="query"
     *      ), 
     *      @SWG\Parameter(
     *          name="uke2Nama",
     *          description="uke2Nama",
     *          type="string",
     *          required=true,
     *          in="query"
     *      ),      
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/SuratBerkas")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function exportExcel(Request $request)
    {
        $year   = $request->input('year');
        $month  = $request->input('month');
        $uke2    = $request->input('uke2id');
        $uke2Nama    = $request->input('uke2Nama');

        // get uke1
        $userDetail = $request->get('resUser');
        // $uke2       = $userDetail['uke2Id'];
        // $uke2Nama   = $userDetail['uke2Nama'];

        $monthName = date("F", strtotime('00-'.$month.'-01'));
        $periods = " (" .$monthName. "-" . $year .")";
        
        return Excel::download(new SuratBerkasExport($year, $month, $uke2, $uke2Nama), 'Laporan berkas '.$uke2Nama.$periods.'.xlsx');
    }

    /**
     * @param CreateSuratBerkasAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/suratBerkas",
     *      summary="Store a newly created SuratBerkas in storage",
     *      tags={"SuratBerkas"},
     *      description="Store SuratBerkas",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="SuratBerkas that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/SuratBerkas")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/SuratBerkas"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateSuratBerkasAPIRequest $request)
    {
        $input = $request->all();

        $suratBerkas = $this->suratBerkasRepository->create($input);

        return $this->sendResponse($suratBerkas->toArray(), 'Surat Berkas saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/suratBerkas/{id}",
     *      summary="Display the specified SuratBerkas",
     *      tags={"SuratBerkas"},
     *      description="Get SuratBerkas",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of SuratBerkas",
     *          type="string",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/SuratBerkas"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var SuratBerkas $suratBerkas */
        $suratBerkas = $this->suratBerkasRepository->find($id);

        if (empty($suratBerkas)) {
            return $this->sendError('Surat Berkas not found');
        }

        return $this->sendResponse($suratBerkas->toArray(), 'Surat Berkas retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateSuratBerkasAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/suratBerkas/{id}",
     *      summary="Update the specified SuratBerkas in storage",
     *      tags={"SuratBerkas"},
     *      description="Update SuratBerkas",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of SuratBerkas",
     *          type="string",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="SuratBerkas that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/SuratBerkas")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/SuratBerkas"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateSuratBerkasAPIRequest $request)
    {
        $input = $request->all();

        /** @var SuratBerkas $suratBerkas */
        $suratBerkas = $this->suratBerkasRepository->find($id);

        if (empty($suratBerkas)) {
            return $this->sendError('Surat Berkas not found');
        }
        
        $suratBerkas = $this->suratBerkasRepository->update($input, $id);

        return $this->sendResponse($suratBerkas->toArray(), 'SuratBerkas updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/suratBerkas/{id}",
     *      summary="Remove the specified SuratBerkas from storage",
     *      tags={"SuratBerkas"},
     *      description="Delete SuratBerkas",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of SuratBerkas",
     *          type="string",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var SuratBerkas $suratBerkas */
        $suratBerkas = $this->suratBerkasRepository->find($id);

        if (empty($suratBerkas)) {
            return $this->sendError('Surat Berkas not found');
        }

        $suratBerkas->delete();

        return $this->sendResponse($id, 'Surat Berkas deleted successfully');
    }
}
