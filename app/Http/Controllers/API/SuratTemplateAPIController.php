<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Models\SuratTemplate;
use App\Http\Controllers\AppBaseController;

use App\Http\Requests\API\CreateSuratTemplateAPIRequest;
use App\Http\Requests\API\UpdateSuratTemplateAPIRequest;

use App\Repositories\SuratTemplateRepository;
use App\Repositories\SuratTemplateRepositoryEloquent;

use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Illuminate\Support\Facades\DB;
use Response;

/**
 * Class SuratTemplateController
 * @package App\Http\Controllers\API
 */

class SuratTemplateAPIController extends AppBaseController
{
    /** @var  SuratTemplateRepository */
    private $masterSuratTemplateRepository;

    public function __construct(SuratTemplateRepositoryEloquent $masterSuratTemplateRepo)
    {
        $this->masterSuratTemplateRepository = $masterSuratTemplateRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/suratTemplate",
     *      summary="Get a listing of the SuratTemplates.",
     *      tags={"SuratTemplate"},
     *      description="Get all SuratTemplates",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(ref="#/parameters/offset"),
     *      @SWG\Parameter(ref="#/parameters/limit"),
     *      @SWG\Parameter(ref="#/parameters/search"),
     *      @SWG\Parameter(ref="#/parameters/filters"),
     *      @SWG\Parameter(ref="#/parameters/sortedBy"),
     *      @SWG\Parameter(ref="#/parameters/orderBy"),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/SuratTemplate")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $suratTemplate = $this->masterSuratTemplateRepository->pushCriteria(new LimitOffsetCriteria($request));
        if($request->has('filters')){
            $filters = $request->get('filters');
            $filters = explode(";",$filters);
            $suratTemplate->scopeQuery(function($query) use ($filters){
                foreach ($filters as $fil){
                    $fil = explode(",",$fil);
                    $whr = collect([trim($fil[0]),trim($fil[1]),trim($fil[2])])->all();
                    $query->where([$whr]);
                }
                return $query;
            });
        }

        return $this->sendResponse($suratTemplate->paginate(), 'Master Surat Templates retrieved successfully');
    }

    /**
     * @param CreateSuratTemplateAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/suratTemplate",
     *      summary="Store a newly created SuratTemplate in storage",
     *      tags={"SuratTemplate"},
     *      description="Store SuratTemplate",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="SuratTemplate that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/SuratTemplate")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/SuratTemplate"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateSuratTemplateAPIRequest $request)
    {
        $input = $request->all();

        $masterSuratTemplate = $this->masterSuratTemplateRepository->create($input);

        return $this->sendResponse($masterSuratTemplate->toArray(), 'Master Surat Template saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/suratTemplate/{id}",
     *      summary="Display the specified SuratTemplate",
     *      tags={"SuratTemplate"},
     *      description="Get SuratTemplate",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of SuratTemplate",
     *          type="string",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/SuratTemplate"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var SuratTemplate $masterSuratTemplate */
        $masterSuratTemplate = $this->masterSuratTemplateRepository->find($id);

        if (empty($masterSuratTemplate)) {
            return $this->sendError('Master Surat Template not found');
        }

        return $this->sendResponse($masterSuratTemplate->toArray(), 'Master Surat Template retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateSuratTemplateAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/suratTemplate/{id}",
     *      summary="Update the specified SuratTemplate in storage",
     *      tags={"SuratTemplate"},
     *      description="Update SuratTemplate",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of SuratTemplate",
     *          type="string",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="SuratTemplate that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/SuratTemplate")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/SuratTemplate"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateSuratTemplateAPIRequest $request)
    {
        $input = $request->all();

        /** @var SuratTemplate $masterSuratTemplate */
        $masterSuratTemplate = $this->masterSuratTemplateRepository->find($id);

        if (empty($masterSuratTemplate)) {
            return $this->sendError('Master Surat Template not found');
        }

        $masterSuratTemplate = $this->masterSuratTemplateRepository->update($input, $id);

        return $this->sendResponse($masterSuratTemplate->toArray(), 'SuratTemplate updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/suratTemplate/{id}",
     *      summary="Remove the specified SuratTemplate from storage",
     *      tags={"SuratTemplate"},
     *      description="Delete SuratTemplate",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of SuratTemplate",
     *          type="string",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
     
        /** @var SuratTemplate $masterSuratTemplate */
        $masterSuratTemplate = $this->masterSuratTemplateRepository->find($id);

        if ( empty($masterSuratTemplate) ) {
            return $this->sendError('Master Surat Template not found');
        }

        // else go deleted()
        $masterSuratTemplate->delete();

        return $this->sendResponse($id, 'Master Surat Template deleted successfully');
    }
}
