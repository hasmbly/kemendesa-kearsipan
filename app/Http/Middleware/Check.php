<?php

namespace App\Http\Middleware;

use Closure;
use http\Client\Response;
use Illuminate\Support\Facades\Log;
use InfyOm\Generator\Utils\ResponseUtil;
use Ixudra\Curl\Facades\Curl;

class Check
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!$request->header('Authorization')){
            return Response()->json(ResponseUtil::makeError('Unauthorized'),'401');
        }
        $results = Curl::to(getenv("URL_SIMPEG") . "/api/auth/check-token")
            ->withHeaders(array('Accept: application/json', 'Content-Type: application/json','Authorization:'.$request->header('Authorization')))
            ->withResponseHeaders()
            ->asJson(true)
            ->get();
//        Log::debug($results);
        if(array_key_exists ('error',$results)){
            return Response()->json(ResponseUtil::makeError($results['error']),$results['status']);
        }

//        Log::debug($result);
//        {
//            "success": true,
//            "message": "Success",
//            "mapData": {
//                "data": {
//                    "accessToken": "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIxOSIsInVzZXJuYW1lIjoiYWRtaW4iLCJyb2xlcyI6WyJST0xFX0FETUlOIl0sInBuc0lkIjoiN0UyMTI4MEZDQkQ5ODkwRkUwNTA2NDBBM0MwMzU5NzMiLCJ1bm9ySWQiOiIxQ0I3NUQ5MjM1Nzk1OURFRTA1MDY0MEExNTAyNTdCNSIsImlhdCI6MTU2ODk1ODY5MSwiZXhwIjoxNjAwNTgxMDkxfQ.UPSIiyPOz9T01SQFMQ27zhnNEzkTCtizp4aosrENmLH2YpOAYFfhnrYS238O0c_j4X7R0DHfBICUK0zIvxydtw",
//                    "tokenType": "Bearer",
//                    "pnsId": "7E21280FCBD9890FE050640A3C035973",
//                    "username": "admin",
//                    "email": "admin@kemendesa.go.id",
//                    "nama": null,
//                    "jenisJabatan": "FUNGSIONAL_UMUM",
//                    "roles": [
//                                "ROLE_ADMIN"
//                            ],
//                    "unorId": "1CB75D92357959DEE050640A150257B5",
//                    "unorNama": "Biro Sumber Daya Manusia dan Umum"
//                }
//            }
//        }

        $result = $results['mapData']['data'];
//        Log::debug($result);
        $roles = ['ROLE_ADMIN', 'ROLE_TU', 'ROLE_RECORD_CENTER'];
        $true = false;
        foreach ($result['roles'] as $rol){
            if(in_array($rol,$roles)){
                $true = true;
            }
        }
        if($result['jenisJabatan'] != "STRUKTURAL" && !$true){ return Response()->json(ResponseUtil::makeError("Forbidden"),403); }



            ////baca dari jwt token
//        $jwt = MyJwtLibrary::decode($request->header('Authorization'),'',['HS256','HS384','HS512']);
//        Log::debug(json_encode($jwt->unorId));
//        {
//            "sub": "19",
//            "username": "admin",
//            "roles": [
//                "ROLE_ADMIN"
//            ],
//            "pnsId": "7E21280FCBD9890FE050640A3C035973",
//            "unorId": "1CB75D92357959DEE050640A150257B5",
//            "iat": 1568958691,
//            "exp": 1600581091
//        }
//        $request->attributes->add(['resUser'=>$jwt]);
        $request->attributes->add(['resUser'=>$result]);

        return $next($request);
    }
}
