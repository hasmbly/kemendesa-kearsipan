<?php
/**
 * ${NAME}
 *
 * @author aji gojali
 */
//SWG\Parameter(
// *          in="query",
// *          name="search",
// *          description="search if multi ex. id:17;nama:aji ",
// *          type="string"
//    *      ),

/**
 *
 *      @SWG\Parameter(
 *          in="query",
 *          name="page",
 *          description="page",
 *          type="integer"
 *      ),
 *      @SWG\Parameter(
 *          in="query",
 *          name="skip",
 *          description="skip",
 *          type="integer"
 *      ),
 *
 *
 *      @SWG\Parameter(
 *          in="query",
 *          name="offset",
 *          description="offset",
 *          type="integer"
 *      ),
 *      @SWG\Parameter(
 *          in="query",
 *          name="limit",
 *          description="limit",
 *          type="integer",
 *          default=15
 *      ),
 *      @SWG\Parameter(
 *          in="query",
 *          name="search",
 *          description="search",
 *          type="string"
 *      ),
 *      @SWG\Parameter(
 *          in="query",
 *          name="filters",
 *          description="filters ex. id,=,123;nama,=,aji keterangan -> field, conditions (ex. (=,>,<,<=,>=) not for like), value",
 *          type="string",
 *
 *      ),
 *      @SWG\Parameter(
 *          in="query",
 *          name="sortedBy",
 *          description="sortedBy",
 *          type="string",
 *          default="asc"
 *      ),
 *      @SWG\Parameter(
 *          in="query",
 *          name="orderBy",
 *          description="orderBy",
 *          type="string",
 *          default="id"
 *      )
 *
 */
