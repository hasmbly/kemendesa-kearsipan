<?php
namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

// use App\Helpers\makePdf;
// use PDF;

class MailtrapExample extends Mailable
{

    // private $publicPath = 'uploads/pelayanan';

    // private $urlUpload  = '/uploads/info-grafis/';

    // private $ticket = '';

    // private $antrian = 0;

    private $nama = '';
    private $nomor_surat = '';
    private $perihal = '';
    private $unor_pengirim = '';
    private $nama_pengirim = '';

    use Queueable, SerializesModels;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($nama,$nomor_surat,$perihal,$unor_pengirim,$nama_pengirim)
    {
        // $this->ticket = $ticket;
        // $this->antrian = $antrian;
        $this->nama           = $nama;
        $this->nomor_surat    = $nomor_surat;
        $this->perihal        = $perihal;
        $this->unor_pengirim  = $unor_pengirim;
        $this->nama_pengirim  = $nama_pengirim;
    }
    /**
     * Build the message.
     *
     * @return $this
     */
public function build()
    {

        // $filename   = DB::table('tbl_pelayanan')->where('id_pelayanan', $id)->pluck('filename');        

        // $path = public_path($this->publicPath) . '/' . $filename[0];
        
        // $path = public_path($this->publicPath) . '/' . "1692249643_CRM-FRONT_V1.1.pdf";

         // $getDownloadPdf = makePdf::getDownloadPdf($this->ticket, $this->antrian);

        return $this->from('no-reply@kemendesa.go.id', 'Kemendesa')
            ->subject('Kemendesa')
            ->markdown('mails.exmpl')
            ->with([
                'name' => $this->nama,
                'nomor_surat' => $this->nomor_surat,
                'perihal' => $this->perihal,
                'unor_pengirim' => $this->unor_pengirim,
                'nama_pengirim' => $this->nama_pengirim
            ]);
    }

}