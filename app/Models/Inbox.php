<?php

namespace App\Models;

use Eloquent as Model;

class Inbox extends Model
{
    public $table = 'inbox';

    public $fillable = [
        'id',
        'surat_id',
        'ori_unor_surat_id_tujuan',
        'eselon_ii_unor_id',
        'is_read'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'surat_id' => 'string',
        'ori_unor_surat_id_tujuan' => 'string',
        'eselon_ii_unor_id' => 'string',
        'is_read' => 'integer'
    ];    

    /**
     * Get the surat activity tujuan that owns the surat.
     */
    public function surat()
    {
        return $this->belongsTo('App\Models\Surat','surat_id','id');
    }

    /**
     * Get the surat activity tujuan that owns the activity.
     */
    public function suratActivityTujuan()
    {
        return $this->belongsTo('App\Models\SuratActivity','ori_unor_surat_id_tujuan','tujuan_tembusan_id');
    }
       
}