<?php

namespace App\Models;

// use App\Traits\Uuid;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="MasterSuratKlasifikasi",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="parent_id",
 *          description="parent_id",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="code",
 *          description="code",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="nama",
 *          description="nama",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="keterangan",
 *          description="keterangan",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="retensi_thn_active",
 *          description="retensi_thn_active",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="retensi_thn_inactive",
 *          description="retensi_thn_inactive",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="susut_id",
 *          description="susut_id",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="status",
 *          description="status",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="status_parent",
 *          description="status_parent",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="deleted_at",
 *          description="deleted_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class MasterSuratKlasifikasi extends Model
{
    // use SoftDeletes, Uuid;
    // use SoftDeletes;

    public $table = 'm_surat_klasifikasi';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $dates = ['deleted_at'];

    public $incrementing = true;

    public $fillable = [
        'parent_id',
        'code',
        'nama',
        'keterangan',
        'retensi_thn_active',
        'retensi_thn_inactive',
        'susut_id',
        'status',
        'status_parent'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'parent_id' => 'string',
        'code' => 'string',
        'nama' => 'string',
        'keterangan' => 'string',
        'retensi_thn_active' => 'integer',
        'retensi_thn_inactive' => 'integer',
        'susut_id' => 'string',
        'status' => 'integer',
        'status_parent' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'code' => 'required',
        'nama' => 'required',
        'status' => 'required',
        'status_parent' => 'required'
    ];

    public function penyusutan()
    {
        return $this->belongsTo('App\Models\MasterPenyusutan','susut_id','id');
    }

    public function klasifikasiChild()
    {
        return $this->hasMany('App\Models\MasterSuratKlasifikasi', 'parent_id')->withCount('isExpandable');
    }

    public function isExpandable()
    {
        return $this->hasMany('App\Models\MasterSuratKlasifikasi', 'parent_id');
    }    
}

    //     public function klasifikasiChild()
    // {
    //     return $this->hasMany('App\Models\MasterSuratKlasifikasi', 'parent_id')
    //         ->withCount('isExpandable')
    //         ->select("*")
    //         ->selectRaw("if ( (select count(parent_id) from m_surat_klasifikasi where parent_id = parent_id) = 0, '0', '1' ) as isExpandable")
    //         ;
    // }