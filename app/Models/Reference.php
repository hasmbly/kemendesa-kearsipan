<?php
/**
 * Reference
 *
 * @author aji gojali
 */


namespace App\Models;


/**
 * @SWG\Definition(
 *      definition="Reference",
 *      required={""},
 *      @SWG\Property(
 *          property="total",
 *          type="integer"
 *      ),
 *      @SWG\Property(
 *          property="items",
 *          type="array",
 *          @SWG\Items(
 *              @SWG\Property(
 *                  property="id",
 *                  description="id",
 *                  type="string"
 *              ),
 *              @SWG\Property(
 *                  property="text",
 *                  description="text",
 *                  type="string"
 *              )
 *          )
 *      ),
 *      @SWG\Property(
 *          property="offset",
 *          type="integer"
 *      )
 * )
 */
class Reference
{



}
