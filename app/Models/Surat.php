<?php

namespace App\Models;

use App\Traits\Uuid;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Surat",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="peruntukan",
 *          description="surat masuk=0 dan keluar=1",
 *          type="number",
 *          format="number"
 *      ),
*      @SWG\Property(
 *          property="eselon_ii_unor_id",
 *          description="eselon_ii_unor_id",
 *          type="string",
 *      ),      
 *      @SWG\Property(
 *          property="jenis_id",
 *          description="fk m_surat_jenis",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="tipe_id",
 *          description="fk m_surat_tipe (asli, copy)",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="sifat_id",
 *          description="fk m_surat_sifat (biasa,rahasia,sangat rahasia)",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="urgensi_id",
 *          description="fk m_surat_urgensi (biasa,segera,sangat segera)",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="akses_publik_id",
 *          description="fk m_surat_akses_publik (terbuka, terturup)",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="arsip_kategori_id",
 *          description="fk m_arsip_kategori (umum, terjaga)",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="arsip_media_id",
 *          description="fk m_surat_arsip_media (kertas)",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="arsip_vital_id",
 *          description="fk m_surat_arsip_vital (tidak vital, vital)",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="arsip_tipe_id",
 *          description="fk m_surat_arsip_tipe (lembar,halaman)",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="arsip_tipe_jumlah",
 *          description="jumlah halaman atau lembar",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="tanggal_surat",
 *          description="tanggal_surat",
 *          type="string",
 *          format="date"
 *      ),
 *      @SWG\Property(
 *          property="perihal",
 *          description="perihal",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="berkas_id",
 *          description="fk surat_berkas",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="tujuan",
 *          description="tujuan",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="no_surat_unit_kerja",
 *          description="no_surat_unit_kerja",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="no_agenda",
 *          description="no_agenda",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="pengirim",
 *          description="internal, external",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="deleted_at",
 *          description="deleted_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Surat extends Model
{
    use SoftDeletes, Uuid;

    public $table = 'surat';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];

    public $incrementing = false;


    public $fillable = [
        'peruntukan',
        'jenis_id',
        'tipe_id',
        'sifat_id',
        'urgensi_id',
        'akses_publik_id',
        'arsip_kategori_id',
        'arsip_media_id',
        'arsip_vital_id',
        'arsip_tipe_id',
        'arsip_tipe_jumlah',
        'tanggal_surat',
        'perihal',
        'berkas_id',
        'tujuan',
        'no_surat_unit_kerja',
        'no_agenda',
        'pengirim'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'string',
        'peruntukan' => 'float',
        'jenis_id' => 'string',
        'tipe_id' => 'string',
        'sifat_id' => 'string',
        'urgensi_id' => 'string',
        'akses_publik_id' => 'string',
        'arsip_kategori_id' => 'string',
        'arsip_media_id' => 'string',
        'arsip_vital_id' => 'string',
        'arsip_tipe_id' => 'string',
        'arsip_tipe_jumlah' => 'integer',
        'tanggal_surat' => 'date',
        'perihal' => 'string',
        'berkas_id' => 'string',
        'tujuan' => 'string',
        'no_surat_unit_kerja' => 'string',
        'no_agenda' => 'string',
        'pengirim' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'jenis_id' => 'required',
        'tanggal_surat' => 'required',
        'perihal' => 'required',
        'no_surat_unit_kerja' => 'required'
    ];

    /**
     * Get the surat that owns the surat jenis.
     */
    public function jenis()
    {
        return $this->belongsTo('App\Models\MasterSuratJenis','jenis_id','id');
    }
    /**
     * Get the surat that owns the surat tipe.
     */
    public function tipe()
    {
        return $this->belongsTo('App\Models\MasterSuratTipe','tipe_id','id');
    }
    /**
     * Get the surat that owns the surat sifat.
     */
    public function sifat()
    {
        return $this->belongsTo('App\Models\MasterSuratSifat','sifat_id','id');
    }
    /**
     * Get the surat that owns the surat urgensi.
     */
    public function urgensi()
    {
        return $this->belongsTo('App\Models\MasterSuratUrgensi','urgensi_id','id');
    }
    /**
     * Get the surat that owns the surat akses Publik.
     */
    public function aksesPublik()
    {
        return $this->belongsTo('App\Models\MasterSuratAksesPublik','akses_publik_id','id');
    }
    /**
     * Get the surat that owns the surat arsip kategori.
     */
    public function arsipKategori()
    {
        return $this->belongsTo('App\Models\MasterSuratArsipKategori','arsip_kategori_id','id');
    }
    /**
     * Get the surat that owns the surat arsip media.
     */
    public function arsipMedia()
    {
        return $this->belongsTo('App\Models\MasterSuratArsipMedia','arsip_media_id','id');
    }
    /**
     * Get the surat that owns the surat arsip vital.
     */
    public function arsipVital()
    {
        return $this->belongsTo('App\Models\MasterSuratArsipVital','arsip_vital_id','id');
    }
    /**
     * Get the surat that owns the surat arsip tipe.
     */
    public function arsipTipe()
    {
        return $this->belongsTo('App\Models\MasterSuratArsipTipe','arsip_tipe_id','id');
    }

    /**
     * Get the surat that owns the surat berkas.
     */
    public function berkas()
    {
        return $this->belongsTo('App\Models\SuratBerkas','berkas_id','id');
    }

    /**
     * Get the surat activity for the surat.
     */
    public function suratActivity()
    {
        return $this->hasMany('App\Models\SuratActivity','surat_id','id')->orderBy('created_at', 'DESC');
    }

    public function sumActivity()
    {
        return $this->hasMany('App\Models\SuratActivity','surat_id','id');
    }    

    public function from()
    {
        return $this->hasOne('App\Models\Inbox','surat_id', 'id');
    }

    public function asal_tujuan()
    {
        return $this->hasOne('App\Models\Inbox','surat_id', 'surat_id')->where('is_first', '1');
    }    

    public function to()
    {
        return $this->hasMany('App\Models\Inbox','surat_id','id')->where('is_first', '1');
    }

    public function is_read()
    {
        return $this->hasOne('App\Models\Inbox','surat_id','id');
    }

    /**
     * Get the surat activity tujuan for the surat.
     */
    public function suratActivityTujuan()
    {
        return $this->hasMany('App\Models\SuratActivityTujuan','surat_id','id');
    }

    /**
     * Get the surat activity file for the surat.
     */
    public function suratActivityFile()
    {
        return $this->hasMany('App\Models\SuratActivityFile','surat_id','id');
    }

}


/**
 * @SWG\Definition(
 *      definition="SuratSave",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="peruntukan",
 *          description="surat masuk=0 dan keluar=1",
 *          type="number",
 *          format="number"
 *      ),
 *      @SWG\Property(
 *          property="jenis_id",
 *          description="fk m_surat_jenis",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="tipe_id",
 *          description="fk m_surat_tipe (asli, copy)",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="sifat_id",
 *          description="fk m_surat_sifat (biasa,rahasia,sangat rahasia)",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="urgensi_id",
 *          description="fk m_surat_urgensi (biasa,segera,sangat segera)",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="akses_publik_id",
 *          description="fk m_surat_akses_publik (terbuka, terturup)",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="arsip_kategori_id",
 *          description="fk m_arsip_kategori (umum, terjaga)",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="arsip_media_id",
 *          description="fk m_surat_arsip_media (kertas)",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="arsip_vital_id",
 *          description="fk m_surat_arsip_vital (tidak vital, vital)",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="arsip_tipe_id",
 *          description="fk m_surat_arsip_tipe (lembar,halaman)",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="arsip_tipe_jumlah",
 *          description="jumlah halaman atau lembar",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="tanggal_surat",
 *          description="tanggal_surat",
 *          type="string",
 *          format="date"
 *      ),
 *      @SWG\Property(
 *          property="perihal",
 *          description="perihal",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="berkas_id",
 *          description="fk surat_berkas",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="tujuan",
 *          description="tujuan",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="no_surat_unit_kerja",
 *          description="no_surat_unit_kerja",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="no_agenda",
 *          description="no_agenda",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="pengirim",
 *          description="internal, external",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="surat_activity",
 *          description="surat_activity",
 *          type="object",
 *          ref="#/definitions/SuratActivitySave"
 *      )
 * )
 */
