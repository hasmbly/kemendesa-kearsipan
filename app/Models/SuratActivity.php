<?php

namespace App\Models;

use App\Traits\Uuid;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="SuratActivity",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="surat_id",
 *          description="surat_id",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="activity_type",
 *          description="masuk, keluar, disposisi, notadinas, teruskan",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="activity_sifat_id",
 *          description="activity_sifat_id",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="no_index",
 *          description="no_index",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="pesan",
 *          description="pesan",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="disposisi",
 *          description="json/text/array",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="asal_unit_kerja_id",
 *          description="asal_unit_kerja_id",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="asal_unit_kerja_nama",
 *          description="asal_unit_kerja_nama",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="deleted_at",
 *          description="deleted_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="surat_activity_file",
 *          description="surat_activity_file",
 *          type="object",
 *          ref="#/definitions/SuratActivityFile"
 *      ),
 *      @SWG\Property(
 *          property="surat_activity_tujuan",
 *          description="surat_activity_tujuan",
 *          type="object",
 *          ref="#/definitions/SuratActivityTujuan"
 *      )
 * )
 */
class SuratActivity extends Model
{
    use SoftDeletes, Uuid;

    public $table = 'surat_activity';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];

    public $incrementing = false;


    public $fillable = [
        'surat_id',
        'activity_type',
        'activity_sifat_id',
        'no_index',
        'pesan',
        'disposisi',
        'asal_unit_kerja_id',
        'asal_unit_kerja_nama'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'string',
        'surat_id' => 'string',
        'activity_type' => 'integer',
        'activity_sifat_id' => 'string',
        'no_index' => 'string',
        'pesan' => 'string',
        'disposisi' => 'string',
        'asal_unit_kerja_id' => 'string',
        'asal_unit_kerja_nama' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'surat_id' => 'required',
        'activity_type' => 'required'
    ];


    /**
     * Get the surat activity that owns the surat.
     */
    public function surat()
    {
        return $this->belongsTo('App\Models\Surat','surat_id','id');
    }

    /**
     * Get the surat activity that owns the surat.
     */
    public function activitySifat()
    {
        return $this->belongsTo('App\Models\MasterActivitySifat','activity_sifat_id','id');
    }

    /**
     * Get the surat activity file for the surat activity.
     */
    public function suratActivityFile()
    {
        return $this->hasMany('App\Models\SuratActivityFile','surat_activity_id','id');
    }

    /**
     * Get the surat activity tujuan for the surat.
     */
    public function suratActivityTujuan()
    {
        return $this->hasMany('App\Models\SuratActivityTujuan','surat_activity_id','id');
    }

}


/**
 * @SWG\Definition(
 *      definition="SuratActivitySave",
 *      required={""},
 *      @SWG\Property(
 *          property="activity_type",
 *          description="masuk, keluar, disposisi, notadinas, teruskan",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="activity_sifat_id",
 *          description="activity_sifat_id",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="no_index",
 *          description="no_index",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="pesan",
 *          description="pesan",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="disposisi",
 *          description="json/text/array",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="asal_unit_kerja_id",
 *          description="asal_unit_kerja_id",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="asal_unit_kerja_nama",
 *          description="asal_unit_kerja_nama",
 *          type="string"
 *      ),
 *     @SWG\Property(
 *          property="surat_activity_tujuan",
 *          description="surat_activity_tujuan",
 *          type="array",
 *          @SWG\Items(
 *              ref="#/definitions/SuratActivityTujuanSave"
 *          )
 *      ),
 *     @SWG\Property(
 *          property="surat_activity_tembusan",
 *          description="surat_activity_tembusan",
 *          type="array",
 *          @SWG\Items(
 *              ref="#/definitions/SuratActivityTembusanSave"
 *          )
 *      ),
 *      @SWG\Property(
 *          property="surat_activity_file",
 *          description="surat_activity_file",
 *          type="array",
 *          @SWG\Items(
 *              type="file"
 *          )
 *
 *      ),
 *
 * )
 */
