<?php

namespace App\Models;

use App\Traits\Uuid;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="SuratActivityFile",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="surat_id",
 *          description="surat_id",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="surat_activity_id",
 *          description="surat_activity_id",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="path",
 *          description="path",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="nama",
 *          description="nama",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="deleted_at",
 *          description="deleted_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class SuratActivityFile extends Model
{
    use SoftDeletes,Uuid;

    public $table = 'surat_activity_file';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];

    public $incrementing = false;


    public $fillable = [
        'surat_id',
        'surat_activity_id',
        'path',
        'nama'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'string',
        'surat_id' => 'string',
        'surat_activity_id' => 'string',
        'path' => 'string',
        'nama' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'surat_id' => 'required',
    ];


    /**
     * Get the activity that owns the surat activity file.
     */
    public function surat()
    {
        return $this->belongsTo('App\Models\Surat','surat_id');
    }

    /**
     * Get the activity that owns the surat activity file.
     */
    public function activity()
    {
        return $this->belongsTo('App\Models\SuratActivity','surat_activity_id');
    }
}
