<?php

namespace App\Models;

use App\Traits\Uuid;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="SuratActivityNoIncrement",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="fk_id",
 *          description="id uk atau yang lain",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="tahun",
 *          description="tahun penomoran",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="incremen",
 *          description="nomor urut tiap uk atau yang lain pertahun",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="deleted_at",
 *          description="deleted_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class SuratActivityNoIncrement extends Model
{
    use SoftDeletes,Uuid;

    public $table = 'surat_activity_no_increment';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];

    public $incrementing = false;


    public $fillable = [
        'fk_id',
        'tahun',
        'incremen'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'string',
        'fk_id' => 'string',
        'tahun' => 'integer',
        'incremen' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'fk_id' => 'required'
    ];


}
