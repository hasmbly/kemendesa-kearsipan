<?php

namespace App\Models;

use App\Traits\Uuid;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="SuratActivityTujuan",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="surat_id",
 *          description="surat_id",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="surat_activity_id",
 *          description="surat_activity_id",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="berkas_id",
 *          description="fk surat_berkas",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="tujuan_tembusan_id",
 *          description="tujuan / tembusan",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="tujuan_tembusan_nama",
 *          description="tujuan_tembusan_nama",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="tujuan_tembusan_status",
 *          description="0 tujuan, 1 tembusan",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="asal_unit_kerja_id",
 *          description="asal_unit_kerja_id",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="asal_unit_kerja_nama",
 *          description="asal_unit_kerja_nama",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="status",
 *          description="0 unread, 1 read",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="deleted_at",
 *          description="deleted_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class SuratActivityTujuan extends Model
{
    use SoftDeletes, Uuid;

    public $table = 'surat_activity_tujuan';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    // protected $dates = ['deleted_at'];

    public $incrementing = false;

    public $fillable = [
        'surat_id',
        'surat_activity_id',
        'berkas_id',
        'tujuan_tembusan_id',
        'tujuan_tembusan_nama',
        'tujuan_tembusan_status',
        'asal_unit_kerja_id',
        'asal_unit_kerja_nama',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'string',
        'surat_id' => 'string',
        'surat_activity_id' => 'string',
        'berkas_id' => 'string',
        'tujuan_tembusan_id' => 'string',
        'tujuan_tembusan_nama' => 'string',
        'tujuan_tembusan_status' => 'integer',
        'asal_unit_kerja_id' => 'string',
        'asal_unit_kerja_nama' => 'string',
        'status' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    /**
     * Get the surat activity tujuan that owns the surat.
     */
    public function surat()
    {
        return $this->belongsTo('App\Models\Surat','surat_id','id');
    }

    /**
     * Get the surat activity tujuan that owns the activity.
     */
    public function suratActivity()
    {
        return $this->belongsTo('App\Models\SuratActivity','surat_activity_id','surat_id','id');
    }

    public function is_read()
    {
        return $this->hasMany('App\Models\Inbox','ori_unor_surat_id_tujuan','tujuan_tembusan_id');
    }

}

/**
 * @SWG\Definition(
 *      definition="SuratActivityTujuanSave",
 *      required={""},
 *      @SWG\Property(
 *          property="berkas_id",
 *          description="fk surat_berkas",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="tujuan_tembusan_id",
 *          description="tujuan / tembusan",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="tujuan_tembusan_nama",
 *          description="tujuan_tembusan_nama",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="status",
 *          description="0 unread, 1 read",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */


/**
 * @SWG\Definition(
 *      definition="SuratActivityTembusanSave",
 *      required={""},
 *      @SWG\Property(
 *          property="berkas_id",
 *          description="fk surat_berkas",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="tujuan_tembusan_id",
 *          description="tujuan / tembusan",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="tujuan_tembusan_nama",
 *          description="tujuan_tembusan_nama",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="status",
 *          description="0 unread, 1 read",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
