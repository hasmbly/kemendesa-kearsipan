<?php

namespace App\Models;

use App\Traits\Uuid;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;


/**
 * @SWG\Definition(
 *      definition="SuratBerkas",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="uk_id",
 *          description="uk_id",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="uk_nama",
 *          description="uk_nama",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="berkas_no",
 *          description="berkas_no",
 *          type="integer"
 *      ),
 *      @SWG\Property(
 *          property="berkas_nama",
 *          description="berkas_nama",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="klasifikasi_id",
 *          description="ref master klasifikasi",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="klasifikasi_code",
 *          description="klasifikasi_code",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="retensi_hitung_dari",
 *          description="create, close",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="retensi_type_active",
 *          description="tgl,retensi_thn_active(ref klasifikasi = 4// )",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="retensi_active",
 *          description="retensi_active",
 *          type="string",
 *          format="date"
 *      ),
 *      @SWG\Property(
 *          property="retensi_type_inactive",
 *          description="tgl,retensi_thn_inactive(ref klasifikasi = 2// )",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="retensi_inactive",
 *          description="retensi_inactive",
 *          type="string",
 *          format="date"
 *      ),
 *      @SWG\Property(
 *          property="penyusutan_id",
 *          description="ref master penyusutan",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="lokasi_berkas",
 *          description="lokasi_berkas",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="deskripsi",
 *          description="deskripsi",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="status",
 *          description="active,inactive",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="deleted_at",
 *          description="deleted_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class SuratBerkas extends Model
{
    use SoftDeletes,Uuid;

    public $table = 'surat_berkas';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];

    public $incrementing = false;

    public $fillable = [
        'uk_id',
        'uk_nama',
        'berkas_no',
        'berkas_nama',
        'klasifikasi_id',
        'klasifikasi_code',
        'retensi_hitung_dari',
        'retensi_type_active',
        'retensi_active',
        'retensi_type_inactive',
        'retensi_inactive',
        'penyusutan_id',
        'lokasi_berkas',
        'deskripsi',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'string',
        'uk_id' => 'string',
        'uk_nama' => 'string',
        'berkas_no' => 'integer',
        'berkas_nama' => 'string',
        'klasifikasi_id' => 'string',
        'klasifikasi_code' => 'string',
        'retensi_hitung_dari' => 'string',
        'retensi_type_active' => 'string',
        'retensi_active' => 'date',
        'retensi_type_inactive' => 'string',
        'retensi_inactive' => 'date',
        'penyusutan_id' => 'string',
        'lokasi_berkas' => 'string',
        'deskripsi' => 'string',
        'status' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'uk_id' => 'required',
        'berkas_no' => 'required',
        'berkas_nama' => 'required',
        'klasifikasi_id' => 'required',
        'klasifikasi_code' => 'required',
//        'retensi_type_active' => 'required',
//        'retensi_active' => 'required',
//        'retensi_type_inactive' => 'required',
//        'retensi_inactive' => 'required'
    ];

    /**
     * Get the surat for the berkas.
     */
    public function surat()
    {
        return $this->hasMany('App\Models\Surat','berkas_id','id');
    }

    /**
     * Get the retensi_active for the berkas.
     */
    public function retensi_active_until()
    {
        return $this->hasOne('App\Models\SuratBerkas','id')
        ->join('m_surat_klasifikasi', 'm_surat_klasifikasi.id', '=', 'surat_berkas.klasifikasi_id')
        ->select(
            'surat_berkas.id',
            'surat_berkas.created_at as surat_berkas_created_at',
            'm_surat_klasifikasi.retensi_thn_active'
        )
        ->selectRaw(
            'surat_berkas.created_at + interval m_surat_klasifikasi.retensi_thn_active year as active_until'
        );
    }

    public function surat_terkait()
    {
        // return $this->hasMany('App\Models\Surat','berkas_id','id')->select('surat.berkas_id','surat.id as surat_id', 'surat.no_surat_unit_kerja', 'surat.perihal');
        
        return $this->hasMany('App\Models\Surat','berkas_id','id')
               ->join('inbox', 'inbox.surat_id', '=', 'surat.id')
                ->join('m_surat_sifat', 'm_surat_sifat.id', '=', 'surat.sifat_id')
                ->select(
                    'surat.berkas_id', 
                    'surat.id as surat_id',
                    'surat.no_surat_unit_kerja',
                    'surat.perihal',
                    'm_surat_sifat.nama as sifat_nama', 
                    'm_surat_sifat.deskripsi as sifat_deskripsi', 
                    'inbox.ori_unor_surat_nama_asal as asal_surat', 
                    'inbox.ori_unor_surat_nama_tujuan as tujuan_surat',
                    'surat.created_at'
                )
                ->where('is_first', '1');
    }

}