<?php

namespace App\Models;

use Eloquent as Model;

/**
 * @SWG\Definition(
 *      definition="SuratTemplate",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer"
 *      ),
 *      @SWG\Property(
 *          property="judul",
 *          description="judul",
 *          type="string"
 *      ),
 *      @SWG\Property(  
 *          property="links",
 *          description="links",
 *          type="string"
 *      )
 * )
 */
class SuratTemplate extends Model
{
    public $table = 'surat_template';

    public $fillable = [
        'id',
        'judul',
        'links'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'        => 'integer',
        'judul'     => 'string',
        'links'     => 'string'
    ];    

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'judul' => 'required',
        'links' => 'required'
    ];
       
}