<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface MasterActivitySifatRepository.
 *
 * @package namespace App\Repositories;
 */
interface MasterActivitySifatRepository extends RepositoryInterface
{
    //
}
