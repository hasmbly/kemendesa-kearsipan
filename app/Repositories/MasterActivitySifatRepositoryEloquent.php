<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\MasterActivitySifatRepository;
use App\Models\MasterActivitySifat;
use App\Validators\MasterActivitySifatValidator;

/**
 * Class MasterActivitySifatRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class MasterActivitySifatRepositoryEloquent extends BaseRepository implements MasterActivitySifatRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return MasterActivitySifat::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nama' => 'like',
        'deskripsi' => 'like'
    ];
}
