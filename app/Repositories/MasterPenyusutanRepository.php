<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface MasterPenyusutanRepository.
 *
 * @package namespace App\Repositories;
 */
interface MasterPenyusutanRepository extends RepositoryInterface
{
    //
}
