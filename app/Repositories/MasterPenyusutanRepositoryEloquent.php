<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\MasterPenyusutanRepository;
use App\Models\MasterPenyusutan;
use App\Validators\MasterPenyusutanValidator;

/**
 * Class MasterPenyusutanRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class MasterPenyusutanRepositoryEloquent extends BaseRepository implements MasterPenyusutanRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return MasterPenyusutan::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }


    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nama' => 'like',
        'deskripsi' => 'like'
    ];

}
