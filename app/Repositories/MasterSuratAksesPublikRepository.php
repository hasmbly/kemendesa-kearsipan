<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface MasterSuratAksesPublikRepository.
 *
 * @package namespace App\Repositories;
 */
interface MasterSuratAksesPublikRepository extends RepositoryInterface
{
    //
}
