<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\MasterSuratAksesPublikRepository;
use App\Models\MasterSuratAksesPublik;
use App\Validators\MasterSuratAksesPublikValidator;

/**
 * Class MasterSuratAksesPublikRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class MasterSuratAksesPublikRepositoryEloquent extends BaseRepository implements MasterSuratAksesPublikRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return MasterSuratAksesPublik::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nama' => 'like',
        'deskripsi' => 'like'
    ];
}
