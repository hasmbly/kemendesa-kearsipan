<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface MasterSuratArsipKategoriRepository.
 *
 * @package namespace App\Repositories;
 */
interface MasterSuratArsipKategoriRepository extends RepositoryInterface
{
    //
}
