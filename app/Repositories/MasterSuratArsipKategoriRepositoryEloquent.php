<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\MasterSuratArsipKategoriRepository;
use App\Models\MasterSuratArsipKategori;
use App\Validators\MasterSuratArsipKategoriValidator;

/**
 * Class MasterSuratArsipKategoriRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class MasterSuratArsipKategoriRepositoryEloquent extends BaseRepository implements MasterSuratArsipKategoriRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return MasterSuratArsipKategori::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nama' => 'like',
        'deskripsi' => 'like'
    ];
}
