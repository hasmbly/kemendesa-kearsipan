<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface MasterSuratArsipMediaRepository.
 *
 * @package namespace App\Repositories;
 */
interface MasterSuratArsipMediaRepository extends RepositoryInterface
{
    //
}
