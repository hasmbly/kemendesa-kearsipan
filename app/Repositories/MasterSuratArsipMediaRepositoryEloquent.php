<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\MasterSuratArsipMediaRepository;
use App\Models\MasterSuratArsipMedia;
use App\Validators\MasterSuratArsipMediaValidator;

/**
 * Class MasterSuratArsipMediaRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class MasterSuratArsipMediaRepositoryEloquent extends BaseRepository implements MasterSuratArsipMediaRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return MasterSuratArsipMedia::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nama' => 'like',
        'deskripsi' => 'like'
    ];
}
