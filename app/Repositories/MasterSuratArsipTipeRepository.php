<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface MasterSuratArsipTipeRepository.
 *
 * @package namespace App\Repositories;
 */
interface MasterSuratArsipTipeRepository extends RepositoryInterface
{
    //
}
