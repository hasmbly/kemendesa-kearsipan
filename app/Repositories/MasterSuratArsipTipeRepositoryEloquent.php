<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\MasterSuratArsipTipeRepository;
use App\Models\MasterSuratArsipTipe;
use App\Validators\MasterSuratArsipTipeValidator;

/**
 * Class MasterSuratArsipTipeRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class MasterSuratArsipTipeRepositoryEloquent extends BaseRepository implements MasterSuratArsipTipeRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return MasterSuratArsipTipe::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nama' => 'like',
        'deskripsi' => 'like'
    ];
}
