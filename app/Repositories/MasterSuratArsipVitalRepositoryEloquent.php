<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\MasterSuratArsipVitalRepository;
use App\Models\MasterSuratArsipVital;
use App\Validators\MasterSuratArsipVitalValidator;

/**
 * Class MasterSuratArsipVitalRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class MasterSuratArsipVitalRepositoryEloquent extends BaseRepository implements MasterSuratArsipVitalRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return MasterSuratArsipVital::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nama' => 'like',
        'deskripsi' => 'like'
    ];
}
