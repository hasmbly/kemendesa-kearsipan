<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface MasterSuratDisposisiRepository.
 *
 * @package namespace App\Repositories;
 */
interface MasterSuratDisposisiRepository extends RepositoryInterface
{
    //
}
