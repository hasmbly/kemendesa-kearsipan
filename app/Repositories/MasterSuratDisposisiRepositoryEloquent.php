<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\MasterSuratDisposisiRepository;
use App\Models\MasterSuratDisposisi;
use App\Validators\MasterSuratDisposisiValidator;

/**
 * Class MasterSuratDisposisiRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class MasterSuratDisposisiRepositoryEloquent extends BaseRepository implements MasterSuratDisposisiRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return MasterSuratDisposisi::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nama' => 'like',
//        'jabaran_id' => 'like',
        'deskripsi' => 'like'
    ];

}
