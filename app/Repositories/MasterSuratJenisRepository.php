<?php

namespace App\Repositories;

use App\Models\MasterSuratJenis;
use App\Repositories\BaseRepository;

/**
 * Class MasterSuratJenisRepository
 * @package App\Repositories
 * @version October 4, 2019, 9:30 am UTC
*/

class MasterSuratJenisRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nama',
        'deskripsi'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return MasterSuratJenis::class;
    }
}
