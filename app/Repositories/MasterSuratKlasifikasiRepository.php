<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface MasterSuratKlasifikasiRepository.
 *
 * @package namespace App\Repositories;
 */
interface MasterSuratKlasifikasiRepository extends RepositoryInterface
{
    //
}
