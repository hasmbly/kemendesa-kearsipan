<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\MasterSuratKlasifikasiRepository;
use App\Models\MasterSuratKlasifikasi;
use App\Validators\MasterSuratKlasifikasiValidator;

/**
 * Class MasterSuratKlasifikasiRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class MasterSuratKlasifikasiRepositoryEloquent extends BaseRepository implements MasterSuratKlasifikasiRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return MasterSuratKlasifikasi::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }


    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => 'like',
        'parent_id' => 'like',
        'code' => 'like',
        'nama' => 'like',
        'keterangan' => 'like',
        'retensi_thn_active' => 'like',
        'retensi_thn_inactive' => 'like',
        'susut_id' => 'like',
        'status' => 'like',
        'status_parent' => 'like'
    ];

}
