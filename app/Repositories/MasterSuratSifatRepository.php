<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface MasterSuratSifatRepository.
 *
 * @package namespace App\Repositories;
 */
interface MasterSuratSifatRepository extends RepositoryInterface
{
    //
}
