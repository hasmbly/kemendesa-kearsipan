<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\MasterSuratSifatRepository;
use App\Models\MasterSuratSifat;
use App\Validators\MasterSuratSifatValidator;

/**
 * Class MasterSuratSifatRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class MasterSuratSifatRepositoryEloquent extends BaseRepository implements MasterSuratSifatRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return MasterSuratSifat::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nama' => 'like',
        'deskripsi' => 'like'
    ];

}
