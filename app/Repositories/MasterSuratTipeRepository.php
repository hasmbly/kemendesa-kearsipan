<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface MasterSuratTipeRepository.
 *
 * @package namespace App\Repositories;
 */
interface MasterSuratTipeRepository extends RepositoryInterface
{
    //
}
