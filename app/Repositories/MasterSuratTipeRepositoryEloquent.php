<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\MasterSuratTipeRepository;
use App\Models\MasterSuratTipe;
use App\Validators\MasterSuratTipeValidator;

/**
 * Class MasterSuratTipeRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class MasterSuratTipeRepositoryEloquent extends BaseRepository implements MasterSuratTipeRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return MasterSuratTipe::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nama' => 'like',
        'deskripsi' => 'like'
    ];

}
