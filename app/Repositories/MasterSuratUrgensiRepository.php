<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface MasterSuratUrgensiRepository.
 *
 * @package namespace App\Repositories;
 */
interface MasterSuratUrgensiRepository extends RepositoryInterface
{
    //
}
