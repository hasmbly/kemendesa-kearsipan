<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\MasterSuratUrgensiRepository;
use App\Models\MasterSuratUrgensi;
use App\Validators\MasterSuratUrgensiValidator;

/**
 * Class MasterSuratUrgensiRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class MasterSuratUrgensiRepositoryEloquent extends BaseRepository implements MasterSuratUrgensiRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return MasterSuratUrgensi::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nama' => 'like',
        'deskripsi' => 'like'
    ];

}
