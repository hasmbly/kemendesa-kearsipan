<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface SuratActivityRepository.
 *
 * @package namespace App\Repositories;
 */
interface SuratActivityRepository extends RepositoryInterface
{
    //
}
