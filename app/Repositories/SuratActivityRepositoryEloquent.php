<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\SuratActivityRepository;
use App\Models\SuratActivity;
use App\Validators\SuratActivityValidator;

/**
 * Class SuratActivityRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class SuratActivityRepositoryEloquent extends BaseRepository implements SuratActivityRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return SuratActivity::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => 'like',
        'no_index' => 'like',
        'pesan' => 'like',
        'asal_unit_kerja_id' => 'like',
        'asal_unit_kerja_nama' => 'like'
    ];

}
