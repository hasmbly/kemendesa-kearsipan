<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface SuratBerkasRepository.
 *
 * @package namespace App\Repositories;
 */
interface SuratBerkasRepository extends RepositoryInterface
{
    //
}
