<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\SuratBerkasRepository;
use App\Models\SuratBerkas;
use App\Validators\SuratBerkasValidator;

/**
 * Class SuratBerkasRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class SuratBerkasRepositoryEloquent extends BaseRepository implements SuratBerkasRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return SuratBerkas::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => 'like',
        'uk_id' => 'like',
        'uk_nama' => 'like',
        'berkas_no' => 'like',
        'berkas_nama' => 'like',
        'klasifikasi_id' => 'like',
        'klasifikasi_code' => 'like',
        'retensi_hitung_dari' => 'like',
        'retensi_type_active' => 'like',
        'retensi_active' => 'like',
        'retensi_type_inactive' => 'like',
        'retensi_inactive' => 'like',
        'penyusutan_id' => 'like',
        'lokasi_berkas' => 'like',
        'deskripsi' => 'like',
        'status' => 'like'
    ];
}
