<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface SuratJenisRepository.
 *
 * @package namespace App\Repositories;
 */
interface SuratJenisRepository extends RepositoryInterface
{
    //
}
