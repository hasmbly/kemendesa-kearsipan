<?php

namespace App\Repositories;

use App\Models\MasterSuratJenis;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\SuratJenisRepository;
use App\Entities\SuratJenis;
use App\Validators\SuratJenisValidator;

/**
 * Class SuratJenisRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class SuratJenisRepositoryEloquent extends BaseRepository implements SuratJenisRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return MasterSuratJenis::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nama' => 'like',
        'deskripsi' => 'like'
    ];

}
