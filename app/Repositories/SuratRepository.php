<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface SuratRepository.
 *
 * @package namespace App\Repositories;
 */
interface SuratRepository extends RepositoryInterface
{
    //
}
