<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\SuratRepository;
use App\Models\Surat;
use App\Validators\SuratValidator;

/**
 * Class SuratRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class SuratRepositoryEloquent extends BaseRepository implements SuratRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Surat::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public $fieldSearchable = [
        'id' => 'like',
        'peruntukan' => 'like',
        'jenis_id' => 'like',
        'tipe_id' => 'like',
        'sifat_id' => 'like',
        'urgensi_id' => 'like',
        'akses_publik_id' => 'like',
        'arsip_kategori_id' => 'like',
        'arsip_media_id' => 'like',
        'arsip_vital_id' => 'like',
        'arsip_tipe_id' => 'like',
        'arsip_tipe_jumlah' => 'like',
        'tanggal_surat' => 'like',
        'perihal' => 'like',
        'berkas_id' => 'like',
        'tujuan' => 'like',
        'no_surat_unit_kerja' => 'like',
        'no_agenda' => 'like',
        'jenis.nama' => 'like',
        'surat_activity.asal_unit_kerja_nama' => 'like'
    ];

}
