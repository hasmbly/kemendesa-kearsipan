<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface SuratTemplateRepository.
 *
 * @package namespace App\Repositories;
 */
interface SuratTemplateRepository extends RepositoryInterface
{
    //
}
