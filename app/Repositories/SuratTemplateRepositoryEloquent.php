<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\SuratTemplateRepository;
use App\Models\SuratTemplate;
use App\Validators\SuratTemplateValidator;

/**
 * Class SuratTemplateRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class SuratTemplateRepositoryEloquent extends BaseRepository implements SuratTemplateRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return SuratTemplate::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }


    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => 'like',
        'judul' => 'like',
        'links' => 'like'
    ];

}
