<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuratTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('surat', function (Blueprint $table) {
            $table->string('id',100)->primary();
            $table->decimal('peruntukan',1,0)->nullable()->default(0)->comment('surat masuk=0 dan keluar=1');
            $table->string('jenis_id',100)->comment('fk m_surat_jenis');
            $table->string('tipe_id',100)->nullable()->comment('fk m_surat_tipe (asli, copy)');
            $table->string('sifat_id',100)->nullable()->comment('fk m_surat_sifat (biasa,rahasia,sangat rahasia)');
            $table->string('urgensi_id',100)->nullable()->comment('fk m_surat_urgensi (biasa,segera,sangat segera)');
            $table->string('akses_publik_id',100)->nullable()->comment('fk m_surat_akses_publik (terbuka, terturup)');
            $table->string('arsip_kategori_id',100)->nullable()->comment('fk m_arsip_kategori (umum, terjaga)');
            $table->string('arsip_media_id',100)->nullable()->comment('fk m_surat_arsip_media (kertas)');
            $table->string('arsip_vital_id',100)->nullable()->comment('fk m_surat_arsip_vital (tidak vital, vital)');
            $table->string('arsip_tipe_id',100)->nullable()->comment('fk m_surat_arsip_tipe (lembar,halaman)');
            $table->integer('arsip_tipe_jumlah',false,true)->default(1)->nullable()->comment('jumlah halaman atau lembar');
            $table->date('tanggal_surat');
            $table->text('perihal');
            $table->string('berkas_id',100)->nullable()->comment('fk surat_berkas');
            $table->string('tujuan',255)->nullable();
            $table->string('no_surat_unit_kerja',100);
            $table->string('no_agenda',100)->nullable();
            $table->string('pengirim',15)->nullable()->default('internal')->comment('internal, external');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surat');
    }
}
