<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuratActivityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('surat_activity', function (Blueprint $table) {
            $table->string('id',100)->primary();
            $table->string('surat_id',100);
            $table->integer('activity_type',false,false)->comment('masuk, keluar, disposisi, notadinas, teruskan');
            $table->text('no_index')->nullable();
            $table->text('pesan')->nullable();
            $table->text('disposisi')->nullable()->comment('json/text/array');
            $table->string('asal_unit_kerja_id',100)->nullable();
            $table->string('asal_unit_kerja_nama',255)->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surat_activity');
    }
}
