<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuratActivityNoIncrementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('surat_activity_no_increment', function (Blueprint $table) {
            $table->string('id',100)->primary();
            $table->string('fk_id')->comment('id uk atau yang lain');
            $table->integer('tahun',false,true)->nullable()->comment('tahun penomoran');
            $table->integer('incremen',false,true)->nullable()->comment('nomor urut tiap uk atau yang lain pertahun');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surat_activity_no_increment');
    }
}
