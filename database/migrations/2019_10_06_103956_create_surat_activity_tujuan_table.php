<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuratActivityTujuanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('surat_activity_tujuan', function (Blueprint $table) {
            $table->string('id',100)->primary();
            $table->string('surat_id',100)->nullable();
            $table->string('surat_activity_id',100)->nullable();
            $table->string('berkas_id',100)->nullable()->comment('fk surat_berkas');
            $table->string('tujuan_tembusan_id',100)->nullable()->comment('tujuan / tembusan');
            $table->string('tujuan_tembusan_nama',255)->nullable();
            $table->integer('tujuan_tembusan_status',false,false)->nullable()->default(0)->comment('0 tujuan, 1 tembusan');
            $table->string('asal_unit_kerja_id',100)->nullable();
            $table->string('asal_unit_kerja_nama',255)->nullable();
            $table->integer('status',false,false)->nullable()->default(0)->comment('0 unread, 1 read');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surat_activity_tujuan');
    }
}
