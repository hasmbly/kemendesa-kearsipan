<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuratActivityFileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('surat_activity_file', function (Blueprint $table) {
            $table->string('id',100)->primary();
            $table->string('surat_id',100)->nullable();
            $table->string('surat_activity_id',100)->nullable();
            $table->text('path')->nullable();
            $table->string('nama',150)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surat_activity_file');
    }
}
