<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMPenyusutanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_penyusutan', function (Blueprint $table) {
            $table->string('id',100)->primary();
            $table->string('nama',150);
            $table->string('deskripsi')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
        \Illuminate\Support\Facades\DB::table('m_penyusutan')->insert([
            ['id' =>'YnHdCRGbhF.1', 'nama' => 'Musnah'],
            ['id' =>'YnHdCRGbhF.2', 'nama' => 'Permanen'],
            ['id' =>'YnHdCRGbhF.3', 'nama' => 'Dinilai Kembali']
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_penyusutan');
    }
}
