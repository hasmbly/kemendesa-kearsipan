<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterSuratActivityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('surat_activity', function (Blueprint $table) {
            $table->string('activity_sifat_id',100)->nullable()->after('activity_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('surat_activity', function (Blueprint $table) {
            $table->dropColumn('activity_sifat_id');
        });
        //
    }
}
