<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuratBerkasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('surat_berkas', function (Blueprint $table) {
            $table->string('id',100)->primary();
            $table->string('uk_id',100);
            $table->string('uk_nama',250)->nullable();
            $table->bigInteger('berkas_no',false,false);
            $table->string('berkas_nama',200);
            $table->string('klasifikasi_id',100)->comment('ref master klasifikasi');
            $table->string('klasifikasi_code',100);
            $table->string('retensi_hitung_dari',20)->comment('create, close')->default('create')->nullable();
            $table->string('retensi_type_active',20)->comment('tgl,retensi_thn_active(ref klasifikasi = 4// )')->nullable();
            $table->date('retensi_active')->nullable();
            $table->string('retensi_type_inactive',20)->comment('tgl,retensi_thn_inactive(ref klasifikasi = 2// )')->nullable();
            $table->date('retensi_inactive')->nullable();
            $table->string('penyusutan_id',100)->comment('ref master penyusutan')->nullable();
            $table->string('lokasi_berkas',200)->nullable();
            $table->text('deskripsi')->nullable();
            $table->string('status',20)->comment('active,inactive')->default('active')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surat_berkas');
    }
}
