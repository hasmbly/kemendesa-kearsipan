<?php

use App\Models\SuratJenis;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(MasterSuratTipesTableSeeder::class);
        $this->call(MasterSuratUrgensisTableSeeder::class);
        $this->call(MasterSuratAksesPubliksTableSeeder::class);
        $this->call(MasterSuratArsipKategorisTableSeeder::class);
        $this->call(MasterSuratArsipMediaTableSeeder::class);
        $this->call(MasterSuratArsipVitalsTableSeeder::class);
        $this->call(MasterSuratArsipTipesTableSeeder::class);

//        $this->call(MasterSuratJenisTableSeeder::class);
        $this->call(MasterPenyusutansTableSeeder::class);
        \$this->call(MasterSuratKlasifikasisTableSeeder::class);
    }
}
