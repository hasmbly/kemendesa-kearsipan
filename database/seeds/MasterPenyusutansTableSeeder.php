<?php

use Illuminate\Database\Seeder;

class MasterPenyusutansTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\MasterPenyusutan::create(["id"=>"YnHdCRGbhF.1","nama"=>"Musnah", "deskripsi"=>"Musnah"]);
        \App\Models\MasterPenyusutan::create(["id"=>"YnHdCRGbhF.2","nama"=>"Permanen", "deskripsi"=>"Permanen"]);
        \App\Models\MasterPenyusutan::create(["id"=>"YnHdCRGbhF.3","nama"=>"Dinilai Kembali", "deskripsi"=>"Dinilai Kembali"]);
    }
}
