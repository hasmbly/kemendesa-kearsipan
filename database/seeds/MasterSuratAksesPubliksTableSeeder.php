<?php

use Illuminate\Database\Seeder;
use App\Models\MasterSuratAksesPublik;

class MasterSuratAksesPubliksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MasterSuratAksesPublik::create(["nama"=>"Terbuka", "deskripsi"=>"Terbuka"]);
        MasterSuratAksesPublik::create(["nama"=>"Tertutup", "deskripsi"=>"Tertutup"]);
    }
}
