<?php

use Illuminate\Database\Seeder;
use App\Models\MasterSuratArsipKategori;

class MasterSuratArsipKategorisTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MasterSuratArsipKategori::create(["nama"=>"Umum", "deskripsi"=>"Umum"]);
        MasterSuratArsipKategori::create(["nama"=>"Terjaga", "deskripsi"=>"Terjaga"]);
    }
}
