<?php

use Illuminate\Database\Seeder;
use App\Models\MasterSuratArsipMedia;

class MasterSuratArsipMediaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MasterSuratArsipMedia::create(["nama"=>"Kertas", "deskripsi"=>"Kertas"]);
    }
}
