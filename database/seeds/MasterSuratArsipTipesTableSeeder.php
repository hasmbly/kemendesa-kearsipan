<?php

use Illuminate\Database\Seeder;
use App\Models\MasterSuratArsipTipe;

class MasterSuratArsipTipesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MasterSuratArsipTipe::create(["nama"=>"Lembar", "deskripsi"=>"Lembar"]);
        MasterSuratArsipTipe::create(["nama"=>"Halaman", "deskripsi"=>"Halaman"]);
    }
}
