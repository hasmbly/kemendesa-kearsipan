<?php

use Illuminate\Database\Seeder;
use App\Models\MasterSuratArsipVital;

class MasterSuratArsipVitalsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MasterSuratArsipVital::create(["nama"=>"Tidak Vital", "deskripsi"=>"Tidak Vital"]);
        MasterSuratArsipVital::create(["nama"=>"Vital", "deskripsi"=>"Vital"]);
    }
}
