<?php

use Illuminate\Database\Seeder;
use App\Models\MasterSuratJenis;

class MasterSuratJenisTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        MasterSuratJenis::create(["nama"=>"Berita Acara", "deskripsi"=>"Berita acara serah terima pekerjaan"]);
        MasterSuratJenis::create(["nama"=>"Disposisi", "deskripsi"=>"Pelimpahan penugasan"]);
        MasterSuratJenis::create(["nama"=>"Memorandum", "deskripsi"=>"Surat arahan dari atasan kepada bawahan"]);
        MasterSuratJenis::create(["nama"=>"Nota Dinas", "deskripsi"=>"Surat dari bawahan kepada atasan"]);
    }
}
