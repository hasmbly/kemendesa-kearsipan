<?php

use Illuminate\Database\Seeder;
use App\Models\MasterSuratTipe;

class MasterSuratTipesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MasterSuratTipe::create(["nama"=>"Umum", "deskripsi"=>"Umum"]);
        MasterSuratTipe::create(["nama"=>"Terjaga", "deskripsi"=>"Terjaga"]);

    }
}
