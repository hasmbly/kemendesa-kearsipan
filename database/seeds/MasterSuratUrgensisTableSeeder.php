<?php

use Illuminate\Database\Seeder;
use App\Models\MasterSuratUrgensi;

class MasterSuratUrgensisTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MasterSuratUrgensi::create(["nama"=>"Biasa", "deskripsi"=>"Biasa"]);
        MasterSuratUrgensi::create(["nama"=>"Segera", "deskripsi"=>"Segera"]);
        MasterSuratUrgensi::create(["nama"=>"Sangat Segera", "deskripsi"=>"Sangat Segera"]);
    }
}
