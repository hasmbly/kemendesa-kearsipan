<!DOCTYPE html>
<html>
<head>
<style>
#surats {
  font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

#surats td, #surats th {
  border: 1px solid #ddd;
  padding: 8px;
}

#surats tr:nth-child(even){background-color: #f2f2f2;}

#surats tr:hover {background-color: #ddd;}

#surats th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  background-color: #4CAF50;
  color: white;
}
</style>
</head>
<body>
<table id="surats">
    <thead>
	<tr>
	    @foreach($data[0] as $key => $value)
		<th>{{ ucfirst($key) }}</th>
	    @endforeach
    	</tr>
    </thead>
    <tbody>
    @foreach($data as $row)
    	<tr>
        @foreach ($row as $value)
    	    <td>{{ $value }}</td>
        @endforeach        
	</tr>
    @endforeach
    </tbody>
</table>

</body>
</html>