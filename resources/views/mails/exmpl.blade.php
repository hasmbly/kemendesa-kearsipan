@component('mail::message')
Dengan Hormat **{{ $name }}**,

Nomor : **{{ $nomor_surat }}**

Perihal : **{{ $perihal }}**

Informasi Pengirim

Unor : **{{ $unor_pengirim }}**

Nama : **{{ $nama_pengirim }}**

Salam APIK

Email ini otomatis dikirimkan oleh sistem informasi dan kearsipan (SISURIP)
http://sisurip.kemendesa.go.id
@endcomponent