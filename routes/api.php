<?php

use Illuminate\Http\Request;
use App\Mail\MailtrapExample;
use Illuminate\Support\Facades\Mail;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::fallback(function(){
//    return response()->json([
//        'message' => 'Page Not Found. If error persists, contact info@website.com'], 404);
//});

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::post('auth/login', 'AuthAPIController@login');

Route::group([
    'middleware' => 'check'
], function () {
    /*
     * Other
     */
    Route::get('auth/user', 'AuthAPIController@user');

    /*
     * Reference
     */
    Route::get('reference/uke2BerkasCount', 'ReferenceAPIController@refGetUke2BerkasCount');
    Route::get('reference/chart', 'ReferenceAPIController@refChart');
    Route::get('reference/suratJenis', 'ReferenceAPIController@refSuratJenis');
    Route::get('reference/suratSifat', 'ReferenceAPIController@refSuratSifat');
    Route::get('reference/suratDisposisi', 'ReferenceAPIController@refSuratDisposisi');
    Route::get('reference/suratTipe', 'ReferenceAPIController@refSuratTipe');
    Route::get('reference/suratUrgensi', 'ReferenceAPIController@refSuratUrgensi');
    Route::get('reference/suratAksesPublik', 'ReferenceAPIController@refSuratAksesPublik');
    Route::get('reference/suratArsipKategori', 'ReferenceAPIController@refSuratArsipKategori');
    Route::get('reference/suratArsipMedia', 'ReferenceAPIController@refSuratArsipMedia');
    Route::get('reference/suratArsipVital', 'ReferenceAPIController@refSuratArsipVital');
    Route::get('reference/suratArsipTipe', 'ReferenceAPIController@refSuratArsipTipe');
    Route::get('reference/penyusutan', 'ReferenceAPIController@refPenyusutan');
    Route::get('reference/suratKlasifikasi', 'ReferenceAPIController@refSuratKlasifikasi');
    Route::get('reference/activitySifat', 'ReferenceAPIController@refActivitySifat');
    Route::get('reference/suratBerkas', 'ReferenceAPIController@refSuratBerkas');
    Route::get('reference/suratBerkasIncrementNo/{id}', 'ReferenceAPIController@refSuratBerkasIncrementNo');
    // Route::get('reference/suratKlasifikasiTree', 'ReferenceAPIController@refSuratKlasifikasiTree');
    
    /*
     * Mean
     */
    Route::get('suratActivities/{surat_id}', 'SuratActivityAPIController@index');
    Route::post('suratActivities/{surat_id}', 'SuratActivityAPIController@store');
    Route::get('suratActivities/{surat_id}/{id}', 'SuratActivityAPIController@show');
    Route::put('suratActivities/{surat_id}/{id}', 'SuratActivityAPIController@update');
    Route::delete('suratActivities/{id}', 'SuratActivityAPIController@destroy');
//    Route::post('suratActivities/coba/{surat_id}/{id}', 'SuratActivityAPIController@coba');

    Route::get('download', 'DownloadAPIController@download');

    Route::post('surats/berkas', 'SuratAPIController@updateBerkas');
    Route::post('surats/is_read', 'SuratAPIController@is_read');

    Route::resource('surats', 'SuratAPIController');
    //  Route::resource('suratActivities', 'SuratActivityAPIController');

    /*
     * Master
     */
    Route::resource('masterSuratJenis', 'MasterSuratJenisAPIController');
    Route::resource('masterSuratSifats', 'MasterSuratSifatAPIController');
    Route::resource('masterSuratDisposisis', 'MasterSuratDisposisiAPIController');
    Route::resource('masterSuratTipes', 'MasterSuratTipeAPIController');
    Route::resource('masterSuratUrgensis', 'MasterSuratUrgensiAPIController');
    Route::resource('masterSuratAksesPubliks', 'MasterSuratAksesPublikAPIController');
    Route::resource('masterSuratArsipKategoris', 'MasterSuratArsipKategoriAPIController');
    Route::resource('masterSuratArsipMedia', 'MasterSuratArsipMediaAPIController');
    Route::resource('masterSuratArsipVitals', 'MasterSuratArsipVitalAPIController');
    Route::resource('masterSuratArsipTipes', 'MasterSuratArsipTipeAPIController');
    Route::resource('masterPenyusutans', 'MasterPenyusutanAPIController');
    Route::resource('masterSuratKlasifikasis', 'MasterSuratKlasifikasiAPIController');
    Route::resource('masterActivitySifats', 'MasterActivitySifatAPIController');

    // berkas
    Route::get('suratBerkas/daftarIsi', 'SuratBerkasAPIController@daftarIsi');
    Route::get('suratBerkas/exportExcel', 'SuratBerkasAPIController@exportExcel');
    Route::resource('suratBerkas', 'SuratBerkasAPIController');

    /**
     * SuratTemplate
     */
    Route::resource('suratTemplate', 'SuratTemplateAPIController');

});

