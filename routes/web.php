<?php
use Illuminate\Http\Request;
use App\Mail\MailtrapExample;
use Illuminate\Support\Facades\Mail;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
//    return view('welcome');
    return redirect("/api/docs");
});

// Route::get('/mailtest', function () {

//         $email          = 'justhasby@gmail.com';
//         $name           = 'Mr. Oke';
//         $nomor_surat    = '012345';
//         $perihal        = 'Perihal testing';
//         $unor_pengirim  = 'Unor ORMAS';
//         $nama_pengirim  = 'Brapto';

//     // foreach ($dummy as $key => $value) {

//         return Mail::to($email)->send(new MailtrapExample(
//             $name,
//             $nomor_surat,
//             $perihal,
//             $unor_pengirim,
//             $nama_pengirim
//         ));        
//     // }


// });

Route::get('/clear-cache', function() {
    Artisan::call('cache:clear');
    Artisan::call('config:cache');
    Artisan::call('view:clear');
    return "Cache is cleared";
});

Route::get('/clear-config', function() {
    Artisan::call('config:clear');
    return "Confige is cleared";
});

Route::get('/clear-view', function() {
    Artisan::call('view:clear');
    return "View is cleared";
});

Route::get('/vendor-publish', function() {
    Artisan::call('vendor:publish --all');
    return "vendor is publish all";
});


Route::get('/migrate-rollback/{jml}', function($jml) {
    if($jml) {
        Artisan::call('migrate:rollback --step=' . $jml);
    }else{
        Artisan::call('migrate:rollback');
    }
    return "migrate rollback";
});

Route::get('/migrate', function() {
    Artisan::call('migrate');
    return "migrate";
});
//
//Route::get('/seed', function() {
//    Artisan::call('db:seed');
//    return "seed";
//});

//Route::get('generator_builder', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@builder');
//
//Route::get('field_template', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@fieldTemplate');
//
//Route::get('relation_field_template', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@relationFieldTemplate');
//
//Route::post('generator_builder/generate', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@generate');
//
//Route::post('generator_builder/rollback', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@rollback');
//
//Route::post(
//    'generator_builder/generate-from-file',
//    '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@generateFromFile'
//);



//Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
