<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\SuratJenis;

class SuratJenisApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_surat_jenis()
    {
        $suratJenis = factory(SuratJenis::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/surat_jenis', $suratJenis
        );

        $this->assertApiResponse($suratJenis);
    }

    /**
     * @test
     */
    public function test_read_surat_jenis()
    {
        $suratJenis = factory(SuratJenis::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/surat_jenis/'.$suratJenis->id
        );

        $this->assertApiResponse($suratJenis->toArray());
    }

    /**
     * @test
     */
    public function test_update_surat_jenis()
    {
        $suratJenis = factory(SuratJenis::class)->create();
        $editedSuratJenis = factory(SuratJenis::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/surat_jenis/'.$suratJenis->id,
            $editedSuratJenis
        );

        $this->assertApiResponse($editedSuratJenis);
    }

    /**
     * @test
     */
    public function test_delete_surat_jenis()
    {
        $suratJenis = factory(SuratJenis::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/surat_jenis/'.$suratJenis->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/surat_jenis/'.$suratJenis->id
        );

        $this->response->assertStatus(404);
    }
}
