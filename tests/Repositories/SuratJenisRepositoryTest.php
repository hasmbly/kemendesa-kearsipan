<?php namespace Tests\Repositories;

use App\Models\SuratJenis;
use App\Repositories\SuratJenisRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class SuratJenisRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var SuratJenisRepository
     */
    protected $suratJenisRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->suratJenisRepo = \App::make(SuratJenisRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_surat_jenis()
    {
        $suratJenis = factory(SuratJenis::class)->make()->toArray();

        $createdSuratJenis = $this->suratJenisRepo->create($suratJenis);

        $createdSuratJenis = $createdSuratJenis->toArray();
        $this->assertArrayHasKey('id', $createdSuratJenis);
        $this->assertNotNull($createdSuratJenis['id'], 'Created SuratJenis must have id specified');
        $this->assertNotNull(SuratJenis::find($createdSuratJenis['id']), 'SuratJenis with given id must be in DB');
        $this->assertModelData($suratJenis, $createdSuratJenis);
    }

    /**
     * @test read
     */
    public function test_read_surat_jenis()
    {
        $suratJenis = factory(SuratJenis::class)->create();

        $dbSuratJenis = $this->suratJenisRepo->find($suratJenis->id);

        $dbSuratJenis = $dbSuratJenis->toArray();
        $this->assertModelData($suratJenis->toArray(), $dbSuratJenis);
    }

    /**
     * @test update
     */
    public function test_update_surat_jenis()
    {
        $suratJenis = factory(SuratJenis::class)->create();
        $fakeSuratJenis = factory(SuratJenis::class)->make()->toArray();

        $updatedSuratJenis = $this->suratJenisRepo->update($fakeSuratJenis, $suratJenis->id);

        $this->assertModelData($fakeSuratJenis, $updatedSuratJenis->toArray());
        $dbSuratJenis = $this->suratJenisRepo->find($suratJenis->id);
        $this->assertModelData($fakeSuratJenis, $dbSuratJenis->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_surat_jenis()
    {
        $suratJenis = factory(SuratJenis::class)->create();

        $resp = $this->suratJenisRepo->delete($suratJenis->id);

        $this->assertTrue($resp);
        $this->assertNull(SuratJenis::find($suratJenis->id), 'SuratJenis should not exist in DB');
    }
}
